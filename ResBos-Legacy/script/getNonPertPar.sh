#!/bin/bash
NonPertMode=$1

if [ $NonPertMode -eq 1 ]; then  #before marginalizing

echo "The NonPertFit result is before marginalizing."

G1NUMBER=`grep 'Summary :   0)  Parameter' log.txt |awk '{print $7}'`
G2NUMBER=`grep 'Summary :   1)  Parameter' log.txt |awk '{print $7}'`
G3NUMBER=`grep 'Summary :   2)  Parameter' log.txt |awk '{print $7}'`

G1NUMBER=`source /home/qdhan/jiuzhao_resbos2/resbos2-for-heavy-quark/ResBos-Legacy/script/SymbolExtract.sh $G1NUMBER`
G2NUMBER=`source /home/qdhan/jiuzhao_resbos2/resbos2-for-heavy-quark/ResBos-Legacy/script/SymbolExtract.sh $G2NUMBER`
G3NUMBER=`source /home/qdhan/jiuzhao_resbos2/resbos2-for-heavy-quark/ResBos-Legacy/script/SymbolExtract.sh $G3NUMBER`

elif [ $NonPertMode -eq 2 ]; then #after marginalizing

echo "The NonPertFit result is after marginalizing."

G1NUMBER=`grep 'Summary :       Mean +- sqrt(Variance):' log.txt |awk '{print $6}' |head -1 |tail -1`
G2NUMBER=`grep 'Summary :       Mean +- sqrt(Variance):' log.txt |awk '{print $6}' |head -2 |tail -1`
G3NUMBER=`grep 'Summary :       Mean +- sqrt(Variance):' log.txt |awk '{print $6}' |head -3 |tail -1`

G1NUMBER=`source /home/qdhan/jiuzhao_resbos2/resbos2-for-heavy-quark/ResBos-Legacy/script/SymbolExtract.sh $G1NUMBER`
G2NUMBER=`source /home/qdhan/jiuzhao_resbos2/resbos2-for-heavy-quark/ResBos-Legacy/script/SymbolExtract.sh $G2NUMBER`
G3NUMBER=`source /home/qdhan/jiuzhao_resbos2/resbos2-for-heavy-quark/ResBos-Legacy/script/SymbolExtract.sh $G3NUMBER`

fi

echo "NonPertFitting results:"
echo "g1 = $G1NUMBER"
echo "g2 = $G2NUMBER"
echo "g3 = $G3NUMBER"
