#!/bin/bash
ZquarkCoupl=$1

if [ $ZquarkCoupl -eq 1 ]; then
GUV=1800
GUA=5000
GDV=-3500
GDA=-5140
echo central value
elif [ $ZquarkCoupl -eq 2 ]; then
GUV=2300
GUA=5000
GDV=-3500
GDA=-5140
echo u quark gV +1 sigma
elif [ $ZquarkCoupl -eq 3 ]; then
GUV=1300
GUA=5000
GDV=-3500
GDA=-5140
echo u quark gV -1 sigma
elif [ $ZquarkCoupl -eq 4 ]; then
GUV=1800
GUA=5400
GDV=-3500
GDA=-5140
echo u quark gA +1 sigma
elif [ $ZquarkCoupl -eq 5 ]; then
GUV=1800
GUA=4500
GDV=-3500
GDA=-5140
echo u quark gA -1 sigma
elif [ $ZquarkCoupl -eq 6 ]; then
GUV=1800
GUA=5000
GDV=-3000
GDA=-5140
echo d quark gV +1 sigma
elif [ $ZquarkCoupl -eq 7 ]; then
GUV=1800
GUA=5000
GDV=-4100
GDA=-5140
echo d quark gV -1 sigma
elif [ $ZquarkCoupl -eq 8 ]; then
GUV=1800
GUA=5000
GDV=-3500
GDA=-4640
echo d quark gA +1 sigma
elif [ $ZquarkCoupl -eq 9 ]; then
GUV=1800
GUA=5000
GDV=-3500
GDA=-5430
echo d quark gA -1 sigma
fi

myGDV=$[-1*$GDV]
myGDA=$[-1*$GDA]
echo u quark vector coupling is "       " 0.$GUV
echo u quark axial-vector coupling is " " 0.$GUA
echo d quark vector coupling is "       "-0.$myGDV
echo d quark axial-vector coupling is " "-0.$myGDA

#elif [ $ZquarkCoupl -eq 2 ]; then


