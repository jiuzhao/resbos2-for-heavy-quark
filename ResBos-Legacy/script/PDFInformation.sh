#!/bin/bash
PDFNumber=$1
if [ $PDFNumber -eq 1 ]; then
MYPDF=CT14HERA2NNLO
PDFFile=If1363.
NumberOfPDFSet=56
elif [ $PDFNumber -eq 2 ]; then
MYPDF=CT14nnlo
PDFFile=CT14nn.
NumberOfPDFSet=56
elif [ $PDFNumber -eq 3 ]; then
MYPDF=MMHT2014nnlo68cl
PDFFile=MMHT2_00
NumberOfPDFSet=50
elif [ $PDFNumber -eq 4 ]; then
MYPDF=CT18NNLO
PDFFile=i2Tn3.
NumberOfPDFSet=0
elif [ $PDFNumber -eq 5 ]; then
MYPDF=CT10
PDFFile=ct10.
NumberOfPDFSet=52
elif [ $PDFNumber -eq 6 ]; then
MYPDF=NNPDF30_nnlo_as_0118
PDFFile=NNPDF_00
NumberOfPDFSet=100 ##actually is 100, but when number of PDF set is larger than 99, all of the script need to be modified more
elif [ $PDFNumber -eq 7 ]; then
MYPDF=CT18NNLO
PDFFile=i2Tn3.
NumberOfPDFSet=58
elif [ $PDFNumber -eq 8 ]; then
MYPDF=NNPDF31_nnlo_as_0118_1000
PDFFile=NNPDF_00
NumberOfPDFSet=1000 ##actually is 100, but when number of PDF set is larger than 99, all of the script need to be modified more
elif [ $PDFNumber -eq 9 ]; then
MYPDF=cteq6
PDFFile=cteq6.
NumberOfPDFSet=40
elif [ $PDFNumber -eq 10 ]; then
MYPDF=cteq66
PDFFile=cteq66.
NumberOfPDFSet=44
elif [ $PDFNumber -eq 11 ]; then
MYPDF=NNPDF31_nnlo_as_0118
PDFFile=NNPDF31_nnlo_as_0118.
NumberOfPDFSet=100
elif [ $PDFNumber -eq 12 ]; then
MYPDF=FullAFB_RotatedCT18NNLO_LHA
PDFFile=FullAFB_RotatedCT18NNLO_LHA.
NumberOfPDFSet=2
elif [ $PDFNumber -eq 13 ]; then
MYPDF=MSHT20nnlo_as118
PDFFile=MSHT20nnlo_as118.
NumberOfPDFSet=64
elif [ $PDFNumber -eq 14 ]; then
MYPDF=NNPDF40_nnlo_as_01180
PDFFile=NNPDF40_nnlo_as_01180.
NumberOfPDFSet=100
elif [ $PDFNumber -eq 15 ]; then
MYPDF=P0Theory_RotatedCT18NNLO
PDFFile=P0Theory_RotatedCT18NNLO.
NumberOfPDFSet=58
elif [ $PDFNumber -eq 16 ]; then
MYPDF=NNPDF31_nnlo_as_0118_hessian
PDFFile=NNPDF31_nnlo_as_0118_hessian.
NumberOfPDFSet=100
elif [ $PDFNumber -eq 17 ]; then
MYPDF=CT18NLO
PDFFile=CT18NLO.
NumberOfPDFSet=58
elif [ $PDFNumber -eq 18 ]; then
MYPDF=MMHT2014nlo68cl
PDFFile=MMHT2014nlo68cl.
NumberOfPDFSet=50
elif [ $PDFNumber -eq 19 ]; then
MYPDF=NNPDF31_nlo_as_0118_hessian
PDFFile=NNPDF31_nlo_as_0118_hessian.
NumberOfPDFSet=100
elif [ $PDFNumber -eq 20 ]; then
MYPDF=CT10nnlo
PDFFile=CT10nnlo.
NumberOfPDFSet=50
elif [ $PDFNumber -eq 21 ]; then
MYPDF=ABMP16als118_5_nnlo
PDFFile=ABMP16als118_5_nnlo.
NumberOfPDFSet=29
elif [ $PDFNumber -eq 22 ]; then
MYPDF=PDF4LHC15_nnlo_30
PDFFile=PDF4LHC15_nnlo_30.
NumberOfPDFSet=30
elif [ $PDFNumber -eq 23 ]; then
MYPDF=PDF4LHC21_40
PDFFile=PDF4LHC21_40.
NumberOfPDFSet=42
elif [ $PDFNumber -eq 24 ]; then
MYPDF=ATLASpdf21_T3
PDFFile=ATLASpdf21_T3.
NumberOfPDFSet=52
elif [ $PDFNumber -eq 25 ]; then
MYPDF=DeltaZY2QT2_RotatedCT18NNLO
PDFFile=DeltaZY2QT2_RotatedCT18NNLO.
NumberOfPDFSet=8
elif [ $PDFNumber -eq 26 ]; then
MYPDF=NNPDF40_nnlo_as_01180_hessian
PDFFile=NNPDF40_nnlo_as_01180_hessian.
NumberOfPDFSet=50
elif [ $PDFNumber -eq 27 ]; then
MYPDF=CT18AsNNLO
PDFFile=CT18AsNNLO.
NumberOfPDFSet=68
elif [ $PDFNumber -eq 28 ]; then
MYPDF=CT18AsLatNNLO
PDFFile=CT18AsLatNNLO.
NumberOfPDFSet=68
elif [ $PDFNumber -eq 29 ]; then
MYPDF=CT18ANNLO
PDFFile=CT18ANNLO.
NumberOfPDFSet=58
elif [ $PDFNumber -eq 30 ]; then
MYPDF=CT18ZNNLO
PDFFile=CT18ZNNLO.
NumberOfPDFSet=58
elif [ $PDFNumber -eq 31 ]; then
MYPDF=CT18XNNLO
PDFFile=CT18XNNLO.
NumberOfPDFSet=58
elif [ $PDFNumber -eq 110 ]; then
MYPDF=CT18NNLO_as_0110
PDFFile=CT18NNLO_as_0110.
NumberOfPDFSet=0
elif [ $PDFNumber -eq 111 ]; then
MYPDF=CT18NNLO_as_0111
PDFFile=CT18NNLO_as_0111.
NumberOfPDFSet=0
elif [ $PDFNumber -eq 112 ]; then
MYPDF=CT18NNLO_as_0112
PDFFile=CT18NNLO_as_0112.
NumberOfPDFSet=0
elif [ $PDFNumber -eq 113 ]; then
MYPDF=CT18NNLO_as_0113
PDFFile=CT18NNLO_as_0113.
NumberOfPDFSet=0
elif [ $PDFNumber -eq 114 ]; then
MYPDF=CT18NNLO_as_0114
PDFFile=CT18NNLO_as_0114.
NumberOfPDFSet=0
elif [ $PDFNumber -eq 115 ]; then
MYPDF=CT18NNLO_as_0115
PDFFile=CT18NNLO_as_0115.
NumberOfPDFSet=0
elif [ $PDFNumber -eq 116 ]; then
MYPDF=CT18NNLO_as_0116
PDFFile=CT18NNLO_as_0116.
NumberOfPDFSet=0
elif [ $PDFNumber -eq 117 ]; then
MYPDF=CT18NNLO_as_0117
PDFFile=CT18NNLO_as_0117.
NumberOfPDFSet=0
elif [ $PDFNumber -eq 118 ]; then
MYPDF=CT18NNLO_as_0118
PDFFile=CT18NNLO_as_0118.
NumberOfPDFSet=0
elif [ $PDFNumber -eq 119 ]; then
MYPDF=CT18NNLO_as_0119
PDFFile=CT18NNLO_as_0119.
NumberOfPDFSet=0
elif [ $PDFNumber -eq 120 ]; then
MYPDF=CT18NNLO_as_0120
PDFFile=CT18NNLO_as_0120.
NumberOfPDFSet=0
elif [ $PDFNumber -eq 121 ]; then
MYPDF=CT18NNLO_as_0121
PDFFile=CT18NNLO_as_0121.
NumberOfPDFSet=0
elif [ $PDFNumber -eq 122 ]; then
MYPDF=CT18NNLO_as_0122
PDFFile=CT18NNLO_as_0122.
NumberOfPDFSet=0
elif [ $PDFNumber -eq 123 ]; then
MYPDF=CT18NNLO_as_0123
PDFFile=CT18NNLO_as_0123.
NumberOfPDFSet=0
elif [ $PDFNumber -eq 124 ]; then
MYPDF=CT18NNLO_as_0124
PDFFile=CT18NNLO_as_0124.
NumberOfPDFSet=0
fi
