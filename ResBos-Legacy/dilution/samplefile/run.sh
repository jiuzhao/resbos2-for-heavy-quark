#!/bin/bash
WORKDIR=$PWD
Process=$1
GridDIR=/lustre/AtlUser/yfu/pku_resbos/gridfile
multicore=0
localrun=0
WConfig=1   ###1: w321  2: w432  0: delsig  -1: asy
YConfig=1   ###1: y1    2: y2    0: onlyW   -1: asy  ##For NLO calculation, please usage WConfig=0 YConfig=0, and then WConfig=-1 YConfig=-1, and hadd the result.
isResBos2=0
isLegacy=1
isKinCorr=0
KinCorr=1
UseLatestPara=0

SeparateQuark=1

PseudoData=0
STWINPUT=0.2315
onlyCentral=0
TryOld=0
NTemplate=40
isRotation=0

SCALECHOICE=_MT
NonPertFormat=_BLNY

PDFNumber=1
ECM=13TeV
# PDFNumber = 1: CT14HEAR2NNLO
# PDFNumber = 2: CT14nnlo
# PDFNumber = 3: MMHT2014nnlo68cl
######################Initialize the PDF Name###############################
source /home/yfu/pku_resbos/FrameWork/script/PDFInformation.sh $PDFNumber
source /home/yfu/pku_resbos/FrameWork/script/ResBosConfig.sh $WConfig $YConfig 1
if [ $onlyCentral -eq 1 -o $PseudoData -eq 1 ]; then
    NumberOfPDFSet=0
fi
############################################################################
NJobs=1

Process=$1
if [ $Process -eq 1 ]; then
    echo Running scale variation.
    NProcess=49
    InitialProcess=22
elif [ $Process -eq 2 ]; then
    echo Running PDF uncertainty.
    NProcess=${NumberOfPDFSet}
    InitialProcess=0
elif [ $Process -eq 3 ]; then
    echo Generating stw template.
    NProcess=${NTemplate}
    InitialProcess=1
    PseudoData=0
fi

NBosons=2
if [ $SeparateQuark -eq 1 ]; then
    NBosons=10
fi

IsReweight=$2
if [ $IsReweight -eq 1 ]; then
    IWGT=-1
    if [ $Process -eq 1 ]; then
        NProcess=1
        InitialProcess=1
    elif [ $Process -eq 2 ]; then
        NProcess=0
        InitialProcess=0
    fi
elif [ $IsReweight -eq 2 ]; then
    IWGT=-2
    if [ $Process -eq 1 ]; then
        InitialProcess=2
    elif [ $Process -eq 2 ]; then
        InitialProcess=1
    fi
elif [ $IsReweight -eq 0 ]; then
    IWGT=0
fi

if [ $PseudoData -eq 1 ]; then
    export SINT2W=${STWINPUT}
    WHICHPDF=PseudoData_${SINT2W}_${MYPDF}
fi

#if [ $TryOld -eq 1 ]; then
#    WHICHPDF=${WHICHPDF}_TryOld
#fi

#if [ $Process -eq 3 ]; then
#    WHICHPDF=Template_${WHICHPDF}
#fi

WHICHPDF=${WHICHPDF}${SCALECHOICE}
cd $WORKDIR
if [ ! -d "$WHICHPDF/" ]; then
    mkdir $WHICHPDF
fi

for((Boson=1;Boson<=$NBosons;Boson++)); do
########Set Boson Type############
if [ $SeparateQuark -eq 0 ]; then

    if [ $Boson -eq 1 ]; then
        WHICHBOSON=ZU
        myBOSON=zu
    elif [ $Boson -eq 2 ]; then
        WHICHBOSON=ZD
        myBOSON=zd
    elif [ $Boson -eq 3 ]; then
        WHICHBOSON=W+
        myBOSON=wp
    elif [ $Boson -eq 4 ]; then
        WHICHBOSON=W-
        myBOSON=wm
    fi

elif [ $SeparateQuark -eq 1 ]; then

    if [ $Boson -eq 1 ]; then
    WHICHBOSON=ZU
    myBOSON=zu_uubar
    export ZUZDFLAG=1
    elif [ $Boson -eq 2 ]; then
    WHICHBOSON=ZD
    myBOSON=zd_ddbar
    export ZUZDFLAG=2
    elif [ $Boson -eq 3 ]; then
    WHICHBOSON=ZD
    myBOSON=zd_ssbar
    export ZUZDFLAG=3
    elif [ $Boson -eq 4 ]; then
    WHICHBOSON=ZU
    myBOSON=zu_ccbar
    export ZUZDFLAG=4
    elif [ $Boson -eq 5 ]; then
    WHICHBOSON=ZD
    myBOSON=zd_bbbar
    export ZUZDFLAG=5
    elif [ $Boson -eq 6 ]; then
    WHICHBOSON=ZU
    myBOSON=zu_ubaru
    export ZUZDFLAG=-1
    elif [ $Boson -eq 7 ]; then
    WHICHBOSON=ZD
    myBOSON=zd_dbard
    export ZUZDFLAG=-2
    elif [ $Boson -eq 8 ]; then
    WHICHBOSON=ZD
    myBOSON=zd_sbars
    export ZUZDFLAG=-3
    elif [ $Boson -eq 9 ]; then
    WHICHBOSON=ZU
    myBOSON=zu_cbarc
    export ZUZDFLAG=-4
    elif [ $Boson -eq 10 ]; then
    WHICHBOSON=ZD
    myBOSON=zd_bbarb
    export ZUZDFLAG=-5
    fi

fi

echo " "
echo ${myBOSON} production:
echo " "

##########Define Scale and PDF name####################
for((k=${InitialProcess};k<=${NProcess};k++)); do
if [ $k -lt 10 ]; then
    PDFDIR=${PDFFile}0${k}_${myBOSON}${NonPertFormat}
else
    PDFDIR=${PDFFile}${k}_${myBOSON}${NonPertFormat}
fi
ScaleVari=$[900+${k}]
ScaleDIR=Scale${ScaleVari}

##########build directory##############################
if [ $Process -eq 1 ]; then
    cd $WORKDIR
    PDFDIR=${PDFFile}00_${myBOSON}${NonPertFormat}
elif [ $Process -eq 2 ]; then
    cd $WORKDIR
    ScaleDIR=Scale908
    if [ $UseLatestPara -eq 1 ]; then
        ScaleVari=977
        ScaleDIR=Scale977
    fi
elif [ $Process -eq 3 ]; then
    cd $WORKDIR
    PDFDIR=${PDFFile}00_${myBOSON}${NonPertFormat}
    ScaleDIR=Scale908
    if [ $UseLatestPara -eq 1 ]; then
        ScaleVari=977
        ScaleDIR=Scale977
    fi
    STWINPUT=$[2252+3*$k]
    STWINPUT=0.${STWINPUT}
    echo stw input is $STWINPUT
fi

if [ $Process -ne 3 ]; then
    if [ ! -d "$WORKDIR/${WHICHPDF}/$PDFDIR/" ]; then
        mkdir $WORKDIR/${WHICHPDF}/$PDFDIR
    fi
    if [ ! -d "$WORKDIR/${WHICHPDF}/$PDFDIR/$ScaleDIR/" ]; then
        mkdir $WORKDIR/${WHICHPDF}/$PDFDIR/$ScaleDIR
    fi

    echo " "
    echo $PDFDIR $ScaleDIR
    echo " "

elif [ $Process -eq 3 ]; then
    if [ ! -d "$WORKDIR/${WHICHPDF}/${STWINPUT}/" ]; then
        mkdir $WORKDIR/${WHICHPDF}/${STWINPUT}
    fi
    if [ ! -d "$WORKDIR/${WHICHPDF}/${STWINPUT}/$PDFDIR/" ]; then
        mkdir $WORKDIR/${WHICHPDF}/${STWINPUT}/$PDFDIR
    fi
fi

for((n=1;n<=$NJobs;n++)); do

if [ $Process -ne 3 ]; then

    cd $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR

    if [ ! -d "$WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR/JOB${n}/" ]; then
        mkdir $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR/JOB${n}
    fi

    cd $WORKDIR
    cp resbos_root 00resbos.in $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR/JOB${n}
    cd $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR/JOB${n}

elif [ $Process -eq 3 ]; then

    cd $WORKDIR/$WHICHPDF/$STWINPUT/$PDFDIR

    if [ ! -d "$WORKDIR/$WHICHPDF/$STWINPUT/$PDFDIR/JOB${n}/" ]; then
        mkdir $WORKDIR/$WHICHPDF/$STWINPUT/$PDFDIR/JOB${n}
    fi

    cd $WORKDIR
    cp resbos_root 00resbos.in $WORKDIR/$WHICHPDF/$STWINPUT/$PDFDIR/JOB${n}
    cd $WORKDIR/$WHICHPDF/$STWINPUT/$PDFDIR/JOB${n}

fi

if [ $IsReweight -eq 0 ]; then
    RANDOMSEED=$[${RANDOM}+1*${k}+10*${n}]
else
    #RANDOMSEED=14640
    RANDOMSEED=$[10000+10*${n}]
fi
sed -i "s/RANDOMSEED/$RANDOMSEED/g" 00resbos.in
sed -i "s/IWGT/$IWGT/g" 00resbos.in
sed -i "s/ISYGRID/$ISYGRID/g" 00resbos.in
cp 00resbos.in resbos.in

if [ $IsReweight -eq 2 ]; then
    if [ $Process -eq 1 ]; then
        ln -s ../../../${PDFFile}00_${myBOSON}${NonPertFormat}/Scale901/JOB${n}/weights.dat weights.dat
        cp ../../../${PDFFile}00_${myBOSON}${NonPertFormat}/Scale901/JOB${n}/resbos.root resbos.root
    elif [ $Process -eq 2 ]; then
        ln -s ../../../${PDFFile}00_${myBOSON}${NonPertFormat}/Scale908/JOB${n}/weights.dat weights.dat
        cp ../../../${PDFFile}00_${myBOSON}${NonPertFormat}/Scale908/JOB${n}/resbos.root resbos.root
    fi
fi

#sed -i "s/STWINPUT/$STWINPUT/g" resbosm.f

#############refresh the W-piece grid path and Y-piece grid path##############
source /home/yfu/pku_resbos/FrameWork/script/ResBosConfig.sh $WConfig $YConfig 0
##############################################################################
#if [ $TryOld -eq 1 ]; then
#    WHICHPDF=${WHICHPDF}_TryOld
#fi

if [ $PDFNumber -eq 1 ]; then

    if [ $TryOld -eq 1 ]; then
        if [ $WConfig -eq 2 ]; then
            echo WARNING!! There is no w432 gridfile in old path structure.
        fi

        ln -s ${GridDIR}/${MYPDF}/${myBOSON}/w321/${PDFDIR}_w321.out legacy_w321.out

        if [ $YConfig -eq 1 ]; then
            ln -s ${GridDIR}/${MYPDF}/${myBOSON}/y/${PDFDIR}_y.out legacy_yk.out
        elif [ $YConfig -eq 2 ]; then
            ln -s ${GridDIR}/${MYPDF}/${myBOSON}/yk/${PDFDIR}_yk.out legacy_yk.out
        fi
    else
        ln -s $WGridPath legacy_w321.out
        ln -s $YGridPath legacy_yk.out
    fi

else
    ln -s $WGridPath legacy_w321.out
    ln -s $YGridPath legacy_yk.out
fi

#ln -s $WORKDIR/../ResBos2/${MYPDF}_pds/${PDFDIR}/$ScaleDIR/y_1/JOB1/Grids/ResBos_GridFile_0.out legacy_w321.out
#ln -s $WORKDIR/../../old_legacy/${MYPDF}_pds/${PDFDIR}/${ScaleDIR}/JOB1/legacy.out legacy_w321.out
#ln -s $WORKDIR/../../get_yk/${PDFDIR}_${ScaleDIR}_yk.out legacy_yk.out

if [ $localrun -eq 1 ]; then

if [ $multicore -eq 1 ]; then
    nohup ./resbos_root &
else
    ./resbos_root
fi

else ##submit condor job

cat >>mySUB_ResBos_${WConfig}_${YConfig}_${myBOSON}_${k}_${n}  <<EOF
universe                        = vanilla
requirements                    = 
executable                      = myANA.sh


transfer_output                 = true
transfer_error                  = true
transfer_executable             = true

should_transfer_files           = IF_NEEDED

#run v15 on SL5 nodes
#+SL_START                       = 5

when_to_transfer_output         = ON_EXIT

log                             = JOBINDEX_${k}_${n}.condor.log
output                          = JOBINDEX_${k}_${n}.stdout
error                           = JOBINDEX_${k}_${n}.stderr

#notification                    = NEVER

queue
EOF

if [ $Process -ne 3 ]; then
    cd $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR/JOB${n}
elif [ $Process -eq 3 ]; then
    cd $WORKDIR/$WHICHPDF/$STWINPUT/$PDFDIR/JOB${n}
fi

cat >>myANA.sh  <<EOF
#!/bin/bash
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup "root 6.20.06-x86_64-centos7-gcc8-opt"
lsetup "cmake 3.14.0"
export LHAPDFSYS=/home/yfu/LHAPDF
export PATH=/home/yfu/LHAPDF/bin:${PATH}
export LD_LIBRARY_PATH=/home/yfu/LHAPDF/lib:${LD_LIBRARY_PATH}
if [ $PseudoData -eq 1 -o $Process -eq 3 ]; then
export SINT2W=${STWINPUT}
fi
if [ $Process -ne 3 ]; then
    cd $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR/JOB${n}
elif [ $Process -eq 3 ]; then
    cd $WORKDIR/$WHICHPDF/$STWINPUT/$PDFDIR/JOB${n}
fi
if [ $SeparateQuark -eq 1 ]; then
    if [ $Boson -eq 1 ]; then
    export myEventType=uubar
    elif [ $Boson -eq 2 ]; then
    export myEventType=ddbar
    elif [ $Boson -eq 3 ]; then
    export myEventType=ssbar
    elif [ $Boson -eq 4 ]; then
    export myEventType=ccbar
    elif [ $Boson -eq 5 ]; then
    export myEventType=bbbar
    elif [ $Boson -eq 6 ]; then
    export myEventType=ubaru
    elif [ $Boson -eq 7 ]; then
    export myEventType=dbard
    elif [ $Boson -eq 8 ]; then
    export myEventType=sbars
    elif [ $Boson -eq 9 ]; then
    export myEventType=cbarc
    elif [ $Boson -eq 10 ]; then
    export myEventType=bbarb
    fi
fi
#make
./resbos_root
###############################################
EOF
chmod +x myANA.sh

condor_submit mySUB_ResBos_${WConfig}_${YConfig}_${myBOSON}_${k}_${n}

fi

cd $WORKDIR/

done

done ##done process

done ##done boson

cd $WORKDIR/
