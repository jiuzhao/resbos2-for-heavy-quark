#ifndef FileForEPUMP_h
#define FileForEPUMP_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include "TMath.h"
#include "TH1D.h"
#include <iostream>
#include <vector>
#include <fstream>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include "TLorentzVector.h"
#include "TH3F.h"
#include <TRandom3.h>
#include <TMinuit.h>
#include <TApplication.h>
#include "TEnv.h"
#include <TComplex.h>
#include <TGraph.h>
#include <TProfile.h>
#include <TProfile2D.h>
#include "TTree.h"

using namespace std;

class FileForEPUMP
{
 public:

 ofstream myData;
 ofstream myTheory;

 double DataPoint;
 double StaError, TotError, UncError, Corr1, Corr2;
 char pdfname[40];

 bool isRebin;
 double *xbins;
 int size;

 bool isInputStaErr;
 double *StaErr;

 double *TheoryPoint;
 double *TheoryStaError;

 double myR;

 TFile *file;
 TFile *file1;
 TFile *file2;
 TFile *CorrelationFile;

 TH1D* h1;
 TH1D* h2;
 TH2D* Correlation;

 bool isAFB;
 bool isSideband;
 bool isStatSmear;

 int Error_type;

 FileForEPUMP(const char* fileName);
 virtual ~FileForEPUMP();
 virtual void writedata(const char* rootName, const char* histName);
 virtual void writetheory(vector<TString> rootNames, const char* histName);
 virtual void AFBdata(const char* rootName, const char* FName, const char* BName);
 virtual void AFBtheory(vector<TString> rootNames, const char* FName, const char* BName);
 virtual void writeWdata(const char* rootName1, const char* rootName2, const char* histName);
 virtual void writeWtheory(vector<TString> rootNames1, vector<TString> rootNames2, const char* histName);
 virtual void GetTheoryStaError(const char* rootName1, const char* rootName2, const char* histName);
 virtual void SetAFB();
 virtual void SetSideband();
 virtual void Rebin(double *rebin, int Nbin);
 virtual void InputStaErr(double * myStaErr);
 virtual void InputCorrelation(const char* rootName, const char* histName);
 virtual void End();
};

FileForEPUMP::FileForEPUMP(const char* fileName)
{
 isSideband = false;
 isRebin = false;
 isAFB = false;
 isInputStaErr = false;
 isStatSmear = false;
 Error_type = 1;
 myData.open((TString)fileName + ".data");
 myTheory.open((TString)fileName + ".theory");
}

FileForEPUMP::~FileForEPUMP()
{
}

void FileForEPUMP::SetAFB()
{
 isAFB = true;
}

void FileForEPUMP::SetSideband()
{
 isAFB = true;
 isSideband = true;

 cout<<"This is AFB sideband data"<<endl;
}

void FileForEPUMP::Rebin(double *rebin, int Nbin)
{
 isRebin = true;
 xbins = rebin;
 size = Nbin;

 cout<<"Histogram is rebined, bin number = "<<size - 1<<endl;
}

void FileForEPUMP::InputStaErr(double *myStaErr)
{
 isInputStaErr = true;
 StaErr = myStaErr;
}
void FileForEPUMP::InputCorrelation(const char* rootName, const char* histName)
{
 Error_type = 2;
 CorrelationFile = new TFile(rootName);
 Correlation = (TH2D *)CorrelationFile->Get(histName);
}

void FileForEPUMP::writedata(const char* rootName, const char* histName)
{
 file = new TFile(rootName);
 h1 = (TH1D *)file->Get(histName);

 myData<<"* 2 NormError #_corr_sys     Ecm      |# grids|# KF|# col|"<<endl;
 myData<<"*  0.0          9         1960.0           888231"<<endl;
 myData<<"# of corr_err   ,  Data Column,    StatErr Column,  UncSys Column,   corr_err Column"<<endl;
 myData<<"      2                  3             4                 6                 7"<<endl;
 myData<<"left right DataPoint StatErr TotSys UncSys Corr1 Corr2"<<endl;

 for(int ibin = 0; ibin < h1->GetNbinsX(); ibin++)
 {
  if(isAFB && !isSideband && !((h1->GetBinCenter(ibin+1) - h1->GetBinWidth(ibin+1) / 2) < 91.1876 && (h1->GetBinCenter(ibin+1) + h1->GetBinWidth(ibin+1) / 2) > 91.1876)) continue; 
  if(isAFB &&  isSideband &&  ((h1->GetBinCenter(ibin+1) - h1->GetBinWidth(ibin+1) / 2) < 91.1876 && (h1->GetBinCenter(ibin+1) + h1->GetBinWidth(ibin+1) / 2) > 91.1876)) continue;

  DataPoint = h1->GetBinContent(ibin+1);
  if(!isInputStaErr) StaError = h1->GetBinError(ibin+1);
  if(isInputStaErr) StaError = StaErr[ibin];

  if(fabs(DataPoint)<0.00000001)
  {
   DataPoint=1E-8;
   StaError=1E-4;
  }

  TotError = StaError;

  UncError = 0;
  Corr1 = 0;
  Corr2 = 0;

  myData<<(h1->GetBinCenter(ibin+1) - h1->GetBinWidth(ibin+1) / 2)<<"  "<<(h1->GetBinCenter(ibin+1) + h1->GetBinWidth(ibin+1) / 2)<<"  "<<DataPoint<<"  "<<StaError<<"  "<<TotError<<"  "<<UncError<<"  "<<Corr1<<"  "<<Corr2<<endl;
 }

 if(Error_type == 2)
 {
  myData<<"*: Correlation Matrix"<<endl;
  myData<<"#a        #b    CorrCoeff"<<endl;

  for(int ibiny = 1; ibiny < Correlation->GetNbinsY(); ibiny++)
  {
   for(int ibinx = 1; ibinx < Correlation->GetNbinsX(); ibinx++)
   {
    if(ibinx >= ibiny)
    {
     myData<<ibinx<<"  "<<ibiny<<"  "<<Correlation->GetBinContent(ibinx,ibiny)<<endl;
    }
   }
  }
  cout<<"The Correlation matrix has been writed"<<endl;
 }
 cout<<"The data has been writed"<<endl;
}

void FileForEPUMP::writetheory(vector<TString> rootNames, const char* histName)
{
 myTheory<<"Theory Column"<<endl;
 myTheory<<"      1"<<endl;

 for(int ifile = 0; ifile < rootNames.size(); ifile++)
 {
  file = new TFile(rootNames[ifile]);
  h1 = (TH1D *)file->Get(histName);

  if(ifile<10) sprintf(pdfname," Data : If1363H.0%d.dta",ifile);
  else sprintf(pdfname," Data : If1363H.%d.dta",ifile);
  myTheory<<pdfname<<endl;

  for(int ibin = 0; ibin < h1->GetNbinsX(); ibin++)
  {
   if(isAFB && !isSideband && !((h1->GetBinCenter(ibin+1) - h1->GetBinWidth(ibin+1) / 2) < 91.1876 && (h1->GetBinCenter(ibin+1) + h1->GetBinWidth(ibin+1) / 2) > 91.1876)) continue;
   if(isAFB &&  isSideband &&  ((h1->GetBinCenter(ibin+1) - h1->GetBinWidth(ibin+1) / 2) < 91.1876 && (h1->GetBinCenter(ibin+1) + h1->GetBinWidth(ibin+1) / 2) > 91.1876)) continue;

   DataPoint = h1->GetBinContent(ibin+1);

   if(fabs(DataPoint)<0.00000001) DataPoint=1E-8;

   myTheory<<DataPoint<<endl;
  }
  delete file;
 }
}

void FileForEPUMP::AFBdata(const char* rootName, const char* FName, const char* BName)
{
 double Nf, Nb, AFB, NfError, NbError, AFBError;

 myData<<"* 2 NormError #_corr_sys     Ecm      |# grids|# KF|# col|"<<endl;
 myData<<"*  0.0          9         1960.0           888231"<<endl;
 myData<<"# of corr_err   ,  Data Column,    StatErr Column,  UncSys Column,   corr_err Column"<<endl;
 myData<<"      2                  3             4                 6                 7"<<endl;
 myData<<"left right DataPoint StatErr TotSys UncSys Corr1 Corr2"<<endl;

 file = new TFile(rootName);
 h1 = (TH1D *)file->Get(FName);
 h2 = (TH1D *)file->Get(BName);

 if(isRebin)
 {
  h1 = (TH1D *)h1->Rebin(size - 1, "FZmass", xbins);
  h2 = (TH1D *)h2->Rebin(size - 1, "BZmass", xbins);
 }
// cout<<size - 1<<endl;
// cout<<FZmass->GetNbinsX()<<endl;
 for (int ibin = 0; ibin < h1->GetNbinsX(); ibin++)
 {

  if(isSideband && ((h1->GetBinCenter(ibin+1) - h1->GetBinWidth(ibin+1) / 2) < 91.1876 && (h1->GetBinCenter(ibin+1) + h1->GetBinWidth(ibin+1) / 2) > 91.1876)) continue;

  Nf = h1->GetBinContent(ibin + 1);
  Nb = h2->GetBinContent(ibin + 1);
  AFB = (Nf - Nb) / (Nf + Nb);
  
  NfError = h1->GetBinError(ibin + 1);
  NbError = h2->GetBinError(ibin + 1);
  AFBError = 2 * sqrt((Nf * NbError) * (Nf * NbError) + (Nb * NfError) * (Nb * NfError)) / ((Nf + Nb) * (Nf + Nb));
  
  DataPoint = AFB;
  StaError = AFBError;
  TotError = StaError;
  
  UncError = 0;
  Corr1 = 0;
  Corr2 = 0;

  myData<<(h1->GetBinCenter(ibin+1) - h1->GetBinWidth(ibin+1) / 2)<<"  "<<(h1->GetBinCenter(ibin+1) + h1->GetBinWidth(ibin+1) / 2)<<"  "<<DataPoint<<"  "<<StaError<<"  "<<TotError<<"  "<<UncError<<"  "<<Corr1<<"  "<<Corr2<<endl;
 }

 delete file;
}

void FileForEPUMP::AFBtheory(vector<TString> rootNames, const char* FName, const char* BName)
{
 double Nf, Nb, AFB, NfError, NbError, AFBError;
 
 myTheory<<"Theory Column"<<endl;
 myTheory<<"      1"<<endl;

 for(int ifile = 0; ifile < rootNames.size(); ifile++)
 {
  file = new TFile(rootNames[ifile]);
  h1 = (TH1D *)file->Get(FName);
  h2 = (TH1D *)file->Get(BName);

  if(isRebin)
  {
   h1 = (TH1D *)h1->Rebin(size - 1, "FZmass", xbins);
   h2 = (TH1D *)h2->Rebin(size - 1, "BZmass", xbins);
  }
  
  if(ifile < 10) sprintf(pdfname," Data : If1363H.0%d.dta",ifile);
  else sprintf(pdfname," Data : If1363H.%d.dta",ifile);
  myTheory<<pdfname<<endl;

  for(int ibin = 0; ibin < h1->GetNbinsX(); ibin++)
  {

   if(isSideband && ((h1->GetBinCenter(ibin+1) - h1->GetBinWidth(ibin+1) / 2) < 91.1876 && (h1->GetBinCenter(ibin+1) + h1->GetBinWidth(ibin+1) / 2) > 91.1876)) continue;

   Nf = h1->GetBinContent(ibin + 1);
   Nb = h2->GetBinContent(ibin + 1);
   AFB = (Nf - Nb) / (Nf + Nb);

   NfError = h1->GetBinError(ibin + 1); 
   NbError = h2->GetBinError(ibin + 1);
   AFBError = 2 * sqrt((Nf * NbError) * (Nf * NbError) + (Nb * NfError) * (Nb * NfError)) / ((Nf + Nb) * (Nf + Nb));  
 
   DataPoint = AFB;
   
   if(fabs(DataPoint)<0.00000001) DataPoint=1E-8;
   
   myTheory<<DataPoint<<endl;
  }
  delete file;
 }

}

void FileForEPUMP::GetTheoryStaError(const char* rootName1, const char* rootName2, const char* histName)
{
 isStatSmear = true;

 file1 = new TFile(rootName1);
 h1 = (TH1D *)file1->Get(histName);
 file2 = new TFile(rootName2);
 h2 = (TH1D *)file2->Get(histName);

 TheoryPoint = new double[100];
 TheoryStaError = new double[100];

 if(isRebin)
 {
  h1 = (TH1D *)h1->Rebin(size - 1, "h1", xbins);
  h2 = (TH1D *)h2->Rebin(size - 1, "h2", xbins);
 }

 double Wplus_point, Wplus_error, Wminus_point, Wminus_error, Wasymmetry, Wasymmetry_error;

 for(int ibin = 0; ibin < h1->GetNbinsX(); ibin++)
 {
  Wplus_point = h1->GetBinContent(ibin + 1);
  Wplus_error = h1->GetBinError(ibin + 1);
  Wminus_point = h2->GetBinContent(ibin + 1);
  Wminus_error = h2->GetBinError(ibin + 1);
  Wasymmetry = (Wplus_point - Wminus_point) / (Wplus_point + Wminus_point);
  Wasymmetry_error = (2 * sqrt(Wminus_point * Wminus_point * Wplus_error * Wplus_error + Wplus_point * Wplus_point * Wminus_error * Wminus_error)) / ((Wplus_point +Wminus_point) * (Wplus_point + Wminus_point));

  TheoryPoint[ibin] = Wasymmetry;
  TheoryStaError[ibin] = Wasymmetry_error;
 }

 delete file1;
 delete file2;

}

void FileForEPUMP::writeWdata(const char* rootName1, const char* rootName2, const char* histName)
{
 TRandom3 random(0);

 file1 = new TFile(rootName1);
 h1 = (TH1D *)file1->Get(histName);
 file2 = new TFile(rootName2);
 h2 = (TH1D *)file2->Get(histName);

 if(isRebin)
 {
  h1 = (TH1D *)h1->Rebin(size - 1, "h1", xbins);
  h2 = (TH1D *)h2->Rebin(size - 1, "h2", xbins);
 }

 double Wplus_point, Wplus_error, Wminus_point, Wminus_error, Wasymmetry, Wasymmetry_error;

 myData<<"* 2 NormError #_corr_sys     Ecm      |# grids|# KF|# col|"<<endl;
 myData<<"*  0.0          9         13000.0           888231"<<endl;
 myData<<"# of corr_err   ,  Data Column,    StatErr Column,  UncSys Column,   corr_err Column"<<endl;
 myData<<"      2                  3             4                 6                 7"<<endl;
 myData<<"left right DataPoint StatErr TotSys UncSys Corr1 Corr2"<<endl;

 for(int ibin = 0; ibin < h1->GetNbinsX(); ibin++)
 {
  Wplus_point = h1->GetBinContent(ibin + 1);
  Wplus_error = h1->GetBinError(ibin + 1);
  Wminus_point = h2->GetBinContent(ibin + 1);
  Wminus_error = h2->GetBinError(ibin + 1);
  Wasymmetry = (Wplus_point - Wminus_point) / (Wplus_point + Wminus_point);
  Wasymmetry_error = (2 * sqrt(Wminus_point * Wminus_point * Wplus_error * Wplus_error + Wplus_point * Wplus_point * Wminus_error * Wminus_error)) / ((Wplus_point +Wminus_point) * (Wplus_point + Wminus_point));
  
  DataPoint = Wasymmetry;
  StaError = Wasymmetry_error;
  TotError = StaError;

  if(isStatSmear)
  {
   myR = random.Gaus(0, 1);
   cout<<"Manual input Gaus smear:"<<endl;
   cout<<"myR = "<<myR<<endl;
   DataPoint = DataPoint * (1 + TheoryStaError[ibin] * myR);
   StaError = TheoryStaError[ibin];
   TotError = StaError;
  }
  
  UncError = 0;
  Corr1 = 0;
  Corr2 = 0;
  myData<<(h1->GetBinCenter(ibin+1) - h1->GetBinWidth(ibin+1) / 2)<<"  "<<(h1->GetBinCenter(ibin+1) + h1->GetBinWidth(ibin+1) / 2)<<"  "<<DataPoint<<"  "<<StaError<<"  "<<TotError<<"  "<<UncError<<"  "<<Corr1<<"  "<<Corr2<<endl;
 }

 delete file1;
 delete file2;
}

void FileForEPUMP::writeWtheory(vector<TString> rootNames1, vector<TString> rootNames2, const char* histName)
{
 myTheory<<"Theory Column"<<endl;
 myTheory<<"      1"<<endl;

 double Wplus_point, Wplus_error, Wminus_point, Wminus_error, Wasymmetry, Wasymmetry_error;

 for(int ifile = 0; ifile < rootNames1.size(); ifile++)
 {
  file1 = new TFile(rootNames1[ifile]);
  h1 = (TH1D *)file1->Get(histName);

  file2 = new TFile(rootNames2[ifile]);
  h2 = (TH1D *)file2->Get(histName);

  if(isRebin)
  {
   h1 = (TH1D *)h1->Rebin(size - 1, "h1", xbins);
   h2 = (TH1D *)h2->Rebin(size - 1, "h2", xbins);
  }

  if(ifile<10) sprintf(pdfname," Data : If1363H.0%d.dta",ifile);
  else sprintf(pdfname," Data : If1363H.%d.dta",ifile);
  myTheory<<pdfname<<endl;

  for(int ibin = 0; ibin < h1->GetNbinsX(); ibin++)
  {
   Wplus_point = h1->GetBinContent(ibin + 1);
   Wplus_error = h1->GetBinError(ibin + 1);
   Wminus_point = h2->GetBinContent(ibin + 1);
   Wminus_error = h2->GetBinError(ibin + 1);
   Wasymmetry = (Wplus_point - Wminus_point) / (Wplus_point + Wminus_point);
   Wasymmetry_error = (2 * sqrt(Wminus_point * Wminus_point * Wplus_error * Wplus_error + Wplus_point * Wplus_point * Wminus_error * Wminus_error)) / ((Wplus_point +Wminus_point) * (Wplus_point + Wminus_point));
   
   myTheory<<Wasymmetry<<endl;
  }
  delete file1;
  delete file2;
 }
}

void FileForEPUMP::End()
{
}
#endif
