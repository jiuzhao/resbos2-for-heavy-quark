#!/bin/bash
WORKDIR=$PWD
multicore=0
localrun=0
onlyCentral=0
UseLatestPara=0

PDFNumber=7
# PDFNumber = 1: CT14HEAR2NNLO
# PDFNumber = 2: CT14nnlo
# PDFNumber = 3: MMHT2014nnlo68cl
######################Initialize the PDF Name###############################
source /home/yfu/pku_resbos/FrameWork/script/PDFInformation.sh $PDFNumber
############################################################################
if [ $onlyCentral -eq 1 ]; then
    NumberOfPDFSet=0
fi

Process=$1
if [ $Process -eq 1 ]; then
echo Running scale variation.
NProcess=15
InitialProcess=1
elif [ $Process -eq 2 ]; then
echo Running PDF uncertainty.
NProcess=${NumberOfPDFSet}
InitialProcess=1
fi

ScaleChoice1=1

if [ $ScaleChoice1 -eq 1 ]; then
SCALECHOICE=MT
elif [ $ScaleChoice1 -eq 0 ]; then
SCALECHOICE=Q
fi

WHICHPDF=${MYPDF}_${SCALECHOICE}_pds

NBosons=4
NJobs=`cat inp/q_grid_0.inp |wc -l`

cd $WORKDIR
if [ ! -d "$WHICHPDF/" ]; then
    mkdir $WHICHPDF
fi

for((Boson=3;Boson<=$NBosons;Boson++)); do
########Set Boson Type############
if [ $Boson -eq 1 ]; then
WHICHBOSON=2
BOSON=ZU
myBOSON=zu
ZTYPE=1
elif [ $Boson -eq 2 ]; then
WHICHBOSON=2
BOSON=ZD
myBOSON=zd
ZTYPE=-1
elif [ $Boson -eq 3 ]; then
WHICHBOSON=1
myBOSON=wp
ZTYPE=0
elif [ $Boson -eq 4 ]; then
WHICHBOSON=-1
myBOSON=wm
ZTYPE=0
fi

echo " "
echo ${myBOSON} production:
echo " "

##########Define Scale and PDF name####################
for((k=${InitialProcess};k<=${NProcess};k++)); do
if [ $k -lt 10 ]; then
PDFNAME=${PDFFile}0${k}.pds
PDFDIR=${PDFFile}0${k}_${myBOSON}
SETNUMBER=$k
else
PDFNAME=${PDFFile}${k}.pds
PDFDIR=${PDFFile}${k}_${myBOSON}
SETNUMBER=$k
fi
ScaleVari=$[900+${k}]
ScaleDIR=Scale${ScaleVari}

##########build directory##############################
if [ $Process -eq 1 ]; then
cd $WORKDIR
PDFNAME=${PDFFile}00.pds
PDFDIR=${PDFFile}00_${myBOSON}
SETNUMBER=0
elif [ $Process -eq 2 ]; then
cd $WORKDIR
ScaleVari=1051
ScaleDIR=Scale${ScaleVari}
if [ $UseLatestPara -eq 1 ]; then
ScaleVari=977
ScaleDIR=Scale977
fi
fi

if [ ! -d "$WORKDIR/${WHICHPDF}/$PDFDIR/" ]; then
    mkdir $WORKDIR/${WHICHPDF}/$PDFDIR
fi
if [ ! -d "$WORKDIR/${WHICHPDF}/$PDFDIR/$ScaleDIR/" ]; then
    mkdir $WORKDIR/${WHICHPDF}/$PDFDIR/$ScaleDIR
fi

##################################
source /home/qdhan/jiuzhao_resbos2/resbos2-for-heavy-quark/ResBos-Legacy/ResBos2/script/ScaleConfig.sh $ScaleVari
##################################

#if [ $localrun -eq 1 ]; then
#cd $WORKDIR
#
#if [ $Boson -lt 3 ]; then
#cp w_asym 00w_asym.in $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR
#else
#cp w_asym 00w_asym.in $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR
#fi
#
#cp -r inp $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR
#cd $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR
#sed -i "s/WHICHBOSON/$WHICHBOSON/g" 00w_asym.in
#sed -i "s/SCALECHOICE/$ScaleChoice1/g" 00w_asym.in
#sed -i "s/MURSCALE/$MURSCALE/g" 00w_asym.in
#sed -i "s/MUFSCALE/$MUFSCALE/g" 00w_asym.in
#sed -i "s/ZTYPE/$ZTYPE/g" 00w_asym.in
#cat 00w_asym.in > w_asym.in
#ln -s /lustre/AtlUser/yfu/LHAPDF/share/LHAPDF/${MYPDF}_pds/$PDFNAME pdf00.pds
#
#if [ $multicore -eq 1 ]; then
#
#totalJob=`ps -ef|grep w_asym|grep -v grep|awk '{print $1}'|grep ${USER}|wc -l`
#while [ $totalJob -gt 15 ]; do
#  sleep 5
#  totalJob=`ps -ef|grep w_asym|grep -v grep|awk '{print $1}'|grep ${USER}|wc -l`
#done
#
#if [ $Boson -lt 3 ]; then
#nohup ./w_asym &
#else
#nohup ./w_asym &
#fi
#
#else
#
#if [ $Boson -lt 3 ]; then
#./w_asym
#else
#./w_asym
#fi
#
#fi
#
#else ###submit condor job
for((n=1;n<=$NJobs;n++)); do
totalJob=`condor_q ${USER}|grep ${USER}|wc -l`
while [ $totalJob -gt 1960 ] ; do
  sleep 30
  totalJob=`condor_q ${USER}|grep ${USER}|wc -l`
done

cd $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR
mkdir JOB${n}
cd $WORKDIR

if [ $Boson -lt 3 ]; then
cp w_asym 00w_asym.in $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR/JOB$n
else
cp w_asym 00w_asym.in $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR/JOB$n
fi

cp -r inp $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR/JOB$n
cd $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR/JOB$n
sed -i "s/MYPDF/$MYPDF/g" 00w_asym.in
sed -i "s/SETNUMBER/$SETNUMBER/g" 00w_asym.in
sed -i "s/WHICHBOSON/$WHICHBOSON/g" 00w_asym.in
sed -i "s/SCALECHOICE/$ScaleChoice1/g" 00w_asym.in
sed -i "s/MURSCALE/$MURSCALE/g" 00w_asym.in
sed -i "s/MUFSCALE/$MUFSCALE/g" 00w_asym.in
sed -i "s/ZTYPE/$ZTYPE/g" 00w_asym.in
cat 00w_asym.in > w_asym.in
#ln -s /lustre/AtlUser/yfu/LHAPDF/share/LHAPDF/${MYPDF}_pds/$PDFNAME pdf00.pds
cd $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR/JOB$n/inp
cat q_grid_0.inp|head -${n}|tail -1 > q_grid.inp
cd ..

if [ $localrun -eq 1 ]; then

if [ $multicore -eq 1 ]; then

totalJob=`ps -ef|grep w_asym|grep -v grep|awk '{print $1}'|grep ${USER}|wc -l`
#while [ $totalJob -gt 15 ]; do
#  sleep 5
#  totalJob=`ps -ef|grep w_asym|grep -v grep|awk '{print $1}'|grep ${USER}|wc -l`
#done

nohup ./w_asym &

else
./w_asym
fi
echo $PDFDIR $ScaleDIR has been generated.

else ###submit condor job

totalJob=`condor_q ${USER}|grep ${USER}|wc -l`
while [ $totalJob -gt 1960 ] ; do
  sleep 30
  totalJob=`condor_q ${USER}|grep ${USER}|wc -l`
done

cd $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR/JOB$n
cat >>mySUB_asym_${MYPDF}_${myBOSON}_${k}_${n}  <<EOF
universe                        = vanilla
requirements                    = 
executable                      = myANA.sh


transfer_output                 = true
transfer_error                  = true
transfer_executable             = true

should_transfer_files           = IF_NEEDED

#run v15 on SL5 nodes
#+SL_START                       = 5

when_to_transfer_output         = ON_EXIT

log                             = JOBINDEX_${k}_${n}.condor.log
output                          = JOBINDEX_${k}_${n}.stdout
error                           = JOBINDEX_${k}_${n}.stderr

#notification                    = NEVER

queue
EOF
cd $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR/JOB$n
cat >>myANA.sh  <<EOF
#!/bin/bash
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup "root 6.20.06-x86_64-centos7-gcc8-opt"
lsetup "cmake 3.14.0"
export LHAPDFSYS=/home/yfu/LHAPDF
export PATH=/home/yfu/LHAPDF/bin:${PATH}
export LD_LIBRARY_PATH=/home/yfu/LHAPDF/lib:${LD_LIBRARY_PATH}
cd $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR/JOB$n

Boson=${Boson}
if [ $Boson -lt 3 ]; then
./w_asym
else
./w_asym
fi

###############################################
EOF
chmod +x myANA.sh

condor_submit mySUB_asym_${MYPDF}_${myBOSON}_${k}_${n}

fi

done

done

done
cd $WORKDIR
