#!/bin/bash
WORKDIR=$PWD
CONVDIR=/lustre/AtlUser/yfu/pku_resbos/ScaleUncertainty/resbos2_mmht
multicore=0
localrun=0
PDFNumber=7
# PDFNumber = 1: CT14HEAR2NNLO
# PDFNumber = 2: CT14nnlo
# PDFNumber = 3: MMHT2014nnlo68cl
######################Initialize the PDF Name###############################
source /home/yfu/pku_resbos/FrameWork/script/PDFInformation.sh $PDFNumber
############################################################################

echo Running PDF uncertainty.
NProcess=${NumberOfPDFSet}
InitialProcess=0

WHICHPDF=${MYPDF}_pds
NJobs=`cat inp/q_grid_0.inp |wc -l`

cd $WORKDIR

##########Define Scale and PDF name####################
for((k=0;k<=$NProcess;k++)); do

if [ $k -lt 10 ]; then
    PDFDIR=${PDFFile}0${k}
    SETNUMBER=${k}
else
    PDFDIR=${PDFFile}${k}
    SETNUMBER=${k}
fi

cd $WORKDIR

mv $WORKDIR/$WHICHPDF/$PDFDIR/ConvGrids/${MYPDF}_${SETNUMBER}_Conv_C1.out ${CONVDIR}/ConvGrids
mv $WORKDIR/$WHICHPDF/$PDFDIR/ConvGrids/${MYPDF}_${SETNUMBER}_Conv_C1P1.out ${CONVDIR}/ConvGrids
mv $WORKDIR/$WHICHPDF/$PDFDIR/ConvGrids/${MYPDF}_${SETNUMBER}_Conv_C1P1P1.out ${CONVDIR}/ConvGrids
mv $WORKDIR/$WHICHPDF/$PDFDIR/ConvGrids/${MYPDF}_${SETNUMBER}_Conv_C1P2.out ${CONVDIR}/ConvGrids
mv $WORKDIR/$WHICHPDF/$PDFDIR/ConvGrids/${MYPDF}_${SETNUMBER}_Conv_C2.out ${CONVDIR}/ConvGrids
mv $WORKDIR/$WHICHPDF/$PDFDIR/ConvGrids/${MYPDF}_${SETNUMBER}_Conv_C2P1.out ${CONVDIR}/ConvGrids
mv $WORKDIR/$WHICHPDF/$PDFDIR/ConvGrids/${MYPDF}_${SETNUMBER}_Conv_G1.out ${CONVDIR}/ConvGrids
mv $WORKDIR/$WHICHPDF/$PDFDIR/ConvGrids/${MYPDF}_${SETNUMBER}_Conv_G1P1.out ${CONVDIR}/ConvGrids

echo $PDFDIR has been moved.

done ##done process

cd $WORKDIR
