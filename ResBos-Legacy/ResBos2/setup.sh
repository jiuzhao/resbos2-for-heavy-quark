#!/bin/bash
setupATLAS
lsetup "root 6.20.06-x86_64-centos7-gcc8-opt"
#lsetup "views LCG_94 x86_64-slc6-gcc62-opt"
#lsetup "root 6.14.04-x86_64-slc6-gcc62-opt"
lsetup "cmake 3.14.0"
export LHAPDFSYS=/home/yfu/LHAPDF
export LD_LIBRARY_PATH=/ustcfs2/yfu/pku_resbos/SourceCode/resbos2/build/lib64:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=/home/yfu/BAT/lib:${LD_LIBRARY_PATH}
export PATH=/home/yfu/BAT/bin:${PATH}
export LD_LIBRARY_PATH=/ustcfs2/yfu/pku_resbos/SourceCode/resbos2/build/_deps/yaml-cpp-build:${LD_LIBRARY_PATH}
export PATH=/ustcfs2/yfu/pku_resbos/SourceCode/resbos2/build/bin:${PATH}

#for limit setting
export BATINSTALLDIR=/home/yfu/BAT/
export CPATH=$CPATH:/home/yfu/BAT/include


#for analysis code
export LHAPDF_ROOT_DIR=/home/yfu/LHAPDF/
export LHAPDF_PDF_DIR=${LHAPDF_ROOT_DIR}/share/LHAPDF
export PATH=${PATH}:/ustcfs2/yfu/MainCode/build/bin
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/ustcfs2/yfu/MainCode/build/lib
export ROOT_INCLUDE_PATH=/ustcfs2/yfu/MainCode/build/include/Analysis/
export APPLGRID_ROOT_DIR=/home/yfu/APPLGRID/
export BAT_ROOT_DIR=/home/yfu/BAT/
export PATH=/home/yfu/LHAPDF/bin:${PATH}
export LD_LIBRARY_PATH=/home/yfu/LHAPDF/lib:${LD_LIBRARY_PATH}
export PATH=/home/yfu/HOPPET/bin:${PATH}
export LD_LIBRARY_PATH=/home/yfu/HOPPET/lib:${LD_LIBRARY_PATH}

export LWTNN_ROOT_DIR=/home/yfu/lwtnn/build/
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${LWTNN_ROOT_DIR}/lib

#CPP
export PATH=/home/yfu/gnuplot/bin:${PATH}
export PATH=/cvmfs/sft.cern.ch/lcg/external/texlive/2016/bin/x86_64-linux:$PATH

#MCFM MadGraph
#lsetup "python 3.7.4-x86_64-centos7"
lsetup "python 3.9.13-x86_64-centos7"
unset PYTHONPATH
unset PYTHONHOME
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/home/yfu/pixman/lib
export PKG_CONFIG_PATH=${PKG_CONFIG_PATH}:/usr/lib64/pkgconfig
export PKG_CONFIG_PATH=${PKG_CONFIG_PATH}:/usr/share/pkgconfig
export PKG_CONFIG_PATH=${PKG_CONFIG_PATH}:/home/yfu/pixman/lib/pkgconfig
#export PKG_CONFIG_PATH=${PKG_CONFIG_PATH}:/home/yfu/freetype-2.9.1/build/lib64/pkgconfig
export PKG_CONFIG_PATH=${PKG_CONFIG_PATH}:/home/yfu/freetype-2.10.0/build/lib64/pkgconfig

#FastJet
export LD_LIBRARY_PATH=/home/yfu/FASTJET/lib:${LD_LIBRARY_PATH}
export PATH=/home/yfu/FASTJET/bin:${PATH}

#FastNLO
export LD_LIBRARY_PATH=/home/yfu/FASTNLO/lib:${LD_LIBRARY_PATH}
export PATH=/home/yfu/FASTNLO/bin:${PATH}

#ApplGrid
export LD_LIBRARY_PATH=/home/yfu/APPLGRID/lib:${LD_LIBRARY_PATH}
export PATH=/home/yfu/APPLGRID/bin:${PATH}
export PDFPATH=`applgrid-config --share`

#AMCFAST
export LD_LIBRARY_PATH=/home/yfu/AMCFAST/lib:${LD_LIBRARY_PATH}
export PATH=/home/yfu/AMCFAST/bin:${PATH}

#MCFM bridge
export LD_LIBRARY_PATH=/home/yfu/MCFMBRIDGE/lib:${LD_LIBRARY_PATH}
export PATH=/home/yfu/MCFMBRIDGE/bin:${PATH}

#Pythia
export LD_LIBRARY_PATH=/home/yfu/Pythia8/lib:${LD_LIBRARY_PATH}
export PATH=/home/yfu/Pythia8/bin:${PATH}


#python3 -m pip install --upgrade pip
#python3 -m pip install --user numpy
#python3 -m pip install --user matplotlib
#python3 -m pip install --user pycairo
#python3 -m pip install --user uproot
#python3 -m pip install --user "hist[plot]"
#python3 -m pip install --user pandas
#python3 -m pip install --user pyyaml
#python3 -m pip install --user scipy

