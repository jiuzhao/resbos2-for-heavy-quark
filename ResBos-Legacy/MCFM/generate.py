import numpy as np
import matplotlib.pyplot as plt
import yaml
import pandas as pd
import scipy.odr as odr
import scipy.optimize as optimize
import argparse
import functools


def read_plot(fileobj):
    hist = {'low': [], 'high': [], 'val': [], 'err': []}

    for _ in range(4):
        fileobj.readline()
    title = fileobj.readline().split('"')[-2]
    for _ in range(6):
        fileobj.readline()
    line = fileobj.readline()
    while 'PLOT' not in line:
        tokens = line.split()
        print(tokens)
        if len(tokens) == 3:
            hist['low'].append(float(tokens[0]))
            hist['high'].append(float(tokens[0]))
            hist['val'].append(float(tokens[1]))
            hist['err'].append(float(tokens[2]))
        elif len(tokens) == 4:
            hist['low'].append(float(tokens[0]))
            hist['high'].append(float(tokens[1]))
            hist['val'].append(float(tokens[2]))
            hist['err'].append(float(tokens[3]))
        else:
            raise ValueError("Incorrect format")
        line = fileobj.readline()

    fileobj.readline()
    return title, hist
    

def get_titles(filename):
    titles = []
    with open(filename, 'r') as data:
        for i, line in enumerate(data):
            if(i % 24 == 0):
                titles.append(line.strip())

    return titles


def readin(filename):
    titles = get_titles(filename)
    df = pd.read_csv(filename, header=None, names=range(4), sep=r'\s+')
    groups = df[0].isin(titles).cumsum()
    tables = {g.iloc[0, 0]: g.iloc[1:] for _, g in df.groupby(groups)}
    for i, table in enumerate(tables):
        tables[table] = tables[table].reset_index()
        if(i == 0):
            tables[table] = tables[table].rename(columns={0: "pt",
                                                          1: "dpt",
                                                          2: table,
                                                          3: f"d{table}"})
        else:
            tables[table] = tables[table].drop([0, 1], axis=1)
            tables[table] = tables[table].rename(columns={2: table,
                                                          3: f"d{table}"})
        tables[table] = tables[table].astype('float64')

    table = pd.concat(tables.values(), axis=1).drop(columns='index')
    return table


def normalize(df):
    columns = df.columns.values
    for i in range(len(columns[2::12])):
        # Get columns for each rapidity
        ia0, ida0 = columns[2+i*12], columns[3+i*12]
        ia1, ida1 = columns[4+i*12], columns[5+i*12]
        ia2, ida2 = columns[6+i*12], columns[7+i*12]
        ia3, ida3 = columns[8+i*12], columns[9+i*12]
        ia4, ida4 = columns[10+i*12], columns[11+i*12]
        ids, idds = columns[12+i*12], columns[13+i*12]

        # Propagate uncertainty
        df[ida0] = (abs(df[ia0] / df[ids])
                    * np.sqrt((df[ida0]/df[ia0])**2
                              + (df[idds]/df[ids])**2))
        df[ida1] = (abs(df[ia1] / df[ids])
                    * np.sqrt((df[ida1]/df[ia1])**2
                              + (df[idds]/df[ids])**2))
        df[ida2] = (abs(df[ia2] / df[ids])
                    * np.sqrt((df[ida2]/df[ia2])**2
                              + (df[idds]/df[ids])**2))
        df[ida3] = (abs(df[ia3] / df[ids])
                    * np.sqrt((df[ida3]/df[ia3])**2
                              + (df[idds]/df[ids])**2))
        df[ida4] = (abs(df[ia4] / df[ids])
                    * np.sqrt((df[ida4]/df[ia4])**2
                              + (df[idds]/df[ids])**2))

        # Normalize by dsigma
        df[ia0] = df[ia0] / df[ids]
        df[ia1] = df[ia1] / df[ids]
        df[ia2] = df[ia2] / df[ids]
        df[ia3] = df[ia3] / df[ids]
        df[ia4] = df[ia4] / df[ids]

    return df


def merge_nlo(datasets):
    columns = datasets[0].columns.values
    nlo = pd.DataFrame()

    nlo['pt'] = datasets[0]['pt']
    nlo['dpt'] = datasets[0]['dpt']

    for i in range(len(columns[2::2])):
        # Get columns for each rapidity
        ia, ida = columns[2+i*2], columns[3+i*2]

        a0, da0 = 0, 0
        for dataset in datasets:
            # Update central value
            a0 += dataset[ia]/dataset[ida]**2

            # Update uncertainty
            da0 += 1/dataset[ida]**2

        # Set value to mean and uncertainty
        nlo[ia], nlo[ida] = a0/da0, np.sqrt(1/da0)

    return nlo.fillna(0)


def bigaussian(x, xc, w1, w2, y0, H):
    term1 = np.exp(-0.5*(x-xc)**2/w1**2)
    term2 = np.exp(-0.5*(x-xc)**2/w2**2)
    result = np.where(x < xc, term1, term2)
    return y0 + H*result


def logistic(x, L, k, x0):
    return L/(1 + np.exp(-k*(x-x0)))


def polynomial(x, *params):
    result = 0
    for i, param in enumerate(params):
        result += x**(i+1)*param
    return result


def prune(df, name):
    mask = ~np.isnan(df[name])
    df_pt_cut = df[(df['pt'] > 7) | (df[f'd{name}'] < 1)]
    xvals = np.log10(df_pt_cut['pt'][mask])
    xerr = np.log10(df_pt_cut['dpt'][mask])
    yvals = df_pt_cut[name][mask]
    yerr = df_pt_cut[f'd{name}'][mask].values
    xvals = np.insert(-3.0, 0, xvals)
    xerr = np.insert(1e-6, 0, xerr)
    yvals = np.insert(0.0, 0, yvals)
    yerr = np.insert(1e-6, 0, yerr)
    return xvals, xerr, yvals, yerr


def fit(df, func, name, p0):
    xvals, xerr, yvals, yerr = prune(df, name)
    popt, _ = optimize.curve_fit(func, xvals, yvals, sigma=yerr,
                                 method='trf', p0=p0, jac='cs',
                                 bounds=[-2, 10])
    return lambda x: func(x, *popt), popt


def fit_all(df, order):
    filename = f'parameters_{order}.yml'
    try:
        with open(filename) as stream:
            parameters = yaml.load(stream)
    except FileNotFoundError:
        parameters = {}
    columns = [col for col in df.columns.values if col[0] == 'A']
    fits = {}
    for name in columns:
        print(f'\tFitting for {name}')
        parameters[name] = {}
        if 'A1' in name:
            func = bigaussian
            if 'parameters' in parameters[name]:
                p0 = parameters[name]['parameters']
            else:
                p0 = [1.54, 0.24271, 0.8132, 1e-17, 0.02515]
        else:
            func = logistic
            if 'parameters' in parameters[name]:
                p0 = parameters[name]['parameters']
            else:
                p0 = [1, 0.15, np.log10(200)]
        try:
            fits[name], params = fit(df, func, name, p0)
            converged = True
        except RuntimeError:
            print(f'\tFitting failed for {name}')
            print(f'\tTrying a 6th order polynomial')
            p1 = [1]*7
            try:
                fits[name], params = fit(df, polynomial, name, p1)
                converged = True
                func = polynomial
            except RuntimeError:
                print(f'\tFitting still failed for {name}')
                params = np.array(p0)
                converged = False

        fit_dict = parameters[name]
        fit_dict['function'] = func.__name__
        fit_dict['parameters'] = params.tolist()
        fit_dict['converged'] = converged

    with open(filename, 'w') as output:
        output.write(yaml.dump(parameters))
    return fits


def get_fit(func, *params):
    return lambda x: func(x, *params)


def load_fit(order):
    filename = f'parameters_{order}.yml'
    try:
        with open(filename) as stream:
            parameters = yaml.load(stream)
    except FileNotFoundError:
        print('Fit not found. Please run in fitting mode')
        raise

    fits = {}
    for name, node in parameters.items():
        func = node['function']
        params = node['parameters']
        if func == 'logistic':
            fits[name] = get_fit(logistic, *params)
        elif func == 'bigaussian':
            fits[name] = get_fit(bigaussian, *params)
        else:
            fits[name] = get_fit(polynomial, *params)

    return fits


def plot(df, name, color):
    plt.errorbar(df['pt'], df[name],
                 xerr=df['dpt'],
                 yerr=df[f'd{name}'],
                 ls='', marker='.',
                 color=color)


def plot_fitted(fit, color, label):
    x = np.log10(np.logspace(0, np.log10(600), 200))
    y = fit(x)
    plt.plot(10**x, y, color=color, label=label)


def plot_all(lo, lo_fit, nlo, nlo_fit):
    for name in lo_fit.keys():
        try:
            if lo is not None:
                plot(lo, name, 'tab:red')
            if nlo is not None:
                plot(nlo, name, 'tab:blue')
            plot_fitted(lo_fit[name], 'tab:red', 'lo')
            plot_fitted(nlo_fit[name], 'tab:blue', 'nlo')
            plt.title(name)
            plt.xscale('log')
            plt.legend()
            plt.show()
        except KeyError:
            print(f'Fits not available for {name}')
            plt.clf()


def generate_grid(filename, energy, pdf, qvals, qtvals, lo_fit, nlo_fit):
    y_fit = lo_fit.keys()
    y_fit = list(set([float(y.split('y')[-1])/10
                      for y in y_fit if 'YAL' not in y]))
    y_fit.sort()
    y_fit = [round(x-0.2, 1) for x in y_fit]
    y_fit_neg = [-x for x in y_fit[::-1]]
    y_fit_neg.extend(y_fit)
    y_fit = y_fit_neg

    y_tmp = np.linspace(-5, 5, int(10/0.1+1))

    format_string = "{:.4e} {:.4e} {:.4e} {:.8e} {:.8e} {:.8e} {:.8e}\n"
    with open(filename, 'w') as output:
        output.write('ECM, TYPE_V, PDF\n')
        output.write(f'{energy:.1f} DY {pdf}\n')
        output.write('  Q,qT,y R_A0 R_A1 R_A2 R_A3\n')
        for q in qvals:
#            for y in y_tmp:
            for y in y_fit:
                for qt in qtvals:
                     a0, a1, a2, a3, a4 = get_grid_values(q, qt, y,
                                                      lo_fit, nlo_fit)
                     output.write(format_string.format(q, qt, y,
                                                       a0, a1, a2, a3))
                    # output.write(format_string.format(q, qt, y,
                    #                                  1.0, 1.0, 1.0, 1.0))


def get_grid_values(q, qt, y, lo_fit, nlo_fit):
    angular = ["A0", "A1", "A2", "A3", "A4"]
    results = []
    for afunc in angular:
        lo = find_fit(y, lo_fit, afunc)
        nlo = find_fit(y, nlo_fit, afunc)

        result = nlo(np.log10(qt))/lo(np.log10(qt))
        if result < 0:
            result = 0
        if result > 5:
            result = 5
        results.append(result)
    return results


def find_fit(y, fit, func):
    yval = round(abs(y)+0.2, 1)
    name = func + "y{:02d}".format(int(yval*10))
    return fit[name]


def main():
    parser = argparse.ArgumentParser(description='Generate angular function'
                                                 ' k-factor grid')

    parser.add_argument('--fit', default=True, action='store_true')
    parser.add_argument('--no-fit', dest='fit', action='store_false')
    parser.add_argument('--plot', default=False, action='store_true')
    parser.add_argument('output', help='Output file name')
    parser.add_argument('energy', help='Center of mass energy')
    parser.add_argument('pdf', help='PDF set used')
    parser.add_argument('Number', help='Input file name')
    args = parser.parse_args()

    filename = args.output
    energy = args.energy
    Number = args.Number
    qvals = np.linspace(30, 300, int((300-30)/10+1))
    qtvals = np.logspace(np.log10(10), np.log10(600), 200)

    if args.fit:
        print('Loading data files')
        base_name = './Z_1jet_{}_{}Tev_binned{}.dat'
        base_name_nlo = './Z_1jet_{}_{}Tev_binned{}{}.dat'

        lo = readin(base_name.format('lo', energy, ''))
        #nlo = readin(base_name.format('nlo', energy, ''))
        #nlo_2 = readin(base_name.format('nlo', energy, '_2'))
        #nlo_3 = readin(base_name.format('nlo', energy, '_3'))
        #nlo_4 = readin(base_name.format('nlo', energy, '_4'))
        nlo_list = []
        for index in range(int(Number)):
            nlo = readin(base_name_nlo.format('nlo', energy, '_', index + 1))
            nlo_list += [nlo]

        print(len(nlo_list))
        nlo = merge_nlo(nlo_list)

        lo = normalize(lo)
        nlo = normalize(nlo)

        print('Fitting Leading Order:')
        lo_fits = fit_all(lo, 'LO')
        print('Fitting Next-to-Leading Order:')
        nlo_fits = fit_all(nlo, 'NLO')
    else:
        print('Loading fit parameters')
        lo_fits = load_fit('LO')
        nlo_fits = load_fit('NLO')

    print('Generating grid')
    generate_grid(filename, float(energy)*1000, args.pdf,
                  qvals, qtvals, lo_fits, nlo_fits)
    if args.plot:
        if args.fit:
            plot_all(lo, lo_fits, nlo, nlo_fits)
        else:
            plot_all(None, lo_fits, None, nlo_fits)


if __name__ == '__main__':
    main()
