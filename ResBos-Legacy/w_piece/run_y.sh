#!/bin/bash
WORKDIR=$PWD
multicore=0
localrun=0
PDFNumber=2
# PDFNumber = 1: CT14HEAR2NNLO
# PDFNumber = 2: CT14nnlo
# PDFNumber = 3: MMHT2014nnlo68cl
######################Initialize the PDF Name###############################
source /home/yfu/pku_resbos/FrameWork/script/PDFInformation.sh $PDFNumber
############################################################################

Process=$1
if [ $Process -eq 1 ]; then
echo Running scale variation.
NProcess=15
InitialProcess=1
elif [ $Process -eq 2 ]; then
echo Running PDF uncertainty.
NProcess=${NumberOfPDFSet}
InitialProcess=0
fi

WORY=0  ###  0:W-piece 3:Y-piece
WHICHPDF=${MYPDF}_pds
NJobs=`cat inp/q_grid_0.inp |wc -l`
NBosons=2

cd $WORKDIR
if [ ! -d "$WHICHPDF/" ]; then
    mkdir $WHICHPDF
fi

for((Boson=1;Boson<=$NBosons;Boson++)); do
########Set Boson Type############
if [ $Boson -eq 1 ]; then
WHICHBOSON=ZU
myBOSON=zu
elif [ $Boson -eq 2 ]; then
WHICHBOSON=ZD
myBOSON=zd
elif [ $Boson -eq 3 ]; then
WHICHBOSON=W+
myBOSON=wp
elif [ $Boson -eq 4 ]; then
WHICHBOSON=W-
myBOSON=wm
fi

echo " "
echo ${myBOSON} production:
echo " "

##########Define Scale and PDF name####################
for((k=${InitialProcess};k<=$NProcess;k++)); do
if [ $k -lt 10 ]; then
PDFNAME=${PDFFile}0${k}.pds
PDFDIR=${PDFFile}0${k}_${myBOSON}
else
PDFNAME=${PDFFile}${k}.pds
PDFDIR=${PDFFile}${k}_${myBOSON}
fi
ScaleVari=$[900+${k}]
ScaleDIR=Scale${ScaleVari}

##########build directory##############################
if [ $Process -eq 1 ]; then
cd $WORKDIR
PDFNAME=${PDFFile}00.pds
PDFDIR=${PDFFile}00_${myBOSON}
elif [ $Process -eq 2 ]; then
cd $WORKDIR
ScaleVari=908
ScaleDIR=Scale908
fi

if [ ! -d "$WORKDIR/${WHICHPDF}/$PDFDIR/" ]; then
    mkdir $WORKDIR/${WHICHPDF}/$PDFDIR
fi
if [ ! -d "$WORKDIR/${WHICHPDF}/$PDFDIR/$ScaleDIR/" ]; then
    mkdir $WORKDIR/${WHICHPDF}/$PDFDIR/$ScaleDIR
fi

for((n=1;n<=$NJobs;n++)); do
cd $WORKDIR
mkdir $WORKDIR/${WHICHPDF}/$PDFDIR/$ScaleDIR/JOB$n
cp 00legacy.in main $WORKDIR/${WHICHPDF}/$PDFDIR/$ScaleDIR/JOB$n
cp -r inp $WORKDIR/${WHICHPDF}/$PDFDIR/$ScaleDIR/JOB$n
cd $WORKDIR/${WHICHPDF}/$PDFDIR/$ScaleDIR/JOB$n/inp
cat q_grid_0.inp|head -${n}|tail -1 > q_grid.inp
cd $WORKDIR/${WHICHPDF}/$PDFDIR/$ScaleDIR/JOB$n
sed -i "s/ScaleVari/$ScaleVari/g" 00legacy.in
sed -i "s/WORY/$WORY/g" 00legacy.in
sed -i "s/WHICHBOSON/$WHICHBOSON/g" 00legacy.in
cat 00legacy.in > legacy.in
ln -s /lustre/AtlUser/yfu/LHAPDF/share/LHAPDF/$WHICHPDF/$PDFNAME pdf00.pds

if [ $localrun -eq 1 ]; then
if [ $multicore -eq 1 ]; then

totalJob=`ps -ef|grep main|grep -v grep|awk '{print $1}'|grep ${USER}|wc -l`
while [ $totalJob -gt 15 ]; do
  sleep 5
  totalJob=`ps -ef|grep main|grep -v grep|awk '{print $1}'|grep ${USER}|wc -l`
done

nohup ./main &

else
./main
fi
echo $PDFDIR $ScaleDIR has been generated.

else ###submit condor job

totalJob=`condor_q ${USER}|grep ${USER}|wc -l`
while [ $totalJob -gt 1960 ] ; do
  sleep 30
  totalJob=`condor_q ${USER}|grep ${USER}|wc -l`
done

cd $WORKDIR/${WHICHPDF}/$PDFDIR/$ScaleDIR/JOB$n
cat >>mySUB_w321_${MYPDF}_${myBOSON}_${k}_${n}  <<EOF
universe                        = vanilla
requirements                    = (machine == "bl-3-01.hep.ustc.edu.cn" || machine == "bl-3-02.hep.ustc.edu.cn" || machine == "bl-3-03.hep.ustc.edu.cn" || machine == "bl-3-04.hep.ustc.edu.cn" || machine == "bl-3-06.hep.ustc.edu.cn" || machine == "bl-4-01.hep.ustc.edu.cn" || machine == "bl-4-02.hep.ustc.edu.cn" || machine == "bl-4-05.hep.ustc.edu.cn" || machine == "bl-4-06.hep.ustc.edu.cn" || machine == "bl-4-07.hep.ustc.edu.cn" || machine == "bl-4-09.hep.ustc.edu.cn" || machine == "bl-4-10.hep.ustc.edu.cn" || machine == "bl-5-01.hep.ustc.edu.cn" || machine == "bl-5-02.hep.ustc.edu.cn" || machine == "bl-5-03.hep.ustc.edu.cn" || machine == "bl-5-05.hep.ustc.edu.cn" || machine == "bl-5-06.hep.ustc.edu.cn" || machine == "bl-5-07.hep.ustc.edu.cn" || machine == "bl-5-08.hep.ustc.edu.cn" || machine == "bl-5-09.hep.ustc.edu.cn" || machine == "bl-5-10.hep.ustc.edu.cn" || machine == "bl-5-11.hep.ustc.edu.cn" || machine == "bl-5-12.hep.ustc.edu.cn" || machine == "bl-5-13.hep.ustc.edu.cn" || machine == "bl-5-14.hep.ustc.edu.cn")
executable                      = myANA.sh


transfer_output                 = true
transfer_error                  = true
transfer_executable             = true

should_transfer_files           = IF_NEEDED

#run v15 on SL5 nodes
#+SL_START                       = 5

when_to_transfer_output         = ON_EXIT

log                             = JOBINDEX_${k}_${n}.condor.log
output                          = JOBINDEX_${k}_${n}.stdout
error                           = JOBINDEX_${k}_${n}.stderr

#notification                    = NEVER

queue
EOF
cd $WORKDIR/${WHICHPDF}/$PDFDIR/$ScaleDIR/JOB$n
cat >>myANA.sh  <<EOF
#!/bin/bash
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup "root 6.14.04-x86_64-slc6-gcc62-opt"
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/lustre/AtlUser/yfu/LHAPDF/lib
cd $WORKDIR/${WHICHPDF}/$PDFDIR/$ScaleDIR/JOB$n
./main
###############################################
EOF
chmod +x myANA.sh

condor_submit mySUB_w321_${MYPDF}_${myBOSON}_${k}_${n}

fi

done ##done job

done ##done process

done ##done boson

cd $WORKDIR
