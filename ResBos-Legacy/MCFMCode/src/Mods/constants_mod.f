      module constants
          use types
          implicit none

          public

          real(dp), parameter :: pi = 3.14159265358979311599796346854418516_dp
          complex(dp), parameter ::  im = (0._dp, 1._dp)
      end module
