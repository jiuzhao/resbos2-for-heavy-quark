      module singletop2_ints_nonres_m
      include 'singletop2_ints_nonres_common.inc.f'
      include 'nonresonant.inc.f'
      end module

      module singletop2_ints_nonres_resc_m
      include 'singletop2_ints_nonres_common.inc.f'
      include 'nonresonant.inc.f'
      end module
