#ifndef GITSHA1_HH
#define GITSHA1_HH

#include <string>
#include <vector>

extern const std::string GitSHA1;
extern const std::vector<std::string> GitModified;

#endif
