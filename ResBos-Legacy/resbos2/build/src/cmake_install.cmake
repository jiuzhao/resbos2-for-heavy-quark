# Install script for directory: /ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/src

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosUtilities.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosUtilities.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosUtilities.so"
         RPATH "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib64:/cvmfs/sft.cern.ch/lcg/releases/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib64" TYPE SHARED_LIBRARY FILES "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/Utilities/libResbosUtilities.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosUtilities.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosUtilities.so")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosUtilities.so"
         OLD_RPATH "/cvmfs/sft.cern.ch/lcg/releases/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/_deps/fmt-build:::::::::::::::::::::::::::::::::::::::::::::::"
         NEW_RPATH "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib64:/cvmfs/sft.cern.ch/lcg/releases/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.30-e5b21/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosUtilities.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosBeam.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosBeam.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosBeam.so"
         RPATH "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib64:/cvmfs/sft.cern.ch/lcg/releases/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib:/home/yfu/LHAPDF/lib")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib64" TYPE SHARED_LIBRARY FILES "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/Beam/libResbosBeam.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosBeam.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosBeam.so")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosBeam.so"
         OLD_RPATH "/cvmfs/sft.cern.ch/lcg/releases/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/Utilities:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/_deps/fmt-build:/home/yfu/LHAPDF/lib:"
         NEW_RPATH "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib64:/cvmfs/sft.cern.ch/lcg/releases/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib:/home/yfu/LHAPDF/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.30-e5b21/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosBeam.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosCalculation.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosCalculation.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosCalculation.so"
         RPATH "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib64:/cvmfs/sft.cern.ch/lcg/releases/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib64" TYPE SHARED_LIBRARY FILES "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/Calculation/libResbosCalculation.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosCalculation.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosCalculation.so")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosCalculation.so"
         OLD_RPATH "/cvmfs/sft.cern.ch/lcg/releases/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/Utilities:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/_deps/fmt-build:"
         NEW_RPATH "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib64:/cvmfs/sft.cern.ch/lcg/releases/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.30-e5b21/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosCalculation.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosProcess.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosProcess.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosProcess.so"
         RPATH "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib64:/cvmfs/sft.cern.ch/lcg/releases/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib64" TYPE SHARED_LIBRARY FILES "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/Process/libResbosProcess.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosProcess.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosProcess.so")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosProcess.so"
         OLD_RPATH "/cvmfs/sft.cern.ch/lcg/releases/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/Utilities:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/_deps/fmt-build:"
         NEW_RPATH "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib64:/cvmfs/sft.cern.ch/lcg/releases/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.30-e5b21/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosProcess.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosUser.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosUser.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosUser.so"
         RPATH "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib64:/cvmfs/sft.cern.ch/lcg/releases/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib64" TYPE SHARED_LIBRARY FILES "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/User/libResbosUser.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosUser.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosUser.so")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosUser.so"
         OLD_RPATH "/cvmfs/sft.cern.ch/lcg/releases/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"
         NEW_RPATH "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib64:/cvmfs/sft.cern.ch/lcg/releases/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.30-e5b21/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbosUser.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbos.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbos.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbos.so"
         RPATH "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib64:/cvmfs/sft.cern.ch/lcg/releases/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib:/home/yfu/LHAPDF/lib")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib64" TYPE SHARED_LIBRARY FILES "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/libResbos.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbos.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbos.so")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbos.so"
         OLD_RPATH "/cvmfs/sft.cern.ch/lcg/releases/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/Beam:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/Calculation:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/Process:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/User:/home/yfu/LHAPDF/lib:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/Utilities:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/_deps/fmt-build:"
         NEW_RPATH "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib64:/cvmfs/sft.cern.ch/lcg/releases/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib:/home/yfu/LHAPDF/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.30-e5b21/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libResbos.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libMCFMInterface.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libMCFMInterface.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libMCFMInterface.so"
         RPATH "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib64:/cvmfs/sft.cern.ch/lcg/releases/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib:/home/yfu/LHAPDF/lib")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib64" TYPE SHARED_LIBRARY FILES "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/libMCFMInterface.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libMCFMInterface.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libMCFMInterface.so")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libMCFMInterface.so"
         OLD_RPATH "/cvmfs/sft.cern.ch/lcg/releases/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/Beam:/home/yfu/LHAPDF/lib:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/Calculation:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/Process:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/Utilities:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/User:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/_deps/fmt-build:"
         NEW_RPATH "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib64:/cvmfs/sft.cern.ch/lcg/releases/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib:/home/yfu/LHAPDF/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.30-e5b21/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libMCFMInterface.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/resbos" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/resbos")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/resbos"
         RPATH "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib64:/cvmfs/sft.cern.ch/lcg/releases/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib:/home/yfu/LHAPDF/lib")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/resbos")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/resbos" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/resbos")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/resbos"
         OLD_RPATH "/cvmfs/sft.cern.ch/lcg/releases/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/Beam:/home/yfu/LHAPDF/lib:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/Calculation:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/Process:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/Utilities:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/User:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/_deps/fmt-build:"
         NEW_RPATH "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib:/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/lib64:/cvmfs/sft.cern.ch/lcg/releases/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib:/home/yfu/LHAPDF/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.30-e5b21/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/resbos")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libfmt.so.6.2.1"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libfmt.so.6"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    endif()
  endforeach()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib64" TYPE SHARED_LIBRARY FILES
    "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/_deps/fmt-build/libfmt.so.6.2.1"
    "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/_deps/fmt-build/libfmt.so.6"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libfmt.so.6.2.1"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libfmt.so.6"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHANGE
           FILE "${file}"
           OLD_RPATH "/cvmfs/sft.cern.ch/lcg/releases/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib:"
           NEW_RPATH "")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.30-e5b21/x86_64-centos7/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libfmt.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libfmt.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libfmt.so"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib64" TYPE SHARED_LIBRARY FILES "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/_deps/fmt-build/libfmt.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libfmt.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libfmt.so")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libfmt.so"
         OLD_RPATH "/cvmfs/sft.cern.ch/lcg/releases/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.30-e5b21/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libfmt.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libyaml-cpp.so.0.6.3"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libyaml-cpp.so.0.6"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    endif()
  endforeach()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib64" TYPE SHARED_LIBRARY FILES
    "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/_deps/yaml-cpp-build/libyaml-cpp.so.0.6.3"
    "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/_deps/yaml-cpp-build/libyaml-cpp.so.0.6"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libyaml-cpp.so.0.6.3"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libyaml-cpp.so.0.6"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.30-e5b21/x86_64-centos7/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libyaml-cpp.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libyaml-cpp.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libyaml-cpp.so"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib64" TYPE SHARED_LIBRARY FILES "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/_deps/yaml-cpp-build/libyaml-cpp.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libyaml-cpp.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libyaml-cpp.so")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.30-e5b21/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libyaml-cpp.so")
    endif()
  endif()
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/Utilities/cmake_install.cmake")
  include("/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/Beam/cmake_install.cmake")
  include("/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/Calculation/cmake_install.cmake")
  include("/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/Process/cmake_install.cmake")
  include("/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/User/cmake_install.cmake")
  include("/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/build/src/NonPertFit/cmake_install.cmake")

endif()

