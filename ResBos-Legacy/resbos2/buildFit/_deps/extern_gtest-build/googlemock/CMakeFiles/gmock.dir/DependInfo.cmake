# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/buildFit/_deps/extern_gtest-src/googlemock/src/gmock-all.cc" "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/buildFit/_deps/extern_gtest-build/googlemock/CMakeFiles/gmock.dir/src/gmock-all.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "FITTING"
  "GTEST_CREATE_SHARED_LIBRARY=1"
  "HAVE_ROOT"
  "LOGURU_UNSAFE_SIGNAL_HANDLER=0"
  "LOGURU_WITH_STREAMS=1"
  "ROOT6"
  "gmock_EXPORTS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/cvmfs/sft.cern.ch/lcg/releases/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/include"
  "_deps/extern_gtest-src/googlemock/include"
  "_deps/extern_gtest-src/googlemock"
  "_deps/extern_gtest-src/googletest/include"
  "_deps/extern_gtest-src/googletest"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/ustcfs2/yfu/pku_resbos/NewResBos2Test/resbos2/buildFit/_deps/extern_gtest-build/googletest/CMakeFiles/gtest.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
