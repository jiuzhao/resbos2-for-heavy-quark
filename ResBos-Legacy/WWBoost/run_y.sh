#!/bin/bash
WORKDIR=$PWD
multicore=0
localrun=0
onlyCentral=0
RunNonPertFit=0
isKinCorr=1
KinCorr=1
UseLatestPara=0

PDFNumber=1
# PDFNumber = 1: CT14HEAR2NNLO
# PDFNumber = 2: CT14nnlo
# PDFNumber = 3: MMHT2014nnlo68cl
######################Initialize the PDF Name###############################
source /home/yfu/pku_resbos/FrameWork/script/PDFInformation.sh $PDFNumber
############################################################################
if [ $onlyCentral -eq 1 ]; then
    NumberOfPDFSet=0
fi

Process=$1
if [ $Process -eq 1 ]; then
echo Running scale variation.
NProcess=15
InitialProcess=1
elif [ $Process -eq 2 ]; then
echo Running PDF uncertainty.
NProcess=${NumberOfPDFSet}
InitialProcess=0
fi

ScaleChoice1=1

if [ $ScaleChoice1 -eq 1 ]; then
SCALECHOICE=MT
elif [ $ScaleChoice1 -eq 0 ]; then
SCALECHOICE=Q
fi

WHICHPDF=${MYPDF}_${SCALECHOICE}_pds

NJobs=1

cd $WORKDIR
if [ ! -d "$WHICHPDF/" ]; then
    mkdir $WHICHPDF
fi

if [ ! -d "$WHICHPDF/LO" ]; then
    mkdir $WHICHPDF/LO
fi

#if [ ! -d "$WHICHPDF/NLO" ]; then
#    mkdir $WHICHPDF/NLO
#fi


##########Define Scale and PDF name####################
for((k=${InitialProcess};k<=$NProcess;k++)); do
if [ $k -lt 10 ]; then
PDFNAME=${PDFFile}0${k}.pds
PDFDIR=${PDFFile}0${k}
SETNUMBER=$k
else
PDFNAME=${PDFFile}${k}.pds
PDFDIR=${PDFFile}${k}
SETNUMBER=$k
fi
ScaleVari=$[900+${k}]
ScaleDIR=Scale${ScaleVari}

##########build directory##############################
if [ $Process -eq 1 ]; then
cd $WORKDIR
PDFNAME=${PDFFile}00.pds
PDFDIR=${PDFFile}00
SETNUMBER=0
elif [ $Process -eq 2 ]; then
cd $WORKDIR
ScaleVari=908
ScaleDIR=Scale908
if [ $UseLatestPara -eq 1 ]; then
ScaleVari=977
ScaleDIR=Scale977
fi
fi

if [ ! -d "$WORKDIR/${WHICHPDF}/LO/$PDFDIR/" ]; then
    mkdir $WORKDIR/${WHICHPDF}/LO/$PDFDIR
fi
if [ ! -d "$WORKDIR/${WHICHPDF}/LO/$PDFDIR/$ScaleDIR/" ]; then
    mkdir $WORKDIR/${WHICHPDF}/LO/$PDFDIR/$ScaleDIR
fi

#if [ ! -d "$WORKDIR/${WHICHPDF}/NLO/$PDFDIR/" ]; then
#    mkdir $WORKDIR/${WHICHPDF}/NLO/$PDFDIR
#fi
#if [ ! -d "$WORKDIR/${WHICHPDF}/NLO/$PDFDIR/$ScaleDIR/" ]; then
#    mkdir $WORKDIR/${WHICHPDF}/NLO/$PDFDIR/$ScaleDIR
#fi


##################################
source /home/yfu/pku_resbos/FrameWork/script/ScaleConfig.sh $ScaleVari
##################################

##################################
##LO##

for((n=1;n<=$NJobs;n++)); do
#RANDOMSEED=$[${RANDOM}+1*${k}+10*${n}]

ITMX1=50
NCALL1=100000
ITMX2=100
NCALL2=1000000
ORDER=lo

RANDOMSEED=$[2000+10*${k}+1*${n}]
cd $WORKDIR
mkdir $WORKDIR/${WHICHPDF}/LO/$PDFDIR/$ScaleDIR/JOB$n
cp 00input.DAT mcfm_omp process.DAT br.sm1 br.sm2 $WORKDIR/${WHICHPDF}/LO/$PDFDIR/$ScaleDIR/JOB$n
cd $WORKDIR/${WHICHPDF}/LO/$PDFDIR/$ScaleDIR/JOB$n
sed -i "s/MYPDF/$MYPDF/g" 00input.DAT
sed -i "s/SETNUMBER/$SETNUMBER/g" 00input.DAT
sed -i "s/RANDOMSEED/$RANDOMSEED/g" 00input.DAT
sed -i "s/ITMX1/$ITMX1/g" 00input.DAT
sed -i "s/NCALL1/$NCALL1/g" 00input.DAT
sed -i "s/ITMX2/$ITMX2/g" 00input.DAT
sed -i "s/NCALL2/$NCALL2/g" 00input.DAT
sed -i "s/ORDER/$ORDER/g" 00input.DAT
cat 00input.DAT > input.DAT

if [ $localrun -eq 1 ]; then
if [ $multicore -eq 1 ]; then

totalJob=`ps -ef|grep main|grep -v grep|awk '{print $1}'|grep ${USER}|wc -l`
#while [ $totalJob -gt 15 ]; do
#  sleep 5
#  totalJob=`ps -ef|grep main|grep -v grep|awk '{print $1}'|grep ${USER}|wc -l`
#done

nohup ./mcfm_omp &

else
./mcfm_omp
fi
echo $PDFDIR $ScaleDIR has been generated.

else ###submit condor job

totalJob=`condor_q ${USER}|grep ${USER}|wc -l`
while [ $totalJob -gt 1960 ] ; do
  sleep 30
  totalJob=`condor_q ${USER}|grep ${USER}|wc -l`
done

cd $WORKDIR/${WHICHPDF}/LO/$PDFDIR/$ScaleDIR/JOB$n
cat >>mySUB_MCFM_${MYPDF}_${k}_${n}  <<EOF
universe                        = vanilla
requirements                    = 
executable                      = myANA.sh


transfer_output                 = true
transfer_error                  = true
transfer_executable             = true

should_transfer_files           = IF_NEEDED

#run v15 on SL5 nodes
#+SL_START                       = 5

when_to_transfer_output         = ON_EXIT

log                             = JOBINDEX_${k}_${n}.condor.log
output                          = JOBINDEX_${k}_${n}.stdout
error                           = JOBINDEX_${k}_${n}.stderr

accounting_group                = long

#notification                    = NEVER

queue
EOF
cd $WORKDIR/${WHICHPDF}/LO/$PDFDIR/$ScaleDIR/JOB$n
cat >>myANA.sh  <<EOF
#!/bin/bash
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup "root 6.20.06-x86_64-centos7-gcc8-opt"
lsetup "cmake 3.14.0"
lsetup "python 3.7.4-x86_64-centos7"
unset PYTHONPATH
unset PYTHONHOME
export LHAPDFSYS=/home/yfu/LHAPDF
export PATH=/home/yfu/LHAPDF/bin:${PATH}
export LD_LIBRARY_PATH=/home/yfu/LHAPDF/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=/home/yfu/public/pku_resbos/SourceCode/MCFM_Josh/qcdloop-2.0.2/local/lib:${LD_LIBRARY_PATH}
export PATH=/home/yfu/MPI/bin:${PATH}
export LD_LIBRARY_PATH=/home/yfu/MPI/lib:${LD_LIBRARY_PATH}
export MANPATH=/home/yfu/MPI/share/man:${MANPATH}
cd $WORKDIR/${WHICHPDF}/LO/$PDFDIR/$ScaleDIR/JOB$n
./mcfm_omp
###############################################
EOF
chmod +x myANA.sh

condor_submit mySUB_MCFM_${MYPDF}_${k}_${n}

fi

done ##done job

done ##done process

cd $WORKDIR
