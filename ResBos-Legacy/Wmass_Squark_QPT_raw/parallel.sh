WORKDIR=$PWD

# Function to run root -l command for a given sample
run_root_command() {
    sample=$1
    root -q -b -l Ana_new_resbos_Wm.cpp\(\"${sample}\"\) | tee log_${sample}
}

# Create a screen session
screen -d -R -S root_session

# Attach to the screen session and run root commands in separate windows
for ((k=1; k<=100; k++)); do
    WMassDIR=$[335+${k}]
    WMassDIR=80.${WMassDIR}
    NewWMass=$WMassDIR
    mkdir $WMassDIR
    cp Ana_new_resbos_Wm.cpp AlgoWZ.hpp $WORKDIR/$WMassDIR
    cd $WMassDIR
    sed -i "s/NewWMass/$WMassDIR/g"  Ana_new_resbos_Wm.cpp

    # Create a new screen window and run the root command in it
    screen -S root_session -X screen -t "root_${k}" bash -c "run_root_command wm"
    screen -S root_session -X screen -t "root_${k}" bash -c "run_root_command Wm_withoutbc"
    screen -S root_session -X screen -t "root_${k}" bash -c "run_root_command wp"
    screen -S root_session -X screen -t "root_${k}" bash -c "run_root_command Wp_withoutbc"
    cd ..
done

