WORKDIR=$PWD
for((k=1;k<=100;k++)); do
cd $WORKDIR
WMassDIR=$[335+${k}]
WMassDIR=80.${WMassDIR}
NewWMass=$WMassDIR
mkdir $WMassDIR
cp Ana_new_resbos_Wm.cpp AlgoWZ.hpp $WORKDIR/$WMassDIR
cd $WMassDIR
sed -i "s/NewWMass/$WMassDIR/g"  Ana_new_resbos_Wm.cpp
cat >>myANA.sh  <<EOF
    root -l -q -b "Ana_new_resbos_Wm.cpp(\"wp\")"  | tee log_wp &
    root -l -q -b "Ana_new_resbos_Wm.cpp(\"wm\")"  | tee log_wm


EOF
chmod +x myANA.sh
sh myANA.sh

done

