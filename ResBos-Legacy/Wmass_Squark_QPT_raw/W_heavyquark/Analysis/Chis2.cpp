#include <TH2.h>
#include <TStyle.h>
#include "TMath.h"

#include <iostream>
#include <fstream>
#include <vector>
#include "AlgoWZ.hpp"
using namespace algowz;
double CalChi2_1D(TH1D* h1, TH1D* h2, bool printout = false){
  // make sure they have the same binnings
  Int_t nbins = h1->GetNbinsX();
  if(h2->GetNbinsX()!=nbins){
    cout<<" *** Chi^2 calculation: different number of bins for h1 and h2 templates"<<endl;
    cout<<"     The number of bins in h1 is "<<nbins<<";"<<endl;
    cout<<"     The number of bins in h2 is "<<h2->GetNbinsX()<<" !!"<<endl;
  }
  h1->Scale(1.0 / h1->Integral());
  h2->Scale(1.0 / h2->Integral()); 
  double chi2 = 0.;
  for(int ibin = 1; ibin < nbins+1; ibin++) {
    double error_data = h1->GetBinError(ibin)*h1->GetBinError(ibin) + h2->GetBinError(ibin)*h2->GetBinError(ibin);
    if(error_data > 0){
      double chi2_tmp = pow(h1->GetBinContent(ibin) - h2->GetBinContent(ibin), 2)/(error_data);
      if(printout) cout<<"   ibin "<<ibin<<" , Chi2 = "<<chi2_tmp<<endl;
      chi2 += chi2_tmp;
    }
  }
  return chi2;
}





void Chis2(string type, string _number){

  //gROOT->ProcessLine(".x ../Function/lhcbStyle.C");
  string ofname;
  if(type.find("wp")!=string::npos){
  ofname = "./outputs/Chi2_Wp.txt";}else{
  ofname = "./outputs/Chi2_Wm.txt";    
  }
  ofstream ofs;
  ofs.open(ofname.c_str(), ios_base::app);
  //ofs <<"_number"<<_number<< "Chi2" << Chi2 << endl;
  TFile *File_Var = new TFile(Form("./80.%s/resbos%s.root",_number.c_str(),type.c_str()));
  TFile* File_WP    = TFile::Open("/EOS/jzli/W_Data/resbosWp_withoutbc.root");
  TFile* File_WM    = TFile::Open("/EOS/jzli/W_Data/resbosWm_withoutbc.root");
  TH1D *Hist_Q_PT = (TH1D*)File_Var->Get("Hist_Q_PT");
  TH1D *Hist_MUONPT = (TH1D*)File_Var->Get("ResBos_muon_PT");
  TH1D *Hist_NPT = (TH1D*)File_Var->Get("ResBos_n_PT");
  TH1D *Hist_WPT = (TH1D*)File_Var->Get("ResBos_WBoson_PT");
  TH1D *Hist_MT = (TH1D*)File_Var->Get("Hist_MT");

  TH1D *Hist_Q_PT_Wm = (TH1D*)File_WP->Get("Hist_Q_PT");
  TH1D *Hist_MUONPT_Wm = (TH1D*)File_WP->Get("ResBos_muon_PT");
  TH1D *Hist_NPT_Wm = (TH1D*)File_WP->Get("ResBos_n_PT");
  TH1D *Hist_WPT_Wm = (TH1D*)File_WP->Get("ResBos_WBoson_PT");
  TH1D *Hist_MT_Wm = (TH1D*)File_WP->Get("Hist_MT");

  TH1D *Hist_Q_PT_Wp = (TH1D*)File_WM->Get("Hist_Q_PT");
  TH1D *Hist_MUONPT_Wp = (TH1D*)File_WM->Get("ResBos_muon_PT");
  TH1D *Hist_NPT_Wp = (TH1D*)File_WM->Get("ResBos_n_PT");
  TH1D *Hist_WPT_Wp = (TH1D*)File_WM->Get("ResBos_WBoson_PT");
  TH1D *Hist_MT_Wp = (TH1D*)File_WM->Get("Hist_MT");

  double Qpt_chi2,MuonPT_chi2,Npt_chi2,Wpt_chi2,Mt_chi2;

  if(type.find("wm")!=string::npos){
  Qpt_chi2 = CalChi2_1D(Hist_Q_PT_Wm, Hist_Q_PT);
  MuonPT_chi2 = CalChi2_1D(Hist_MUONPT_Wm, Hist_MUONPT);
  Npt_chi2 = CalChi2_1D(Hist_NPT_Wm, Hist_NPT); 
  Wpt_chi2 = CalChi2_1D(Hist_WPT_Wm, Hist_WPT);
  Mt_chi2 = CalChi2_1D(Hist_MT_Wm, Hist_MT);
  }
  if(type.find("wp")!=string::npos){
  Qpt_chi2 = CalChi2_1D(Hist_Q_PT_Wp, Hist_Q_PT);
  MuonPT_chi2 = CalChi2_1D(Hist_MUONPT_Wp, Hist_MUONPT);
  Npt_chi2 = CalChi2_1D(Hist_NPT_Wp, Hist_NPT); 
  Wpt_chi2 = CalChi2_1D(Hist_WPT_Wp, Hist_WPT);
  Mt_chi2 = CalChi2_1D(Hist_MT_Wp, Hist_MT);
  }
  


  ofs <<_number<<"\t"<< Qpt_chi2 <<"\t"<< MuonPT_chi2<<"\t"<< Npt_chi2<<"\t"<< Wpt_chi2<<"\t"<< Mt_chi2<<endl;
  //ofs <<_number<<"\t"<< MuonPT_chi2 << endl;
  //ofs <<_number<<"\t"<< Npt_chi2 << endl;
  //ofs <<""<<_number<<"\t"<< Wpt_chi2 << endl;
  //ofs <<""<<_number<<"\t"<< Mt_chi2 << endl;  
  ofs.close();


}
