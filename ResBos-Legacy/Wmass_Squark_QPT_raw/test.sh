WORKDIR=$PWD
cd $WORKDIR
# Function to process a single iteration
process_iteration() {
    k=$1
    WMassDIR=$[335+${k}]
    NewWMass=$WMassDIR
    mkdir $WMassDIR
    cp Ana_new_resbos_Wm.cpp AlgoWZ.hpp $WORKDIR/$WMassDIR
    cd $WORKDIR/$WMassDIR
    sed -i "s/NewWMass/$WMassDIR/g"  Ana_new_resbos_Wm.cpp
    for sample in wm Wm_withoutbc wp Wp_withoutbc 
    do
        root -q -b -l Ana_new_resbos_Wm.cpp\(\"${sample}\"\) | tee log_${sample}
    done
}

export -f process_iteration

# Run the iterations in parallel using xargs
seq 1 100 | xargs -I {} -P 100 bash -c "process_iteration {}"

