import ROOT
from ROOT import *
import numpy as np
from scipy.interpolate import interp1d
from ROOT import vector


# 打开ROOT文件
file = ROOT.TFile("Chi2.root")

# 访问ROOT文件中的树
tree = file.Get("tree")

# 创建变量一和变量二的变量句柄
_number = ROOT.std.vector('double')()
MuonPT_chi2 = ROOT.std.vector('double')()

# 将树中的变量一和变量二读入到变量句柄中
tree.SetBranchAddress("_number", _number)
tree.SetBranchAddress("MuonPT_chi2", MuonPT_chi2)
print("Number of entries in tree:", tree.GetEntries())

# 将变量一和变量二转换为NumPy数组
x = np.array([_number[i] for i in range(tree.GetEntries())])
y = np.array([MuonPT_chi2[i] for i in range(tree.GetEntries())])
print("Length of x:", len(x))
print("Length of y:", len(y))

# 创建插值函数
interp_func = interp1d(x, y, kind='linear')  # 这里使用线性插值，可以根据需要选择其他插值方法

# 定义插值的新数据点
x_interp = np.linspace(x.min(), x.max(), num=100)  # 在原始数据范围内生成新的数据点

# 进行插值
y_interp = interp_func(x_interp)


# 创建图形并绘制数据线
plt.plot(x_interp, y_interp, 'b-', label='Interpolation')

# 绘制原始数据点
plt.plot(x, y, 'ro', label='Data Points')

# 添加标题和标签
plt.title('Interpolation')
plt.xlabel('Variable One')
plt.ylabel('Variable Two')

# 添加图例
plt.legend()

# 保存图形为PDF文件
plt.savefig('interpolation_result.pdf')

# 显示图形（可选）
plt.show()
