//#include "Function.h"

#include <map>
#include <vector>
#include "RooAbsReal.h"
#include "RooAbsCategory.h"
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include "TObject.h"
#include "TCut.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TFile.h"
#include "TTree.h"
#include "TPostScript.h"
#include "TCanvas.h"
#include "TNtuple.h"
#include "TChain.h"
#include "TVector3.h"
#include "TLorentzVector.h"
#include "TPaveText.h"
#include "RooAbsPdf.h"
#include "RooFFTConvPdf.h"
#include "RooRealVar.h"
#include "RooNovosibirsk.h"
#include "RooCBShape.h"
#include "RooGaussian.h"
#include "RooChebychev.h"
#include "RooDataHist.h"
#include "RooDataSet.h"
#include "RooArgList.h"
#include "RooAddPdf.h"
#include "RooPlot.h"
#include <math.h>
#define PI acos(-1)
#include "RooHist.h"
#include "RooNLLVar.h"
#include "RooMinuit.h"
#include "RooFitResult.h"
#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif

using namespace RooFit;
void mergehist1(){
  gROOT->ProcessLine(".x ~/bins/.lhcbStyle.C");
 TFile* fileData = new TFile("/home/jzli/singlewz_generators/ResBos/tupleReader/outputs/ZMuMu_ZBoson_test.root");
 TH1D* ResBos_ZBoson_PT = (TH1D*) fileData->Get("ResBos_ZBoson_PT");
 TH1D* ResBos_ZBoson_Mass = (TH1D*) fileData->Get("ResBos_ZBoson_Mass");
 TH1D* ResBos_ZBoson_Rapidity = (TH1D*) fileData->Get("ResBos_ZBoson_Rapidity");
 TH1D* ResBos_ZBoson_Phistar = (TH1D*) fileData->Get("ResBos_ZBoson_Phistar");

 ResBos_ZBoson_Mass->SetLineColor(kBlue);
 ResBos_ZBoson_PT->SetLineColor(kBlue);
 ResBos_ZBoson_Phistar->SetLineColor(kBlue);
 ResBos_ZBoson_Rapidity->SetLineColor(kBlue);
 ResBos_ZBoson_Mass->SetXTitle("Zmass");
 ResBos_ZBoson_PT->SetXTitle("Z_PT");
 ResBos_ZBoson_Rapidity->SetXTitle("Z_Rapidity");
 ResBos_ZBoson_Phistar->SetXTitle("#Phi^{*}");
 TCanvas* c1 = new TCanvas("c1", "c1", 700, 500);
 ResBos_ZBoson_Mass->Draw("same");
 TCanvas* c2 = new TCanvas("c2", "", 700, 500);
 gPad->SetLogx();
 gPad->SetLogy();
 ResBos_ZBoson_PT->Draw("same");
 TCanvas* c3 = new TCanvas("c3", "", 700, 500);
 ResBos_ZBoson_Rapidity->Draw("same");
 TCanvas* c4 = new TCanvas("c4", "c4", 700, 500);
 gPad->SetLogx();
 gPad->SetLogy();
 ResBos_ZBoson_Phistar->Draw("same");




 /*mu_phi_truestar->Draw();
 mu_phi_star->Draw("same");
 mu_phi_truestar->SetXTitle("#Phi^{*}");
 TLegend* leg8 = new TLegend(0.75,0.65,0.68,0.85);
 leg8->SetTextFont(132);
 leg8->SetTextSize(0.06);
 leg8->SetBorderSize(2);
 leg8->SetFillColor(0);
 leg8->AddEntry(mu_phi_star,"MC","L");
 leg8->AddEntry(mu_phi_truestar,"ResBos+CT18","L");
 leg8->Draw("same");*/
 TLegend* leg8 = new TLegend(0.75,0.65,0.68,0.85);
 leg8->SetTextFont(132);
 leg8->SetTextSize(0.06);
 leg8->SetBorderSize(2);
 leg8->SetFillColor(0);
 leg8->AddEntry(ResBos_ZBoson_Mass,"ResBos+CT18","L");

 TLegend* leg3 = new TLegend(0.75,0.65,0.68,0.85);
 leg3->SetTextFont(132);
 leg3->SetTextSize(0.06);
 leg3->SetBorderSize(2);
 leg3->SetFillColor(0);
 leg3->AddEntry(ResBos_ZBoson_Phistar,"ResBos+CT18","L");
 leg3->Draw("same");

 TLegend* leg2 = new TLegend(0.75,0.65,0.68,0.85);
 leg2->SetTextFont(132);
 leg2->SetTextSize(0.06);
 leg2->SetBorderSize(2);
 leg2->SetFillColor(0);
 leg2->AddEntry(ResBos_ZBoson_Rapidity,"ResBos+CT18","L");
 leg2->Draw("same");

 TLegend* leg1 = new TLegend(0.75,0.65,0.68,0.85);
 leg1->SetTextFont(132);
 leg1->SetTextSize(0.06);
 leg1->SetBorderSize(2);
 leg1->SetFillColor(0);
 leg1->AddEntry(ResBos_ZBoson_PT,"ResBos+CT18","L");
 leg1->Draw("same");
 ResBos_ZBoson_Mass->Scale(1.0,"width");
 ResBos_ZBoson_PT->Scale(1.0,"width");
 ResBos_ZBoson_Rapidity->Scale(1.0,"width");
 ResBos_ZBoson_Phistar->Scale(1.0,"width");

 TCanvas* c6 = new TCanvas("c6", "LOGX && LOGY", 1400, 1000);
 c6->Divide(2,2);
 c6->cd(1);
 ResBos_ZBoson_Mass->Draw("same");
 leg8->Draw("same");
 c6->cd(2);
 gPad->SetLogx();
 gPad->SetLogy();
 ResBos_ZBoson_PT->Draw("same");
 leg1->Draw("same"); 
 c6->cd(3);
 ResBos_ZBoson_Rapidity->Draw("same");
 leg2->Draw("same");
 c6->cd(4); 
 gPad->SetLogx();
 gPad->SetLogy();
 ResBos_ZBoson_Phistar->Draw("same");
 leg3->Draw("same");
 TCanvas* c5 = new TCanvas("c5", "LOGX", 1400, 1000);
 c5->Divide(2,2);
 c5->cd(1);

 ResBos_ZBoson_Mass->Draw("same");
 leg8->Draw("same");
 c5->cd(2);
 gPad->SetLogx();
 ResBos_ZBoson_PT->Draw("same");
 leg1->Draw("same"); 
 c5->cd(3);
 ResBos_ZBoson_Rapidity->Draw("same");
 leg2->Draw("same");
 c5->cd(4); 
 gPad->SetLogx();
 ResBos_ZBoson_Phistar->Draw("same");
 leg3->Draw("same");

c5->Print("resbos.pdf");


}

