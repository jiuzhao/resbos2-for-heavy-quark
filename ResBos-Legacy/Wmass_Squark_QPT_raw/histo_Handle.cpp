//<--------------------------------------------------->//
//                                                     //
//  histo_Handle.cpp:                                  //
//     The codes used to fill TH1, TH2, TProfile,      //
//     save histograms into a root file,               //
//     also some codes to Clone() a histogram          //
//     during job running.                             //
//                                                     //
//  Version: V.0.0                                     //
//     create functions for the following purposes,    //
//      1) save histograms into a root file            //
//      2) clone histogram in the codes                //
//      3) init histograms with different binning      //
//                                                     //
//                    Hang Yin (hyin@cern.ch)          //
//                    6.12.2015                        //
//                                                     //
//<--------------------------------------------------->//
#include "TH1F.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TProfile.h" 
#include "TProfile2D.h" 
#include "TFile.h"
#include <string>
#include <vector>
#include <iostream>
#include "histo_Handle.hpp" 

using namespace std; 

//---------------------------------------------------------------------
//
//   Save all histogram and close the file
//
//---------------------------------------------------------------------
void histo_Handle::saveHistograms(const string& fileName){
  TFile histf(fileName.c_str(),"RECREATE"); 
  histf.cd(); 
  for(vector<TH1D*>::iterator i = th1fs.begin(); i != th1fs.end(); ++i){
    (*i)->Write();
    delete (*i);
  }
  for(vector<TH2D*>::iterator i = th2fs.begin(); i != th2fs.end(); ++i){
    (*i)->Write();
    delete (*i);
  }
  for(vector<TH3D*>::iterator i = th3fs.begin(); i != th3fs.end(); ++i){
    (*i)->Write();
    delete (*i);
  }
  for(vector<TProfile*>::iterator i = tprofs.begin(); i != tprofs.end(); ++i){
    (*i)->Write();
    delete (*i);
  }
  for(vector<TProfile2D*>::iterator i = tprof2ds.begin(); i != tprof2ds.end(); ++i){ 
    (*i)->Write();
    delete (*i);
  }
  histf.Close();  
}

//---------------------------------------------------------------------
//
//   Save all histogram and close the file
//
//---------------------------------------------------------------------
void histo_Handle::cloneHistogram(TH1D*& h_new, string histname){
  for(vector<TH1D*>::iterator i = th1fs.begin(); i != th1fs.end(); ++i){
    string name = (*i)->GetName();
    if(name == histname) h_new = (TH1D*)((*i)->Clone());
  }
}

void histo_Handle::cloneHistogram(TH2D*& h_new, string histname){
  for(vector<TH2D*>::iterator i = th2fs.begin(); i != th2fs.end(); ++i){
    string name = (*i)->GetName();
    if(name == histname) h_new = (TH2D*)((*i)->Clone());
  }
}

void histo_Handle::cloneHistogram(TH3D*& h_new, string histname){
  for(vector<TH3D*>::iterator i = th3fs.begin(); i != th3fs.end(); ++i){
    string name = (*i)->GetName();
    if(name == histname) h_new = (TH3D*)((*i)->Clone());
  }
}

//---------------------------------------------------------------------
//
//   Method of registration of TH object by giving a name etc
//
//---------------------------------------------------------------------
TH1D* histo_Handle::AddHist1D(std::string name, std::string title, std::string xaxis, 
			      std::string yaxis, int nbins, double xmin, double xmax){
  TH1D* h = new TH1D(name.c_str(), title.c_str(), nbins, xmin, xmax);
  h->Sumw2();
  h->SetLineWidth(1);
  h->GetXaxis()->SetTitle(xaxis.c_str());
  h->GetXaxis()->SetTitleOffset(0.85);
  h->GetXaxis()->SetTitleSize(0.05);
  //  h->GetXaxis()->CenterTitle();
  h->GetYaxis()->SetTitle(yaxis.c_str());
  h->GetYaxis()->SetTitleOffset(0.85);
  h->GetYaxis()->SetTitleSize(0.05);
  h->GetYaxis()->CenterTitle();
  th1fs.push_back(h); 
  return h;
}

TH1D* histo_Handle::AddHist1D(std::string name, std::string title, std::string xaxis, 
			      std::string yaxis, int nbins, const double *xvalues){
  TH1D* h = new TH1D(name.c_str(), title.c_str(), nbins, xvalues);
  h->Sumw2();
  h->SetLineWidth(1);
  h->GetXaxis()->SetTitle(xaxis.c_str());
  h->GetXaxis()->SetTitleOffset(0.85);
  h->GetXaxis()->SetTitleSize(0.05);
  //  h->GetXaxis()->CenterTitle();
  h->GetYaxis()->SetTitle(yaxis.c_str());
  h->GetYaxis()->SetTitleOffset(0.85);
  h->GetYaxis()->SetTitleSize(0.05);
  h->GetYaxis()->CenterTitle();
  th1fs.push_back(h); 
  return h;
}

TH2D* histo_Handle::AddHist2D(std::string name, std::string title, std::string xaxis, 
			      std::string yaxis, int nbinx, double xmin, double xmax, 
			      int nbiny, double ymin, double ymax){
  TH2D* h=new TH2D(name.c_str(), title.c_str(), nbinx, xmin, xmax, nbiny, ymin, ymax);
  h->Sumw2();
  h->SetLineWidth(1);
  h->GetXaxis()->SetTitle(xaxis.c_str());
  h->GetXaxis()->SetTitleOffset(0.85);
  h->GetXaxis()->SetTitleSize(0.05);
  //  h->GetXaxis()->CenterTitle();
  h->GetYaxis()->SetTitle(yaxis.c_str());
  h->GetYaxis()->SetTitleOffset(0.85);
  h->GetYaxis()->SetTitleSize(0.05);
  h->GetYaxis()->CenterTitle();
  th2fs.push_back(h);
  return h; 
}

TH2D* histo_Handle::AddHist2D(std::string name, std::string title, std::string xaxis, 
			      std::string yaxis, int nbinx, const double *xvalues, 
			      int nbiny, const double *yvalues ){
  TH2D* h=new TH2D(name.c_str(), title.c_str(), nbinx, xvalues, nbiny, yvalues);
  h->Sumw2();
  h->SetLineWidth(1);
  h->GetXaxis()->SetTitle(xaxis.c_str());
  h->GetXaxis()->SetTitleOffset(0.85);
  h->GetXaxis()->SetTitleSize(0.05);
  //  h->GetXaxis()->CenterTitle();
  h->GetYaxis()->SetTitle(yaxis.c_str());
  h->GetYaxis()->SetTitleOffset(0.85);
  h->GetYaxis()->SetTitleSize(0.05);
  h->GetYaxis()->CenterTitle();
  th2fs.push_back(h);
  return h; 
}

TH2D* histo_Handle::AddHist2D(std::string name, std::string title, std::string xaxis, 
			      std::string yaxis, int nbinx, double xmin, double xmax, 
			      int nbiny, const double* yvalues ){
  TH2D* h=new TH2D(name.c_str(), title.c_str(), nbinx, xmin, xmax, nbiny, yvalues);
  h->Sumw2();
  h->SetLineWidth(1);
  h->GetXaxis()->SetTitle(xaxis.c_str());
  h->GetXaxis()->SetTitleOffset(0.85);
  h->GetXaxis()->SetTitleSize(0.05);
  //  h->GetXaxis()->CenterTitle();
  h->GetYaxis()->SetTitle(yaxis.c_str());
  h->GetYaxis()->SetTitleOffset(0.85);
  h->GetYaxis()->SetTitleSize(0.05);
  h->GetYaxis()->CenterTitle();
  th2fs.push_back(h);
  return h; 
}

TH2D* histo_Handle::AddHist2D(std::string name, std::string title, std::string xaxis, 
			      std::string yaxis, int nbinx, const double* xvalues, 
			      int nbiny, double ymin, double ymax){
  TH2D* h=new TH2D(name.c_str(), title.c_str(), nbinx, xvalues, nbiny, ymin, ymax);
  h->Sumw2();
  h->SetLineWidth(1);
  h->GetXaxis()->SetTitle(xaxis.c_str());
  h->GetXaxis()->SetTitleOffset(0.85);
  h->GetXaxis()->SetTitleSize(0.05);
  //  h->GetXaxis()->CenterTitle();
  h->GetYaxis()->SetTitle(yaxis.c_str());
  h->GetYaxis()->SetTitleOffset(0.85);
  h->GetYaxis()->SetTitleSize(0.05);
  h->GetYaxis()->CenterTitle();
  th2fs.push_back(h);
  return h; 
}

TH3D* histo_Handle::AddHist3D(std::string name, std::string title, 
			      std::string xaxis, std::string yaxis, std::string zaxis, 
			      int nbinx, const double *xvalues, 
			      int nbiny, const double *yvalues, 
			      int nbinz, const double *zvalues){
  TH3D* h=new TH3D(name.c_str(), title.c_str(), nbinx, xvalues, nbiny, yvalues, nbinz, zvalues);
  h->Sumw2();
  h->SetLineWidth(1);
  h->GetXaxis()->SetTitle(xaxis.c_str());
  h->GetXaxis()->SetTitleOffset(0.85);
  h->GetXaxis()->SetTitleSize(0.05);
  //  h->GetXaxis()->CenterTitle();
  h->GetYaxis()->SetTitle(yaxis.c_str());
  h->GetYaxis()->SetTitleOffset(0.85);
  h->GetYaxis()->SetTitleSize(0.05);
  h->GetYaxis()->CenterTitle();
  h->GetZaxis()->SetTitle(zaxis.c_str());
  h->GetZaxis()->SetTitleOffset(0.85);
  h->GetZaxis()->SetTitleSize(0.05);
  h->GetZaxis()->CenterTitle();
  th3fs.push_back(h);
  return h;
}

TProfile* histo_Handle::AddProf(std::string name, std::string title, std::string xaxis, std::string yaxis, 
				int nbinx, double xmin, double xmax, 
				double ymin, double ymax){
  TProfile* h = new TProfile(name.c_str(), title.c_str(), nbinx, xmin, xmax, ymin, ymax);
  h->Sumw2();
  h->SetLineWidth(1);
  h->GetXaxis()->SetTitle(xaxis.c_str());
  h->GetXaxis()->SetTitleOffset(0.85);
  h->GetXaxis()->SetTitleSize(0.05);
  //  h->GetXaxis()->CenterTitle();
  h->GetYaxis()->SetTitle(yaxis.c_str());
  h->GetYaxis()->SetTitleOffset(0.85);
  h->GetYaxis()->SetTitleSize(0.05);
  h->GetYaxis()->CenterTitle();
  tprofs.push_back(h); 
  return h;
}

TProfile* histo_Handle::AddProf(std::string name, std::string title, std::string xaxis, 
				std::string yaxis, int nbinx, const double *xvalues, 
				double ymin, double ymax){
  TProfile* h = new TProfile(name.c_str(), title.c_str(), nbinx, xvalues, ymin, ymax);
  h->Sumw2();
  h->SetLineWidth(1);
  h->GetXaxis()->SetTitle(xaxis.c_str());
  h->GetXaxis()->SetTitleOffset(0.85);
  h->GetXaxis()->SetTitleSize(0.05);
  //  h->GetXaxis()->CenterTitle();
  h->GetYaxis()->SetTitle(yaxis.c_str());
  h->GetYaxis()->SetTitleOffset(0.85);
  h->GetYaxis()->SetTitleSize(0.05);
  h->GetYaxis()->CenterTitle();
  tprofs.push_back(h);
  return h;
}

TProfile2D* histo_Handle::AddProf2D(std::string name, std::string title, int nbinx, 
				    double xmin, double xmax, int nbiny,  double ymin, 
				    double ymax, double zmin, double zmax){
  TProfile2D* h = new TProfile2D(name.c_str(), title.c_str(), nbinx, xmin, xmax, nbiny, ymin, ymax, zmin, zmax);
  h->Sumw2();
  tprof2ds.push_back(h); 
  return h;
}

TProfile2D* histo_Handle::AddProf2D(std::string name, std::string title, int nbinx, 
				    const double *xvalues, int nbiny, 
				    const double *yvalues, double zmin, double zmax){ 
  TProfile2D* h = new TProfile2D(name.c_str(), title.c_str(), nbinx, xvalues, nbiny, yvalues); 
  h->Sumw2();
  tprof2ds.push_back(h);
  return h;
} 

//---------------------------------------------------------------------
//
//   Method of getting TH object by name to fill
//
//---------------------------------------------------------------------
TH1D* histo_Handle::getTH1DptrByName(std::string name){
  for(vector<TH1D*>::const_iterator i = th1fs.begin(); i != th1fs.end(); ++i){
    if(strcmp(name.c_str(), (*i)->GetName()) == 0) return (*i);
  }
  return 0; 
}

TH2D* histo_Handle::getTH2DptrByName(std::string name){
  for(vector<TH2D*>::const_iterator i = th2fs.begin(); i != th2fs.end(); ++i){
    if(strcmp(name.c_str(), (*i)->GetName()) == 0) return (*i);
  }
  return 0; 
}

TH3D* histo_Handle::getTH3DptrByName(std::string name){
  for(vector<TH3D*>::const_iterator i = th3fs.begin(); i != th3fs.end(); ++i){
    if(strcmp(name.c_str(), (*i)->GetName()) == 0) return (*i);
  }
  return 0;
}

TProfile* histo_Handle::getTProfptrByName(std::string name){
  for(vector<TProfile*>::const_iterator i = tprofs.begin(); i != tprofs.end(); ++i){
    if(strcmp(name.c_str(), (*i)->GetName()) == 0) return (*i);
  }
  return 0; 
}

TProfile2D* histo_Handle::getTProf2DptrByName(std::string name){
  for(vector<TProfile2D*>::const_iterator i = tprof2ds.begin(); i != tprof2ds.end(); ++i){
    if(strcmp(name.c_str(), (*i)->GetName()) == 0) return (*i);
  }
  return 0;
}
