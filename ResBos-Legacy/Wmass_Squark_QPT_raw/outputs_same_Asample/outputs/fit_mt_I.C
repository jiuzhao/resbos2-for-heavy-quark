#include <TChain.h>
#include <TCanvas.h>
#include <TGraph.h>

void fit_mt_I()
{
    // Open the ROOT file
    TChain* ch = new TChain();
    ch->Add("Chi2.root/tree");
    float _number, Qpt_chi2, MuonPT_chi2,Npt_chi2,Wpt_chi2,Mt_chi2;

    // Create arrays to store values
    const int maxEntries = ch->GetEntries();
    double xvalue[100];
    double yvalue[100];


    ch->SetBranchAddress("_number", &_number);  
    ch->SetBranchAddress("Qpt_chi2", &Qpt_chi2);
    ch->SetBranchAddress("MuonPT_chi2", &MuonPT_chi2);
    ch->SetBranchAddress("Npt_chi2", &Npt_chi2);
    ch->SetBranchAddress("Wpt_chi2", &Wpt_chi2);
    ch->SetBranchAddress("Mt_chi2", &Mt_chi2);

    int nPoints = 0;

    // Loop over entries and fill arrays
    for (int i = 0; i < ch->GetEntries(); i++) {
        ch->GetEntry(i);
        if (i > 0 && i < 100) {
            xvalue[nPoints] = _number / 1000 + 80;
            yvalue[nPoints] = Mt_chi2;
            nPoints++;
        }
    }

    cout << nPoints << endl;

    // Create a TGraph and set axes titles
    TGraph* gr = new TGraph(nPoints, xvalue, yvalue);
    gr->GetXaxis()->SetTitle("W mass GeV");
    gr->GetYaxis()->SetTitle("#chi^{2}");
    gr->SetTitle("m_{T}");

    // Perform linear interpolation
    TGraph* grInterpolated = new TGraph();
    int nPointsInterpolated = 10000; // Number of points for interpolation
    double step = (xvalue[nPoints - 1] - xvalue[0]) / (nPointsInterpolated - 1);
    for (int i = 0; i < nPointsInterpolated; i++) {
        double x = xvalue[0] + i * step;
        double y = gr->Eval(x, 0, "S");
        grInterpolated->SetPoint(i, x, y);
    }

// Find the local minimum point on the interpolated curve
double xMinInterpolated = 0.0;
double yMinInterpolated = grInterpolated->Eval(xvalue[0]);
for (int i = 1; i < nPointsInterpolated - 1; i++) {
    double x = grInterpolated->GetX()[i];
    double y = grInterpolated->GetY()[i];
    if (y < yMinInterpolated) {
        xMinInterpolated = x;
        yMinInterpolated = y;
    }
}

cout << "Interpolated Local Minimum point: x = " << xMinInterpolated << ", y = " << yMinInterpolated << endl;
// Find the intersection points between the horizontal line and the interpolated curve
vector<double> intersectionXs;
double intersectionY = yMinInterpolated + 1;

for (int i = 0; i < nPointsInterpolated - 1; i++) {
    double x1 = grInterpolated->GetX()[i];
    double y1 = grInterpolated->GetY()[i];
    double x2 = grInterpolated->GetX()[i + 1];
    double y2 = grInterpolated->GetY()[i + 1];

    if ((y1 <= intersectionY && y2 >= intersectionY) || (y1 >= intersectionY && y2 <= intersectionY)) {
        double intersectionX = x1 + (intersectionY - y1) * (x2 - x1) / (y2 - y1);
        intersectionXs.push_back(intersectionX);
    }
}

// Print the intersection points
cout << "Intersection points:" << endl;
for (const double& intersectionX : intersectionXs) {
    cout << "x = " << intersectionX << ", y = " << intersectionY << endl;
}
double sigma    = intersectionXs[1] - intersectionXs[0];
cout << "Difference between the x-values of the second and first intersection points: " <<intersectionXs[1] - intersectionXs[0] << endl;


    // Create a TCanvas
    TCanvas* c1 = new TCanvas("c1", "", 700, 500);

    // Draw the original curve
    gr->Draw("ALP");

    // Draw the interpolated curve
    grInterpolated->SetLineColor(kRed);
    grInterpolated->SetLineWidth(2);
    grInterpolated->Draw("LP");

    // Draw the minimum point on the interpolated curve
    TMarker* marker = new TMarker(xMinInterpolated, yMinInterpolated, 20);
    marker->SetMarkerColor(kRed);
    marker->Draw();
    std::string sigmaStr = std::to_string(sigma); 
    std::string xMinInterpolatedStr = std::to_string(xMinInterpolated); 
    TLegend* leg8 = new TLegend(0.15,0.65,0.28,0.85);
    leg8->SetTextFont(132);
    leg8->SetTextSize(0.04);
    leg8->SetBorderSize(2);
    leg8->SetTextColor(kBlue); 
    leg8->AddEntry(c1,("Mt_chi2 Best W mass is " + xMinInterpolatedStr +" +- "+ sigmaStr + " GeV ").c_str(),"");


    leg8->Draw("same");

    c1->Print("outputs/Mt_chi2.pdf");
}