# 读取data1.txt和data2.txt的内容
with open('Chi2_Wp.txt', 'r') as file1, open('Chi2_Wm.txt', 'r') as file2:
    lines1 = file1.readlines()
    lines2 = file2.readlines()

# 对数据进行处理
result_lines = []
for line1, line2 in zip(lines1, lines2):
    # 按照空格或制表符分割列数据
    data1 = line1.split()
    data2 = line2.split()

    # 将对应列相加
    sum_columns = [float(data1[i]) + float(data2[i]) for i in range(1, len(data1))]

    # 组合新的行数据
    result_line = data1[0] + ' ' + ' '.join(str(x) for x in sum_columns) + '\n'
    result_lines.append(result_line)

# 将结果写入out.txt文件
with open('out.txt', 'w') as outfile:
    outfile.writelines(result_lines)

