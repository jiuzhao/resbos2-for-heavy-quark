#include <TFile.h>
#include <TH1D.h>

void renameHistograms(const char* filename) {
    TFile* file = TFile::Open(filename, "UPDATE"); // 打开ROOT文件，并允许写入

    TH1D* ptHist = (TH1D*)file->Get("ResBos_ZBoson_PT_FSRRatio");
    if (ptHist) {
        ptHist->SetName("Hist_ZPT");
        ptHist->Write();
    }

    TH1D* rapidityHist = (TH1D*)file->Get("ResBos_ZBoson_Rapidity_FSRRatio");
    if (rapidityHist) {
        rapidityHist->SetName("Hist_ZRapidity");
        rapidityHist->Write();
    }

    TH1D* phiHist = (TH1D*)file->Get("ResBos_ZBoson_Phistar_FSRRatio");
    if (phiHist) {
        phiHist->SetName("Hist_ZPHI");
        phiHist->Write();
    }

    file->Close(); // 关闭ROOT文件
}

