double Ratio_Error(double a,double b, double a_err, double b_err){
  double part_1 = (a_err*a_err)/(b*b);
  double part_2 = (a*a*b_err*b_err)/(b*b*b*b);
  return TMath::Sqrt(part_1 + part_2);
}

void Make_Ratio(TH1D* hist_nume, TH1D* hist_deno, TH1D* hist, bool norma){

  if(norma){
    double n1_tot = hist_nume->Integral();
    double n2_tot = hist_nume->Integral();
    hist_deno -> Scale(n1_tot/n2_tot);
  }

  for(int i = 0; i < hist_deno->GetNbinsX()+2; i++){
    hist->SetBinContent(i, 0.);
    hist->SetBinError(i, 0.);

    double n_deno = hist_deno->GetBinContent(i);
    double n_nume = hist_nume->GetBinContent(i);
    double e_deno = hist_deno->GetBinError(i);
    double e_nume = hist_nume->GetBinError(i);

    double ratio = 1.;
    double error = 0.;
    if(n_deno > 0){
      ratio = n_nume/n_deno;
      error = Ratio_Error(n_nume, n_deno, e_nume, e_deno);
    }

    hist->SetBinContent(i, ratio);
    hist->SetBinError(i, error);
  }
}

void Make_Ratio(TH2D* hist_nume, TH2D* hist_deno, TH2D* hist, bool norma){

  if(norma){
    double n1_tot = hist_nume->Integral();
    double n2_tot = hist_nume->Integral();
    hist_deno -> Scale(n1_tot/n2_tot);
  }

  for(int i = 0; i < hist_deno->GetNbinsX()+2; i++){
    for(int j = 0; j < hist_deno->GetNbinsY()+2; j++){
      hist->SetBinContent(i, j, 0.);
      hist->SetBinError(i, j, 0.);

      double n_deno = hist_deno->GetBinContent(i, j);
      double n_nume = hist_nume->GetBinContent(i, j);
      double e_deno = hist_deno->GetBinError(i, j);
      double e_nume = hist_nume->GetBinError(i, j);

      double ratio = 1.;
      double error = 0.;
      if(n_deno > 0){
	ratio = n_nume/n_deno;
	error = Ratio_Error(n_nume, n_deno, e_nume, e_deno);
      }

      hist->SetBinContent(i, j, ratio);
      hist->SetBinError(i, j, error);
    }
  }
}


void Study_ZBoson_FSR(){
  gROOT->ProcessLine(".x ~/bins/.lhcbStyle.C");
  system("mkdir -p outputs");

  // read root files
  TH1D* hist_ratio[3];
  TFile* file_nofsr  = new TFile("../outputs/ZMuMu_ZBoson_NOFSR_FullSpace.root");
  TFile* file_fsr  = new TFile("../outputs/ZMuMu_ZBoson_FSR_FullSpace.root");

  vector<string> histnames; histnames.clear();
  vector<string> xtitles;   xtitles.clear();
  
  histnames.push_back("ResBos_ZBoson_PT");      xtitles.push_back("p_{T}^{Z} [GeV/#it{c}]");
  histnames.push_back("ResBos_ZBoson_Rapidity");    xtitles.push_back("ZBoson_Rapidity");
  histnames.push_back("ResBos_ZBoson_Phistar");  xtitles.push_back("#phi^{*}");
  // ------------------------------------------------------ //
  // ------------------------------------------------------ //
  // 1-D NOFSR/FSR ratio
  for(int i = 0; i < histnames.size(); i++){
    string histname = histnames[i];
    string xtitle   = xtitles[i];
    
    // get hists
    TH1D* hist_nofsr = (TH1D*) file_nofsr->Get(histname.c_str());
    TH1D* hist_fsr   = (TH1D*) file_fsr->Get(histname.c_str());
    
    //
    hist_nofsr->SetLineColor(2);
    hist_nofsr->SetMarkerColor(2);
    hist_fsr->SetLineColor(4);
    hist_fsr->SetMarkerColor(4);
    hist_nofsr->SetXTitle(xtitle.c_str());
    hist_nofsr->SetYTitle("Entries");
    hist_nofsr->GetYaxis()->CenterTitle(false);
    
    // draw
    TCanvas* c1 = new TCanvas("c1", "", 700, 500);
    TLegend* leg = new TLegend(0.5, 0.4, 0.7, 0.6);
    if(i==1) leg = new TLegend(0.2, 0.4, 0.4, 0.6);

    leg->AddEntry(hist_nofsr, "Born", "ep");
    leg->AddEntry(hist_fsr, "FSR", "ep");
    hist_nofsr->Draw();
    hist_fsr->Draw("same");
    leg->Draw("same");
    c1->Print(("outputs/"+histname+"_FSRComp.pdf").c_str());
    
    TCanvas* c2 = new TCanvas("c2", "", 700, 500);
    hist_ratio[i] = (TH1D*)(hist_nofsr->Clone());
    hist_ratio[i]->SetName((histname + "_FSRRatio").c_str());
    Make_Ratio(hist_nofsr, hist_fsr, hist_ratio[i], false);
    hist_ratio[i]->SetYTitle("Ratio");
    hist_ratio[i]->SetLineColor(1);
    hist_ratio[i]->SetMarkerColor(1);
    gPad->SetLogx();
//    hist_ratio[i]->GetXaxis()->SetLimits(1,300);
    hist_ratio[i]->Draw("");
    c2->Print(("outputs/"+histname+"_FSRRatio.pdf").c_str());
    
    delete c1;
    delete c2;
    delete hist_nofsr;
    delete hist_fsr;
    delete leg;
  }

  // ------------------------------------------------------ //
  // ------------------------------------------------------ //
  // 2-D NOFSR/FSR ratio
  // Eta-PT comparison
  string histname = "ResBos_ZBoson_RapidityPT";
  string xtitle   = "Z Rapidity";

  // get hists
  TH2D* hist_nofsr2 = (TH2D*) file_nofsr->Get(histname.c_str());
  TH2D* hist_fsr2   = (TH2D*) file_fsr->Get(histname.c_str());

  //
  hist_nofsr2->SetLineColor(2);
  hist_nofsr2->SetMarkerColor(2);
  hist_fsr2->SetLineColor(4);
  hist_fsr2->SetMarkerColor(4);
  hist_nofsr2->SetXTitle(xtitle.c_str());
  hist_nofsr2->SetYTitle("p_{T}^{Z} [GeV/#it{c}]");
  hist_nofsr2->GetYaxis()->CenterTitle(false);

  // draw
  TCanvas* c5 = new TCanvas("c5", "", 700, 500);
  TLegend* leg3 = new TLegend(0.7, 0.7, 0.9, 0.9);
  leg3->AddEntry(hist_nofsr2, "Born", "l");
  leg3->AddEntry(hist_fsr2, "FSR", "l");
  hist_nofsr2->Draw("BOX");
  hist_fsr2->Draw("same&&BOX");
  leg3->Draw("same");
  c5->Print(("outputs/"+histname+"_FSRComp.pdf").c_str());

  TCanvas* c6 = new TCanvas("c6", "", 700, 500);
  TH2D* hist_ratio2  = (TH2D*)(hist_nofsr2->Clone());
  hist_ratio2->SetName((histname + "_FSRRatio").c_str());
  Make_Ratio(hist_nofsr2, hist_fsr2, hist_ratio2, false);
  hist_ratio2->SetLineColor(1);
  hist_ratio2->SetMarkerColor(1);

  c6->SetRightMargin(0.15);
  hist_ratio2->Draw("COLZ");
  c6->Print(("outputs/"+histname+"_FSRRatio.pdf").c_str());

  // ------------------------------------------------------ //
  // ------------------------------------------------------ //
  // save into a root file
  TFile* file_new = new TFile("ZBoson_RESBOS_FSRWt.root", "recreate");
  
  for(int i = 0; i < histnames.size(); i++)
    hist_ratio[i]->Write();
  
//  hist_ratio2->Write();
  file_new->Close();
}
