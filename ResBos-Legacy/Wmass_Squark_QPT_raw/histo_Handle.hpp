//<--------------------------------------------------->//
//                                                     //
//  histo_Handle.hpp:                                  //
//     The codes used to fill TH1, TH2, TProfile,      //
//     save histograms into a root file,               //
//     also some codes to Clone() a histogram          //
//     during job running.                             //
//                                                     //
//  Version: V.0.0                                     //
//     create functions for the following purposes,    //
//      1) save histograms into a root file            //
//      2) clone histogram in the codes                //
//      3) init histograms with different binning      //
//                                                     //
//                    Hang Yin (hyin@cern.ch)          //
//                    6.12.2015                        //
//                                                     //
//<--------------------------------------------------->//
#ifndef __HISTO_HANDLE_HH__
#define __HISTO_HANDLE_HH__

#include "TH1F.h"
#include "TH2F.h" 
#include "TH3F.h" 
#include "TH1D.h"
#include "TH2D.h" 
#include "TH3D.h" 
#include "TProfile.h"
#include "TProfile2D.h" 

#include <string> 
#include <vector> 

class histo_Handle{
private:
  std::vector<TH1D*> th1fs;
  std::vector<TH2D*> th2fs; 
  std::vector<TH3D*> th3fs; 
  std::vector<TProfile*> tprofs;
  std::vector<TProfile2D*> tprof2ds; 

public:
  TH1D* AddHist1D(std::string name, std::string title, std::string xaixs, std::string yaxis, int nbinx, double xmin, double xmax); 
  TH1D* AddHist1D(std::string name, std::string title, std::string xaixs, std::string yaxis, int nbinx, const double *xvalues); 
  TH2D* AddHist2D(std::string name, std::string title, std::string xaixs, std::string yaxis, int nbinx, double xmin, double xmax, int nbiny, double ymin, double ymax); 
  TH2D* AddHist2D(std::string name, std::string title, std::string xaixs, std::string yaxis, int nbinx, const double *xvalues , int nbiny, const double *yvalues); 
  TH2D* AddHist2D(std::string name, std::string title, std::string xaixs, std::string yaxis, int nbinx, const double *xvalues , int nbiny, double ymin, double ymax); 
  TH2D* AddHist2D(std::string name, std::string title, std::string xaixs, std::string yaxis, int nbinx, double xmin, double xmax, int nbiny, const double *yvalues); 
  TH3D* AddHist3D(std::string name, std::string title, std::string xaixs, std::string yaxis, std::string zaxis, int nbinx, const double *xvalues, int nbiny, const double *yvalues, int nbinsz, const double *zvalues); 
 
  TProfile* AddProf(std::string name, std::string title, std::string xaixs, std::string yaxis, int nbinx, double xmin, double xmax, double ymin, double ymax); 
  TProfile* AddProf(std::string name, std::string title, std::string xaixs, std::string yaxis, int nbinx, const double *xvalues, double ymin, double ymax); 
  TProfile2D* AddProf2D(std::string name, std::string title, int nbinx, double xmin, double xmax, int nbiny, double ymin, double ymax, double zmin, double zmax);
  TProfile2D* AddProf2D(std::string name, std::string title, int nbinx, const double *xvalues, int nbiny, const double *yvalues, double zmin, double zmax);

  TH1D* getTH1DptrByName(std::string );
  TH2D* getTH2DptrByName(std::string );
  TH3D* getTH3DptrByName(std::string );
  TProfile* getTProfptrByName(std::string );
  TProfile2D* getTProf2DptrByName(std::string );   

  histo_Handle() {th1fs.clear(); th2fs.clear(); th3fs.clear(); tprofs.clear();}
  ~histo_Handle(){}; 
  void bookHistograms(); 
  void saveHistograms(const std::string& );
  void cloneHistogram(TH1D*& h_new, std::string name);
  void cloneHistogram(TH2D*& h_new, std::string name);
  void cloneHistogram(TH3D*& h_new, std::string name);
};
#endif
