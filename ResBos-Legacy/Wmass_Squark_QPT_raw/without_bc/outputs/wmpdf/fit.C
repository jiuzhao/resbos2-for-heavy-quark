

#include <TChain.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TF1.h>
#include <RooFit.h>
#include <RooRealVar.h>
#include <RooDataSet.h>
#include <RooPlot.h>
#include <RooPolynomial.h>



void fit()
{
    // Open the ROOT file

    TChain* ch = new TChain();
    ch->Add("Chi2_wm.root/tree");
    float _number, Qpt_chi2, MuonPT_chi2,Npt_chi2,Wpt_chi2,Mt_chi2;

    // Create arrays to store values
    const int maxEntries = ch->GetEntries();
    double xvalue[100];
    double yvalue[100];

    ch->SetBranchAddress("_number", &_number);  
    ch->SetBranchAddress("Qpt_chi2", &Qpt_chi2);
    ch->SetBranchAddress("MuonPT_chi2", &MuonPT_chi2);
    ch->SetBranchAddress("Npt_chi2", &Npt_chi2);
    ch->SetBranchAddress("Wpt_chi2", &Wpt_chi2);
    ch->SetBranchAddress("Mt_chi2", &Mt_chi2);
    int nPoints = 0;
    // Loop over entries and fill arrays
    for (int i = 0; i < ch->GetEntries(); i++) {
        ch->GetEntry(i);
        if (i > 40 && i <85 ) {
            xvalue[nPoints] = _number / 1000 + 80;
            yvalue[nPoints] = Qpt_chi2;
            nPoints++;

        }
        ///xvalue[i] = _number / 1000 + 80;
        //cout<<xvalue[i]<<endl;
        //yvalue[i] = Qpt_chi2;
    }
            cout<<nPoints<<endl;
    // Create a TCanvas
    TCanvas* c1 = new TCanvas("c1", "", 700, 500);

    // Create a TGraph and set axes titles
    TGraph* gr = new TGraph(nPoints, xvalue, yvalue);
    gr->GetXaxis()->SetTitle("W^{-} mass GeV");
    gr->GetYaxis()->SetTitle("#chi^{2}");
    gr->SetTitle();
    // Fit with pol-2
    TF1* func = new TF1("func", "pol2");
    func->SetLineColor(2);
    func->SetLineWidth(2);
    gr->Fit("func");

    // Print canvas to PDF



    // Get fitted results
    double p1 = func->GetParameter(1);
    double p2 = func->GetParameter(2);
    double p0 = func->GetParameter(0);
    double e1 = func->GetParError(1);
    double e2 = func->GetParError(2);
    double central = -1. * p1 / (2. * p2);
    double y0 = p2*central*central + p1*central + p0;
    double a,b,c,x1,x2;
    a = p2; b=p1; c=p0-y0-1;
    double discriminant = b * b - 4 * a * c;
    x1  = (-b - sqrt(discriminant)) / (2 * a);
    x2  = (-b + sqrt(discriminant)) / (2 * a);
    double sigma = x2-x1;  
    std::string centralStr = std::to_string(central);
    std::string sigmaStr = std::to_string(sigma);  
    // Draw the TGraph
    gr->Draw("");
    c1->SetTitle("Wp");
    TLegend* leg8 = new TLegend(0.15,0.65,0.28,0.85);
    leg8->SetTextFont(132);
    leg8->SetTextSize(0.04);
    leg8->SetBorderSize(2);
    leg8->SetTextColor(kBlue); 
    leg8->AddEntry(c1,("Qpt_chi2 Best W mass is " + centralStr +" +- "+ sigmaStr + " GeV ").c_str(),"");


    leg8->Draw("same");
    c1->Print("Qpt_chi2.pdf");

    double error = 0.5 * sqrt(pow(e1 / p2, 2) + pow(p1 * e2 / (p2 * p2), 2));
    cout << "Qpt_chi2 Best W mass is " << central << " x1 " << x1 << " x2 " << x2  << endl;

    // Cleanup

}
