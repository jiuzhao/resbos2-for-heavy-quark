#include <TChain.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TF1.h>
#include <RooFit.h>
#include <RooRealVar.h>
#include <RooDataSet.h>
#include <RooPlot.h>
#include <RooPolynomial.h>
//#include "/home/dqliu/lhcbstyle/defie_function.h"
#include "RooAbsReal.h"
#include "RooAbsCategory.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TFile.h"
#include "TTree.h"
#include "TPostScript.h"
#include "TCanvas.h"
#include "TNtuple.h"
#include "TChain.h"
#include "TVector3.h"
#include "TLorentzVector.h"
#include "TPaveText.h"
#include "RooAbsPdf.h"
#include "RooFFTConvPdf.h"
#include "RooRealVar.h"
#include "RooNovosibirsk.h"
#include "RooCBShape.h"
#include "RooGaussian.h"
#include "RooChebychev.h"
#include "RooDataHist.h"
#include "RooDataSet.h"
#include "RooArgList.h"
#include "RooAddPdf.h"
#include "RooPlot.h"
#include "RooHist.h"
#include "RooNLLVar.h"
#include "RooMinuit.h"
#include "RooFitResult.h"
using namespace RooFit;
void fitChi2()
{
    // Open the ROOT file
    TChain* ch = new TChain("tree");
    ch->Add("Chi2.root");

    float _number, Qpt_chi2, MuonPT_chi2, Npt_chi2, Wpt_chi2, Mt_chi2;

    // Set branch addresses
    ch->SetBranchAddress("_number", &_number);
    ch->SetBranchAddress("Qpt_chi2", &Qpt_chi2);
    ch->SetBranchAddress("MuonPT_chi2", &MuonPT_chi2);
    ch->SetBranchAddress("Npt_chi2", &Npt_chi2);
    ch->SetBranchAddress("Wpt_chi2", &Wpt_chi2);
    ch->SetBranchAddress("Mt_chi2", &Mt_chi2);

    const int maxEntries = ch->GetEntries();
    double xvalue[maxEntries];
    double yvalue[maxEntries];

    // Create RooRealVars for fitting
    RooRealVar *Qpt = new RooRealVar("Wmass","Wmass",80.330,80.440,"GeV/c^{2}");
    // RooRealVar y("Qpt_chi2", "#chi^{2}", 30, 100);
    RooDataSet *data = new RooDataSet("data","data",RooArgSet(*Qpt));



    // Fill TGraph with data points
    for (int i = 0; i < ch -> GetEntries(); i++) {
    ch -> GetEntry(i);
    *Qpt = Qpt_chi2;
    data->add(RooArgSet(*Qpt));
    }
    RooRealVar *c1       = new RooRealVar("c1", "c1", 0.1, -10, 10.);
    RooAbsPdf *bkgpdf2   = new RooPolynomial("bkgpdf2", "bkgpdf2", *Qpt, RooArgSet(*c1));


    RooDataHist *dhist_0 = data -> binnedClone();
    RooFitResult *fitr = bkgpdf2 -> fitTo(*dhist_0,Save());
    // framework for the plotting
    RooPlot* jframe = Qpt -> frame();
    // how to present the fitting results
    dhist_0->plotOn(jframe,Name("kaon_fit"),MarkerSize(0.8));
    bkgpdf2 -> plotOn(jframe,Components("bkgpdf2"),LineColor(kGreen));
    bkgpdf2 -> plotOn(jframe,Name("bkgpdf2"),LineColor(kGreen));
  // draw the plot
  TLatex *myTex = new TLatex(0.18,0.70,"#splitline{B^{0}#rightarrow K_{S}^{0} p #bar{p}}{17,18MC}");
  myTex->SetTextFont(132);
  myTex->SetTextSize(0.04751055);
  myTex->SetLineWidth(2);
  myTex->SetNDC();

  char name_input[200];


  jframe->addObject(myTex);

  jframe->SetTitle("");
  TCanvas *c_Data = new TCanvas("c_Data","",700,600);
  c_Data->Divide(1,2);
  c_Data->cd(2);
  //gPad->SetLogy();
  gPad->SetTopMargin(0);
  gPad->SetLeftMargin(0.12);
  gPad->SetPad(0.02,0.02,0.98,0.77);
  jframe -> SetTitle("");
  jframe->Draw();

  c_Data->cd(1);
  gPad->SetTopMargin(0);
  gPad->SetLeftMargin(0.12);
  gPad->SetPad(0.02,0.76,0.98,0.97);
  RooHist* hpull1 = jframe->pullHist();
  RooPlot* mframeh1 = Qpt->frame(Title("Pull distribution"));
  hpull1->SetFillColor(15);
  hpull1->SetFillStyle(3001);
  mframeh1->addPlotable(hpull1,"L3");
  mframeh1->GetYaxis()->SetNdivisions(505);
  mframeh1->GetYaxis()->SetLabelSize(0.20);
  mframeh1->SetMinimum(-5.0);
  mframeh1->SetMaximum(5.0);
  mframeh1->Draw();


}



