#include <TH2.h>
#include <TStyle.h>
#include "TMath.h"

#include <iostream>
#include <fstream>
#include <vector>
#include "AlgoWZ.hpp"
using namespace algowz;
/*
void EW_alphas(double AMU)
{
 double ALPHAS;

 double QCDL=0.326;
 double AL4=QCDL;
 double AL5=AL4 * pow((AL4/mB), (2.0/23.0)) * pow((log(mB*mB/AL4/AL4)), (-963.0/13225.0));
 double AL6=AL5 * pow((AL5/mT), (2.0/21.0)) * pow((log(mT*mT/AL5/AL5)), (-321.0/3381.0));

 double XRAT = AMU * AMU;

 double sqrt_xrat = AMU;

 double RAT;
 double B0;
 double B1;

 if(sqrt_xrat > mT){
   RAT=log(XRAT/AL6/AL6);
   B0=12.0*3.14159/21.0;
   B1=6.0*(153.0-19.0*6.0)/21.0/21.0;
 }
 else if(sqrt_xrat > mB){
   RAT=log(XRAT/AL5/AL5);
   B0=12.0*3.14159/23.0;
   B1=6.0*(153.0-19.0*5.0)/23.0/23.0;
 }
 else{
   RAT=log(XRAT/AL4/AL4);
   B0=12.0*3.14159/25.0;
   B1=6.0*(153.0-19.0*4.0)/25.0/25.0;
 }
 ALPHAS=B0/RAT*(1.0-B1*log(RAT)/RAT);

 return ALPHAS;
}


void GetWMassReweightingFactor(double Q, double OriginWMass, double 80.336){
 double GMU = 1.16637e-5;

 double SWS = 1 - (OriginWMass * OriginWMass) / (91.1876 * 91.1876);
 double AEM = sqrt(2.0) * OriginWMass * OriginWMass * GMU * SWS / TMath::Pi();
 double GFERMI = TMath::Pi() * AEM / sqrt(2.0) / SWS / OriginWMass / OriginWMass;
 double GAMW = GFERMI * pow(OriginWMass, 3) / 6.0 / TMath::Pi() / sqrt(2.0) * 9.0;
 double OriginGammaW = GAMW * (1.0 + 2.0 * EW->alphas(OriginWMass) / TMath::Pi() / 3.0);

 SWS = 1 - (80.336 * 80.336) / (91.1876 * 91.1876);
 AEM = sqrt(2.0) * 80.336 * 80.336 * GMU * SWS / TMath::Pi();
 GFERMI = TMath::Pi() * AEM / sqrt(2.0) / SWS / 80.336 / 80.336;
 GAMW = GFERMI * pow(80.336, 3) / 6.0 / TMath::Pi() / sqrt(2.0) * 9.0;
 double NewGammaW = GAMW * (1.0 + 2.0 * EW_alphas(80.336) / TMath::Pi() / 3.0);

 double Value = 1.0;

 if(WWidth < 0){
// GAMW_NLO_R
   double Numer = (Q * Q - OriginWMass * OriginWMass) * (Q * Q - OriginWMass * OriginWMass) + (Q * Q * Q * Q) / (OriginWMass * OriginWMass * OriginGammaW * OriginGammaW);
   double Denom = (Q * Q - 80.336 * 80.336) * (Q * Q - 80.336 * 80.336) + (Q * Q * Q * Q) / (80.336 * 80.336 * NewGammaW * NewGammaW);

   Value = Numer / Denom;
 }
 else{
//Fixed GammaW
   double GammaW = WWidth;

   double Numer = (Q * Q - OriginWMass * OriginWMass) * (Q * Q - OriginWMass * OriginWMass) + (Q * Q * Q * Q) / (OriginWMass * OriginWMass * GammaW * GammaW);
   double Denom = (Q * Q - 80.336 * 80.336) * (Q * Q - 80.336 * 80.336) + (Q * Q * Q * Q) / (80.336 * 80.336 * GammaW * GammaW);
//   double Denom = (Q * Q - 80.336 * 80.336) * (Q * Q - 80.336 * 80.336) + (Q * Q * Q * Q) / (80.336 * 80.336 * 2.084 * 2.084);

   Value = Numer / Denom;
 }

 return Value;
}*/









void Ana_new_resbos_Wm(string type){

  //gROOT->ProcessLine(".x ../Function/lhcbStyle.C");
  
  TChain* ch = new TChain();
  if(type.find("Wm_withoutbc")!=string::npos)
    ch->Add("/EOS/jzli/W_Data/Wm.root/h10");
    
  if(type.find("wm")!=string::npos)
    ch->Add("/EOS/jzli/W_Data/Wm_bc.root/h10");

  if(type.find("wp")!=string::npos)
    ch->Add("/EOS/jzli/W_Data/Wp_bc.root/h10");
  if(type.find("Wp_withoutbc")!=string::npos)
    ch->Add("/EOS/jzli/W_Data/Wp.root/h10");

  Float_t n_px, n_py, n_pz, n_pe;
  Float_t muon_px, muon_py, muon_pz, muon_pe;
  Float_t w_px  ,   w_py,   w_pz,   w_pe;
  Float_t wt;
  double WMassWeight;
  int n_counts;
  double n_pass = 0.;
  double n_evts = 0.;

// W+ ---> e+  n
// W- ---> e-   n 
  ch->SetBranchAddress("Px_d2", &n_px);
  ch->SetBranchAddress("Py_d2", &n_py);
  ch->SetBranchAddress("Pz_d2", &n_pz);  
  ch->SetBranchAddress("E_d2" , &n_pe);
  
  ch->SetBranchAddress("Px_d1", &muon_px);
  ch->SetBranchAddress("Py_d1", &muon_py);
  ch->SetBranchAddress("Pz_d1", &muon_pz);  
  ch->SetBranchAddress("E_d1" , &muon_pe);


  ch->SetBranchAddress("Px_V", &w_px);
  ch->SetBranchAddress("Py_V", &w_py);
  ch->SetBranchAddress("Pz_V", &w_pz);  
  ch->SetBranchAddress("E_V" , &w_pe);
  
  ch->SetBranchAddress("WT00" , &wt);

  TFile *File = new TFile(Form("./resbos%s.root",type.c_str()),"recreate");
  TTree* AnaTree = new TTree("AnaTree","AnaTree");
  TH1D *Hist_WM   = new TH1D("ResBos_WBoson_Mass", "",   60, 60, 100);
  TH1D *Hist_WY   = new TH1D("ResBos_WBoson_Rapidity",  "", nbins_wy, bins_wy);
  //TH1D *Hist_WPT  = new TH1D("ResBos_WBoson_PT", "",   nbins_wpt, bins_wpt);
  TH1D *Hist_WPT  = new TH1D("ResBos_WBoson_PT", "", 30,0,30);
  TH1D *Hist_Q_PT;
  if(type.find("wm")!=string::npos||type.find("Wm_withoutbc")!=string::npos){
  Hist_Q_PT = new TH1D("Hist_Q_PT","",100,-0.04,0);}
  if(type.find("wp")!=string::npos||type.find("Wp_withoutbc")!=string::npos){
  Hist_Q_PT = new TH1D("Hist_Q_PT","",100,0,0.04);}

  TH1D *Hist_MUONPT  = new TH1D("ResBos_muon_PT", "",  100, 20, 70);
  TH1D *Hist_NPT  = new TH1D("ResBos_n_PT", "",  100, 20, 70);
  TH1D *Hist_NM  = new TH1D("ResBos_N_M", "",  100, 0, 200);
  TH1D *Hist_MUONM  = new TH1D("ResBos_muon_M", "",  100, 0, 200);
  TH1D *Hist_testmuonpt  = new TH1D("Hist_testmuonpt", "",  30, 40, 50);

  TH1D *Hist_MT          = new TH1D("Hist_MT", "",  100, 65, 90);
  Hist_MT->Sumw2();
  Hist_NPT->Sumw2();
  Hist_MUONPT->Sumw2();
  Hist_Q_PT->Sumw2();
  Hist_WPT->Sumw2();
    // 创建 10 个 TH1D 直方图
    TH1D* hists[10];
    for (int i = 0; i < 10; i++) {
        int bin_low = 20 + i * 10;
        int bin_high = bin_low + 10;
        string hist_name = "hist_" + to_string(bin_low) + "_" + to_string(bin_high);
        hists[i] = new TH1D(hist_name.c_str(), Form("MuonPT %d", i), 30, bin_low, bin_high);
    }

  //Hist_WM->Sumw2();
  //Hist_WY->Sumw2();
  //Hist_WPT->Sumw2();  
  
  Float_t muon_pt, muon_eta,muon_m, n_pt, n_eta,n_m,mt;
  Float_t w_mass_reco, w_pt_reco, w_y_reco, phistar;
  AnaTree -> Branch("N_PX" ,&n_px ,"N_PX/D");
  AnaTree -> Branch("N_PY" ,&n_py ,"N_PY/D");
  AnaTree -> Branch("N_PZ" ,&n_pz ,"N_PZ/D");
  AnaTree -> Branch("N_PE" ,&n_pe ,"N_PE/D");    
  AnaTree -> Branch("N_PT" ,&n_pt ,"N_PT/D");
  AnaTree -> Branch("N_ETA",&n_eta,"N_ETA/D");

  AnaTree -> Branch("Muon_PX" ,&muon_px ,"Muon_PX/D");
  AnaTree -> Branch("Muon_PY" ,&muon_py ,"Muon_PY/D");
  AnaTree -> Branch("Muon_PZ" ,&muon_pz ,"Muon_PZ/D");
  AnaTree -> Branch("Muon_PE" ,&muon_pe ,"Muon_PE/D");    
  AnaTree -> Branch("Muon_PT" ,&muon_pt ,"Muon_PT/D");
  AnaTree -> Branch("Muon_ETA",&muon_eta,"Muon_ETA/D");

  AnaTree -> Branch("W_M"  , &w_mass_reco, "W_M/D");
  AnaTree -> Branch("W_PT" , &w_pt_reco  , "W_PT/D");
  AnaTree -> Branch("W_Y"  , &w_y_reco   , "W_Y/D");
  AnaTree -> Branch("W_PHI", &phistar    , "W_PHI/D");  
  AnaTree -> Branch("swits",  &wt        , "swits/D");

  
  for(int evts=0; evts < ch->GetEntries(); evts++){
    //    if(evts%1000000 == 0)  printf("Processing %2d events!\n", n_evts);
    if(n_counts%10000000 == 0)  printf("Processing %2d events!\n", n_counts);
    n_counts++;
    ch->GetEntry(evts); 

    // Initialization
    TLorentzVector muon, n, wboson;
    n.SetPxPyPzE(n_px, n_py, n_pz, n_pe);	
    muon.SetPxPyPzE(muon_px, muon_py, muon_pz, muon_pe);
    wboson.SetPxPyPzE(w_px, w_py, w_pz, w_pe);
    WMassWeight = GetWMassReweightingFactor((muon+n).M(), OriginWMass, 80.386) * wt;
    // Event Information
    n_pt     = n.Pt();
    n_eta    = n.Eta();
    n_m      = n.M();
    muon_pt     = muon.Pt();
    muon_eta    = muon.Eta();
    muon_m      = muon.M();
    mt          = (muon+n).Mt();
    w_mass_reco= (muon+n).M();
    w_pt_reco  = (muon+n).Pt();
    w_y_reco   = (muon+n).Rapidity();
    phistar    =  Cal_PhiStar(muon, n);  

    bool muon_lhcbacc = muon_eta > 2.0 && muon_eta < 4.5 && muon_pt > 0;
    bool n_lhcbacc = n_eta > 2.0 && n_eta < 4.5 && n_pt > 0;
    bool masscut = w_mass_reco > 60 && w_mass_reco < 120;

    // Selection
    //if(muon_lhcbacc && n_lhcbacc && muon_pt > 20 && n_pt > 0 && masscut){
    if(muon_lhcbacc && n_lhcbacc && muon_pt > 20){
      n_pass += (double)wt;
      // AnaTree->Fill();
      Hist_WM->Fill(w_mass_reco, WMassWeight);
      Hist_WY->Fill(w_y_reco, WMassWeight);
      Hist_WPT->Fill(w_pt_reco, WMassWeight);
      Hist_MUONPT->Fill(muon_pt,WMassWeight);
      Hist_NPT->Fill(n_pt,WMassWeight);
      Hist_MUONM->Fill(muon_m*1000);
      Hist_NM->Fill(n_m*1000);
      if(type.find("wm")!=string::npos||type.find("Wm_withoutbc")!=string::npos){
      Hist_Q_PT->Fill(-1/muon_pt,WMassWeight);}
      if(type.find("wp")!=string::npos||type.find("Wp_withoutbc")!=string::npos){
      Hist_Q_PT->Fill(1/muon_pt,WMassWeight);}
      Hist_testmuonpt->Fill(muon_pt,WMassWeight);
      Hist_MT->Fill(mt,WMassWeight);
       for (int i = 0; i < 10; i++) {
            int bin_low = 20 + i * 10;
            int bin_high = bin_low + 10;
        hists[i]->Fill(muon_pt,WMassWeight);    
        }


    }
    n_evts += (double)wt;
  }
  cout << endl;
  Float_t eff = (Float_t)n_pass/n_evts;

  cout << "===== For W " << type << " type  =====" << "80.386 +++++++"<< 80.386 <<endl;
  cout << "Before selection: " << n_evts << endl;
  cout << "After  selection: " << n_pass << endl;
  cout << "New ResBos eff  : " <<  eff << endl;

  File->Write();
  File->Close();
}
