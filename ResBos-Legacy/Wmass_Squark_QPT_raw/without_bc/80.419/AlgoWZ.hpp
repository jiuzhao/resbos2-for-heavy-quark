#ifndef _ALGOWZ_
#define _ALGOWZ_

#include <iostream>
#include <TLorentzVector.h>
#include <TROOT.h>
#include <TMath.h>

namespace algowz {
  double Collins_Soper(TLorentzVector mum, TLorentzVector mup);
  double Collins_Soper_Phi(TLorentzVector mum, TLorentzVector mup);
  double Collins_Soper_NewPhi(TLorentzVector electron, TLorentzVector positron);
  double Cal_PhiStar(TLorentzVector mum, TLorentzVector mup);
  double AFB_Err(double NF, double NB, double NF_err, double NB_err);
  double Efficiency_Error(double a, double b);
  double EW_alphas(double AMU);
  double GetWMassReweightingFactor(double Q, double OriginWMass, double NewWMass);
  void Make_AFB(TH1D* histF, TH1D* histB, TH1D* hist_afb);
  void Make_Eff(TH1D* hist_nume, TH1D* hist_deno, TH1D* hist);
  double GetChi2_OneDHists(TH1D* h1, TH1D* h2, int nmin, int nmax);
  const int nbins_muon_eta = 8;
  const double bins_muon_eta[nbins_muon_eta+1] = {2.0, 2.25, 2.50, 2.75, 3.0, 3.25, 3.50, 4.0, 4.5};

  const int nbins_muonpt_sel = 50;
  const double bins_muonpt_sel[nbins_muonpt_sel+1] = {20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70};
  const int nbins_muonpt = 43;
  const double bins_muonpt[nbins_muonpt+1] = {15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 52, 54, 56, 58, 60, 65, 70, 80};
  //  const int nbins_wpt = 66;
  //  const double bins_wpt[nbins_wpt+1] = {0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10, 10.5, 11, 11.5, 12, 12.5, 13, 13.5, 14, 14.5, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 33, 35, 37, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57, 60, 65, 70, 75, 80, 90, 100}; 
  const int nbins_wpt = 41;
  const double bins_wpt[nbins_wpt+1] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 27, 29, 31, 33, 35, 37, 39, 41, 43, 45, 50, 55, 60, 70, 80, 100};
  const int nnbins_z_rapidity = 18;
  const double nbins_z_rapidity[nnbins_z_rapidity+1] = {2.0, 2.125, 2.250, 2.375, 2.500, 2.625, 2.750, 2.875, 3.000, 3.125, 3.250, 3.375, 3.500, 3.625, 3.750, 3.875, 4.000, 4.250,4.5};
  const int nbins_pt = 14;
  double bins_pt[nbins_pt+1] = {0, 2.2, 3.4, 4.6, 5.8, 7.2, 8.7, 10.5, 12.8, 15.4, 19.0, 24.5, 34.0, 63.0, 270.0};
  const double mB = 4.75; 
  const double mT = 173.5;
  const double WWidth = 2.0895;
  const double OriginWMass =80.385;

  //const int nbins_wy = 27;
  //const double bins_wy[nbins_wy+1] = {1.5, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3, 3.1, 3.2, 3.3, 3.4, 3.5, 3.6, 3.7, 3.8, 3.9, 4, 4.1, 4.2, 4.3, 4.4, 4.5, 5.0};
  const int nbins_wy = 10;
  const double bins_wy[nbins_wy+1] = {1.5, 2.0, 2.25, 2.50, 2.75, 3.0, 3.25, 3.50, 4.0, 4.5, 5.0};

  const int nbins_eta_five = 5;
  const double bins_eta_five[nbins_eta_five+1] = {2.0, 2.5, 3.0, 3.5, 4.0, 4.5};
  const int nbins_eta = 25;
  const double bins_eta[nbins_eta+1] = {2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3, 3.1, 3.2, 3.3, 3.4, 3.5, 3.6, 3.7, 3.8, 3.9, 4, 4.1, 4.2, 4.3, 4.4, 4.5};
  //const int nbins_phistar = 79;
  //const double bins_phistar[nbins_phistar+1] = {0.0, 0.001, 0.002, 0.003, 0.004, 0.005, 0.006, 0.007, 0.008, 0.009, 0.010, 0.012, 0.014, 0.016, 0.018, 0.020, 0.022, 0.024, 0.026, 0.028, 0.030, 0.032, 0.034, 0.036, 0.038, 0.040, 0.045, 0.050, 0.055, 0.060, 0.065, 0.070, 0.075, 0.080, 0.085, 0.090, 0.095, 0.10, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.18, 0.20, 0.22, 0.24, 0.26, 0.28, 0.30, 0.32, 0.34, 0.36, 0.38, 0.40, 0.45, 0.50, 0.55, 0.60, 0.65, 0.70, 0.75, 0.80, 0.85, 0.90, 0.95, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.5, 3.0, 3.5, 4.0, 5.0, 6.0, 8.0};
  const int nbins_phistar= 15;
  double bins_phistar[nbins_phistar+1] = {0.00, 0.01, 0.02, 0.03, 0.05, 0.07, 0.10, 0.15, 0.20, 0.30, 0.40, 0.60, 0.80, 1.20, 2.00, 4.00};


  const int nbins_Zmass = 60;
  const double bins_Zmass[nbins_Zmass+1] = {60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120};
  const int nbins_Zmass_t = 40;
  const double bins_Zmass_t[nbins_Zmass_t+1] = {80, 80.5, 81, 81.5, 82, 82.5, 83, 83.5, 84, 84.5, 85, 85.5, 86, 86.5, 87, 87.5, 88, 88.5, 89, 89.5, 90, 90.5, 91, 91.5, 92, 92.5, 93, 93.5, 94, 94.5, 95, 95.5, 96, 96.5, 97, 97.5, 98, 98.5, 99, 99.5, 100};
}

// calculate chi^2 for two 1-D histograms
//
inline double algowz::GetChi2_OneDHists(TH1D* h1, TH1D* h2, int nmin, int nmax){

  // make sure they have the same binnings
  Int_t nbins = h1->GetNbinsX();

  if(h2->GetNbinsX()!=nbins){
    cout<<" *** Chi^2 calculation: different number of bins for h1 and h2 templates"<<endl;
    cout<<"     The number of bins in h1 is "<<nbins<<";"<<endl;
    cout<<"     The number of bins in h2 is "<<h2->GetNbinsX()<<" !!"<<endl;
  }

  // normalize to 1.
  h1->Scale(1.0 / h1->Integral());
  h2->Scale(1.0 / h2->Integral());  
  double h1_entry = h1->Integral(nmin, nmax);
  double h2_entry = h2->Integral(nmin, nmax);
  //h1->Scale(h2_entry/h1_entry);

  double chi2 = 0.;

  for(int ibinx=nmin; ibinx<=nmax; ibinx++) {
    double error_data = h1->GetBinError(ibinx)*h1->GetBinError(ibinx);
    double error_mc   = h2->GetBinError(ibinx)*h2->GetBinError(ibinx);
    double error      = error_data + error_mc;
    if(error>0)
      chi2 += pow(h1->GetBinContent(ibinx) - h2->GetBinContent(ibinx), 2)/(error);
  }

  return chi2;
}

inline double algowz::Collins_Soper(TLorentzVector mum, TLorentzVector mup){
  double dimass12 = (*(&mum) + *(&mup)).M();
  double diPx = (mum.Px() + mup.Px());
  double diPy = (mum.Py() + mup.Py());
  double dimassQT = sqrt( diPx*diPx + diPy*diPy );
  double P1plus = ( mum.E() + mum.Pz() );
  double P1mins = ( mum.E() - mum.Pz() );
  double P2plus = ( mup.E() + mup.Pz() );
  double P2mins = ( mup.E() - mup.Pz() );
  double collins = ( P1plus*P2mins - P1mins*P2plus ) /
    ( dimass12 * sqrt(dimass12*dimass12 + dimassQT*dimassQT) );

  return collins;
}

inline double algowz::Collins_Soper_Phi(TLorentzVector mum, TLorentzVector mup){
  TLorentzVector boostedMu = mum ;
  TLorentzVector Z = (mum+mup);
  TVector3 CSAxis, xAxis, yAxis;
  boostedMu.Boost(-Z.BoostVector());
  double ProtonMass = 938.272; // MeV
  double BeamEnergy = 6800000; // MeV
  double sign  = fabs(Z.Z())/Z.Z();
  TLorentzVector p1, p2;
  p1.SetPxPyPzE(0, 0, sign*BeamEnergy, 
		TMath::Sqrt(BeamEnergy*BeamEnergy+ProtonMass*ProtonMass)); // quark
  p2.SetPxPyPzE(0, 0, -1*sign*BeamEnergy,
		TMath::Sqrt(BeamEnergy*BeamEnergy+ProtonMass*ProtonMass)); // antiquark
  
  p1.Boost(-Z.BoostVector()); 
  p2.Boost(-Z.BoostVector()); 
  CSAxis = (p1.Vect().Unit()-p2.Vect().Unit()).Unit();
  yAxis = (p1.Vect().Unit()).Cross((p2.Vect().Unit()));
  yAxis = yAxis.Unit();
  xAxis = yAxis.Cross(CSAxis);
                                                                                
  xAxis = xAxis.Unit();
  double phi = atan2((boostedMu.Vect()*yAxis),(boostedMu.Vect()*xAxis));
  //if(phi<0) phi = phi + 2* M_PI;
  return phi;
}

inline double algowz::Collins_Soper_NewPhi(TLorentzVector electron, TLorentzVector positron){

  TLorentzVector dilepton = electron + positron;
  TVector3 boost = -dilepton.BoostVector();

  // redefine electron, positron vectors because they will be modified in the following code
  TLorentzVector el(electron.Px(), electron.Py(), electron.Pz(), electron.E());
  TLorentzVector po(positron.Px(), positron.Py(), positron.Pz(), positron.E());
  
  el.Boost(boost);
  po.Boost(boost);
  TVector3 el_vec=el.Vect();
  TVector3 po_vec=po.Vect();
  TLorentzVector lab_z1(0.,0.,6800.,6800.);
  TLorentzVector lab_z2(0.,0.,-6800.,6800.);
  lab_z1.Boost(boost);
  lab_z2.Boost(boost);
  TVector3 boson_frame = lab_z1.Vect().Unit()-lab_z2.Vect().Unit();
  TVector3 yplant_frame = (lab_z1.Vect().Unit()).Cross(lab_z2.Vect().Unit());
  TVector3 xplant_frame = (yplant_frame.Cross(boson_frame)).Unit();
    
  double phi = TMath::ATan2(el_vec.Dot(yplant_frame), el_vec.Dot(xplant_frame));
  return phi;
}

// error
inline double algowz::AFB_Err(double NF, double NB, double NF_err, double NB_err) {
  return 2*sqrt(NB*NB*NF_err*NF_err + NF*NF*NB_err*NB_err)/pow(NF+NB, 2);
}

// make AFB
inline void algowz::Make_AFB(TH1D* histF, TH1D* histB, TH1D* hist_afb){
  for(int i = 0; i < histF->GetNbinsX(); i++){
    double nf = histF->GetBinContent(i+1);
    double nb = histB->GetBinContent(i+1);
    double ef = histF->GetBinError(i+1);
    double eb = histB->GetBinError(i+1);

    double afb = 0.;
    double err = 0.;
    
    if((nf+nb) > 0){
      afb = (nf - nb)/(nf + nb);
      err = AFB_Err(nf, nb, ef, eb);
    }
    hist_afb->SetBinContent(i+1, afb);
    hist_afb->SetBinError(i+1, err);
  }
}

inline double algowz::Cal_PhiStar(TLorentzVector mum, TLorentzVector mup){
  double dphi = TMath::Pi() - fabs(mum.DeltaPhi(mup));
  double deta = mum.Eta() - mup.Eta();
  double phi  = TMath::Tan(dphi/2.)/TMath::CosH(deta/2.);

  return phi;
}
inline double algowz::Efficiency_Error(double a, double b){
  return TMath::Sqrt((a+1.0)*fabs(b-a+1.0)/(b+2.0)/(b+2.0)/(b+3.0));
}
inline void algowz::Make_Eff(TH1D* hist_nume, TH1D* hist_deno, TH1D* hist){

  for(int i = 0; i < hist_deno->GetNbinsX()+2; i++){
    hist->SetBinContent(i, 0.);
    hist->SetBinError(i, 0.);
    
    double n_deno = hist_deno->GetBinContent(i);
    double n_nume = hist_nume->GetBinContent(i);
    double e_deno = hist_deno->GetBinError(i);
    double e_nume = hist_nume->GetBinError(i);
    
    double ratio = 1.;
    double error = 0.;
    if(n_deno > 0){
      ratio = n_nume/n_deno;
      error = Efficiency_Error(n_nume, n_deno);
    } 
    
    hist->SetBinContent(i, ratio);
    hist->SetBinError(i, error);
  } 
} 
inline double algowz:: EW_alphas(double AMU)
{
 double ALPHAS;

 double QCDL=0.326;
 double AL4=QCDL;
 double AL5=AL4 * pow((AL4/mB), (2.0/23.0)) * pow((log(mB*mB/AL4/AL4)), (-963.0/13225.0));
 double AL6=AL5 * pow((AL5/mT), (2.0/21.0)) * pow((log(mT*mT/AL5/AL5)), (-321.0/3381.0));

 double XRAT = AMU * AMU;

 double sqrt_xrat = AMU;

 double RAT;
 double B0;
 double B1;

 if(sqrt_xrat > mT){
   RAT=log(XRAT/AL6/AL6);
   B0=12.0*3.14159/21.0;
   B1=6.0*(153.0-19.0*6.0)/21.0/21.0;
 }
 else if(sqrt_xrat > mB){
   RAT=log(XRAT/AL5/AL5);
   B0=12.0*3.14159/23.0;
   B1=6.0*(153.0-19.0*5.0)/23.0/23.0;
 }
 else{
   RAT=log(XRAT/AL4/AL4);
   B0=12.0*3.14159/25.0;
   B1=6.0*(153.0-19.0*4.0)/25.0/25.0;
 }
 ALPHAS=B0/RAT*(1.0-B1*log(RAT)/RAT);

 return ALPHAS;
}


inline double algowz:: GetWMassReweightingFactor(double Q, double OriginWMass, double NewWMass){
 double GMU = 1.16637e-5;

 double SWS = 1 - (OriginWMass * OriginWMass) / (91.1876 * 91.1876);
 double AEM = sqrt(2.0) * OriginWMass * OriginWMass * GMU * SWS / TMath::Pi();
 double GFERMI = TMath::Pi() * AEM / sqrt(2.0) / SWS / OriginWMass / OriginWMass;
 double GAMW = GFERMI * pow(OriginWMass, 3) / 6.0 / TMath::Pi() / sqrt(2.0) * 9.0;
 double OriginGammaW = GAMW * (1.0 + 2.0 * EW_alphas(OriginWMass) / TMath::Pi() / 3.0);

 SWS = 1 - (NewWMass * NewWMass) / (91.1876 * 91.1876);
 AEM = sqrt(2.0) * NewWMass * NewWMass * GMU * SWS / TMath::Pi();
 GFERMI = TMath::Pi() * AEM / sqrt(2.0) / SWS / NewWMass / NewWMass;
 GAMW = GFERMI * pow(NewWMass, 3) / 6.0 / TMath::Pi() / sqrt(2.0) * 9.0;
 double NewGammaW = GAMW * (1.0 + 2.0 * EW_alphas(NewWMass) / TMath::Pi() / 3.0);

 double Value = 1.0;

 if(WWidth < 0){
// GAMW_NLO_R
   double Numer = (Q * Q - OriginWMass * OriginWMass) * (Q * Q - OriginWMass * OriginWMass) + (Q * Q * Q * Q) / (OriginWMass * OriginWMass * OriginGammaW * OriginGammaW);
   double Denom = (Q * Q - NewWMass * NewWMass) * (Q * Q - NewWMass * NewWMass) + (Q * Q * Q * Q) / (NewWMass * NewWMass * NewGammaW * NewGammaW);

   Value = Numer / Denom;
 }
 else{
//Fixed GammaW
   double GammaW = WWidth;

   double Numer = (Q * Q - OriginWMass * OriginWMass) * (Q * Q - OriginWMass * OriginWMass) + (Q * Q * Q * Q) / (OriginWMass * OriginWMass * GammaW * GammaW);
   double Denom = (Q * Q - NewWMass * NewWMass) * (Q * Q - NewWMass * NewWMass) + (Q * Q * Q * Q) / (NewWMass * NewWMass * GammaW * GammaW);
//   double Denom = (Q * Q - NewWMass * NewWMass) * (Q * Q - NewWMass * NewWMass) + (Q * Q * Q * Q) / (NewWMass * NewWMass * 2.084 * 2.084);

   Value = Numer / Denom;
 }

 return Value;
}




#endif
