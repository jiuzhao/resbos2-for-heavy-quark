//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sun Jan  5 17:27:48 2020 by ROOT version 5.34/36
// from TTree MCDecayTree/Fake pmcs output
// found on file: ../tupleMaker/Wminus_PT15GEV_ETA15_50_CT14NNLO_FSR_15M.root
//////////////////////////////////////////////////////////

#ifndef _ZMUMU_SKIM_hh
#define _ZMUMU_SKIM_hh

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>
#include "TH1.h"
#include "TH2.h"
#include <TMath.h>
#include <TLorentzVector.h>

#include <iostream>

#include "histo_Handle.hpp"
#include "PParameterReader.hpp"
using namespace std;

// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.

class ZMuMu_Skim : public TSelector {
public :
  TTreeReader     fReader;  //!the tree reader
  TTree          *fChain = 0;   //!pointer to the analyzed TTree or TChain

  // Readers to access the data (delete the ones you do not need).
  TTreeReaderValue<Int_t> nevtp = {fReader, "pmcs_ana.nevtp"};
  TTreeReaderValue<Int_t> npart = {fReader, "pmcs_ana.npart"};
  TTreeReaderValue<Int_t> nvtx = {fReader, "pmcs_ana.nvtx"};
  TTreeReaderArray<Float_t> evnum = {fReader, "pmcs_ana.evnum"};
  TTreeReaderArray<Float_t> evrun = {fReader, "pmcs_ana.evrun"};
  TTreeReaderArray<Float_t> evwt = {fReader, "pmcs_ana.evwt"};
  TTreeReaderArray<Float_t> evxs = {fReader, "pmcs_ana.evxs"};
  TTreeReaderArray<Float_t> pE = {fReader, "pmcs_ana.pE"};
  TTreeReaderArray<Float_t> pcid = {fReader, "pmcs_ana.pcid"};
  TTreeReaderArray<Float_t> pcnum = {fReader, "pmcs_ana.pcnum"};
  TTreeReaderArray<Float_t> pdvtx = {fReader, "pmcs_ana.pdvtx"};
  TTreeReaderArray<Float_t> peta = {fReader, "pmcs_ana.peta"};
  TTreeReaderArray<Float_t> pidx = {fReader, "pmcs_ana.pidx"};
  TTreeReaderArray<Float_t> pistable = {fReader, "pmcs_ana.pistable"};
  TTreeReaderArray<Float_t> pphi = {fReader, "pmcs_ana.pphi"};
  TTreeReaderArray<Float_t> ppid = {fReader, "pmcs_ana.ppid"};
  TTreeReaderArray<Float_t> ppt = {fReader, "pmcs_ana.ppt"};
  TTreeReaderArray<Float_t> ppvtx = {fReader, "pmcs_ana.ppvtx"};
  TTreeReaderArray<Float_t> ppx = {fReader, "pmcs_ana.ppx"};
  TTreeReaderArray<Float_t> ppy = {fReader, "pmcs_ana.ppy"};
  TTreeReaderArray<Float_t> ppz = {fReader, "pmcs_ana.ppz"};
  TTreeReaderArray<Float_t> vcid = {fReader, "pmcs_ana.vcid"};
  TTreeReaderArray<Float_t> vcnum = {fReader, "pmcs_ana.vcnum"};
  TTreeReaderArray<Float_t> vct = {fReader, "pmcs_ana.vct"};
  TTreeReaderArray<Float_t> vidx = {fReader, "pmcs_ana.vidx"};
  TTreeReaderArray<Float_t> visdisp = {fReader, "pmcs_ana.visdisp"};
  TTreeReaderArray<Float_t> vpprt = {fReader, "pmcs_ana.vpprt"};
  TTreeReaderArray<Float_t> vx = {fReader, "pmcs_ana.vx"};
  TTreeReaderArray<Float_t> vy = {fReader, "pmcs_ana.vy"};
  TTreeReaderArray<Float_t> vz = {fReader, "pmcs_ana.vz"};

  ZMuMu_Skim(TTree * /*tree*/ =0) { }
  virtual ~ZMuMu_Skim() { }
  virtual Int_t   Version() const { return 2; }
  virtual void    Begin(TTree *tree);
  virtual void    SlaveBegin(TTree *tree);
  virtual void    Init(TTree *tree);
  virtual Bool_t  Notify();
  virtual Bool_t  Process(Long64_t entry);
  virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
  virtual void    SetOption(const char *option) { fOption = option; }
  virtual void    SetObject(TObject *obj) { fObject = obj; }
  virtual void    SetInputList(TList *input) { fInput = input; }
  virtual TList  *GetOutputList() const { return fOutput; }
  virtual void    SlaveTerminate();
  virtual void    Terminate();
  void setParameterFileName(const char *s){ _parameter_filename = TString(s); }

private:
  TString _parameter_filename;
  void ReadParameters();

  TString _outputfile;
  TString _EvtType;
  bool _IsMC;
  bool _StudyFull;
  histo_Handle* _zmumu_hists;

  void Init_hist();

  int n_events;
  int n_cuts;

  TChain* ch;
  Float_t Px_d1;
  Float_t Px_d2;
  Float_t WT00; 

  ClassDef(ZMuMu_Skim,0);
};
#endif

#ifdef ZMuMu_Skim_cxx
void ZMuMu_Skim::Init(TTree *tree){
  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the branch addresses and branch
  // pointers of the tree will be set.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).

  fReader.SetTree(tree);
}

Bool_t ZMuMu_Skim::Notify(){
  // The Notify() function is called when a new file is opened. This
  // can be either for a new TTree in a TChain or when when a new TTree
  // is started when using PROOF. It is normally not necessary to make changes
  // to the generated code, but the routine can be extended by the
  // user if needed. The return value is currently not used.

  return kTRUE;
}

#endif // #ifdef ZMuMu_Skim_cxx
