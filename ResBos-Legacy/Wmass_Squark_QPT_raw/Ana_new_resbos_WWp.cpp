#include <TH2.h>
#include <TStyle.h>
#include "TMath.h"

#include <iostream>
#include <fstream>
#include <vector>
#include "AlgoWZ.hpp"
using namespace algowz;

void Ana_new_resbos_Wp(string type){

  //gROOT->ProcessLine(".x ../Function/lhcbStyle.C");
  
  TChain* ch = new TChain();
  if(type.find("Wm")!=string::npos)
    ch->Add("/EOS/jzli/W_Data/Wm_bc.root/h10");
    
  if(type.find("Wp")!=string::npos)
    ch->Add("/EOS/jzli/W_Data/Wp_bc.root/h10");
  if(type.find("Wp_withoutbc")!=string::npos)
    ch->Add("/EOS/jzli/W_Data/Wp.root/h10");

  Float_t n_px, n_py, n_pz, n_pe;
  Float_t muon_px, muon_py, muon_pz, muon_pe;
  Float_t w_px  ,   w_py,   w_pz,   w_pe;
  Float_t wt;

  int n_counts;
  double n_pass = 0.;
  double n_evts = 0.;
// W->femi + anti-femi
// W+ ---> neutrino_e  e+
// W- ---> e-   n 


  ch->SetBranchAddress("Px_d1", &n_px);
  ch->SetBranchAddress("Py_d1", &n_py);
  ch->SetBranchAddress("Pz_d1", &n_pz);  
  ch->SetBranchAddress("E_d1" , &n_pe);
  
  ch->SetBranchAddress("Px_d2", &muon_px);
  ch->SetBranchAddress("Py_d2", &muon_py);
  ch->SetBranchAddress("Pz_d2", &muon_pz);  
  ch->SetBranchAddress("E_d2" , &muon_pe);

  ch->SetBranchAddress("Px_V", &w_px);
  ch->SetBranchAddress("Py_V", &w_py);
  ch->SetBranchAddress("Pz_V", &w_pz);  
  ch->SetBranchAddress("E_V" , &w_pe);
  
  ch->SetBranchAddress("WT00" , &wt);

  TFile *File = new TFile(Form("./resbos%s.root",type.c_str()),"recreate");
  TTree* AnaTree = new TTree("AnaTree","AnaTree");
  TH1D *Hist_WM   = new TH1D("ResBos_WBoson_Mass", "",   60, 60, 100);
  TH1D *Hist_WY   = new TH1D("ResBos_WBoson_Rapidity",  "", nbins_wy, bins_wy);
  TH1D *Hist_WPT  = new TH1D("ResBos_WBoson_PT", "",   nbins_wpt, bins_wpt);
  TH1D *Hist_MUONPT  = new TH1D("ResBos_muon_PT", "",  100, 20, 70);
  TH1D *Hist_NPT  = new TH1D("ResBos_n_PT", "",  100, 20, 70);
  //Hist_WM->Sumw2();
  //Hist_WY->Sumw2();
  //Hist_WPT->Sumw2();  
  
  Float_t muon_pt, muon_eta, n_pt, n_eta;
  Float_t w_mass_reco, w_pt_reco, w_y_reco, phistar;
  AnaTree -> Branch("N_PX" ,&n_px ,"N_PX/D");
  AnaTree -> Branch("N_PY" ,&n_py ,"N_PY/D");
  AnaTree -> Branch("N_PZ" ,&n_pz ,"N_PZ/D");
  AnaTree -> Branch("N_PE" ,&n_pe ,"N_PE/D");    
  AnaTree -> Branch("N_PT" ,&n_pt ,"N_PT/D");
  AnaTree -> Branch("N_ETA",&n_eta,"N_ETA/D");

  AnaTree -> Branch("Muon_PX" ,&muon_px ,"Muon_PX/D");
  AnaTree -> Branch("Muon_PY" ,&muon_py ,"Muon_PY/D");
  AnaTree -> Branch("Muon_PZ" ,&muon_pz ,"Muon_PZ/D");
  AnaTree -> Branch("Muon_PE" ,&muon_pe ,"Muon_PE/D");    
  AnaTree -> Branch("Muon_PT" ,&muon_pt ,"Muon_PT/D");
  AnaTree -> Branch("Muon_ETA",&muon_eta,"Muon_ETA/D");

  AnaTree -> Branch("W_M"  , &w_mass_reco, "W_M/D");
  AnaTree -> Branch("W_PT" , &w_pt_reco  , "W_PT/D");
  AnaTree -> Branch("W_Y"  , &w_y_reco   , "W_Y/D");
  AnaTree -> Branch("W_PHI", &phistar    , "W_PHI/D");  
  AnaTree -> Branch("swits",  &wt        , "swits/D");

  
  for(int evts=0; evts < ch->GetEntries(); evts++){
    //    if(evts%1000000 == 0)  printf("Processing %2d events!\n", n_evts);
    if(n_counts%10000000 == 0)  printf("Processing %2d events!\n", n_counts);
    n_counts++;
    ch->GetEntry(evts); 

    // Initialization
    TLorentzVector muon, n, wboson;
    n.SetPxPyPzE(n_px, n_py, n_pz, n_pe);	
    muon.SetPxPyPzE(muon_px, muon_py, muon_pz, muon_pe);
    wboson.SetPxPyPzE(w_px, w_py, w_pz, w_pe);

    // Event Information
    n_pt     = n.Pt();
    n_eta    = n.Eta();
    muon_pt     = muon.Pt();
    muon_eta    = muon.Eta();

    w_mass_reco= (muon+n).M();
    w_pt_reco  = (muon+n).Pt();
    w_y_reco   = (muon+n).Rapidity();
    phistar    =  Cal_PhiStar(muon, n);  

    bool muon_lhcbacc = muon_eta > 2.0 && muon_eta < 4.5 && muon_pt > 4.;
    bool n_lhcbacc = n_eta > 2.0 && n_eta < 4.5 && n_pt > 0;
    bool masscut = w_mass_reco > 60 && w_mass_reco < 120;

    // Selection
    //if(muon_lhcbacc && n_lhcbacc && muon_pt > 20 && n_pt > 0 && masscut){
    if(muon_lhcbacc && n_lhcbacc && muon_pt > 20){
      n_pass += (double)wt;
      // AnaTree->Fill();
      Hist_WM->Fill(w_mass_reco, wt);
      Hist_WY->Fill(w_y_reco, wt);
      Hist_WPT->Fill(w_pt_reco, wt);
      Hist_MUONPT->Fill(muon_pt,wt);
      Hist_NPT->Fill(n_pt,wt);
    }
    n_evts += (double)wt;
  }
  cout << endl;
  Float_t eff = (Float_t)n_pass/n_evts;

//  cout << "===== For W " << type << " type  =====" << endl;
  cout << "Before selection: " << n_evts << endl;
  cout << "After  selection: " << n_pass << endl;
  cout << "New ResBos eff  : " <<  eff << endl;

  File->Write();
  File->Close();
}
