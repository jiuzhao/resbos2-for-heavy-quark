#include <iostream>
#include <fstream>
#include <vector>
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooBinomial.h"
#include "RooFitResult.h"
#include "RooPlot.h"
#include "TCanvas.h"

int main() {
    // 数据文件列表
    std::vector<std::string> dataFiles = {"data1.txt", "data2.txt", "data3.txt"};

    // 循环处理每个数据文件
    for (const auto& dataFile : dataFiles) {
        // 读取数据
        std::vector<double> xData;
        std::vector<double> yData;
        double xValue, yValue;

        std::ifstream inputFile(dataFile);
        if (inputFile.is_open()) {
            while (inputFile >> xValue >> yValue) {
                xData.push_back(xValue);
                yData.push_back(yValue);
            }
            inputFile.close();
        } else {
            std::cout << "Unable to open file: " << dataFile << std::endl;
            continue;  // 继续处理下一个文件
        }

        // 创建 RooDataSet 对象
        RooRealVar x("x", "x", 0, 100);
        RooRealVar y("y", "y", 0, 100);
        RooDataSet *dataSet = new RooDataSet("data", "Data", RooArgSet(x, y));

        for (size_t i = 0; i < xData.size(); i++) {
            x = xData[i];
            y = yData[i];
            dataSet->add(RooArgSet(x, y));
        }

        // 创建二项式分布模型并执行拟合过程
        RooRealVar p("p", "p", 0.5, 0, 1);
        RooRealVar n("n", "n", 100, 0, 200);
        RooBinomial model("model", "Model", y, n, p);

        RooFitResult *fitResult = model.fitTo(*dataSet, RooFit::Save());
        fitResult->Print();

        // 绘制拟合结果
        RooPlot *frame = y.frame();
        dataSet->plotOn(frame);
        model.plotOn(frame);

        TCanvas canvas("canvas", "Fit Result", 800, 600);
        frame->Draw();
        canvas.SaveAs(("fit_result_" + dataFile + ".png").c_str());

        // 输出对称轴所在的X值
        double symmetricX = model.mean(n.getVal(), p.getVal());
        std::cout << "Symmetric X value for " << dataFile << ": " << symmetricX << std::endl;

        delete dataSet;
    }

    return 0;
}
