void ScaleUncertainty()
{
 int NScales = 15;
 int ScaleCentral = 8;
 int iflag = 1;
 TString histName = "ZPt";
 TString saveName = histName + "_ScaleError.root";

 ifstream infile;
  infile.open("scale.list", ios::in);

 TString rootName;

 TFile* file[NScales + 1];
 TH1D *h1[NScales + 1];
 TH2D *h2[NScales + 1];

 TH1D *ScalePlot1D[NScales + 1];
 TH2D *ScalePlot2D[NScales + 2];

 double Unc2 = 0;
 double Unc = 0;

 TFile* writefile = new TFile(saveName, "RECREATE");

 for(int i = 1; i < NScales + 1; i++){
  infile>>rootName;
  file[i] = new TFile(rootName);
  if(iflag == 1) h1[i] = (TH1D *)file[i]->Get(histName);
  if(iflag == 2) h2[i] = (TH2D *)file[i]->Get(histName);
 }

 if(iflag == 1) h1[0] = h1[ScaleCentral];
 if(iflag == 2) h2[0] = h2[ScaleCentral];

 TH1D *ScaleError1D;
 TH2D *ScaleError2D;

 TH1D *ScaleCentral1D;
 TH2D *ScaleCentral2D;

 TH1D *ScaleStatError1D;
 TH2D *ScaleStatError2D;

 writefile->cd();
 if(iflag == 1) ScaleCentral1D = (TH1D *)h1[0]->Clone(histName + "_ScaleCentral");
 if(iflag == 2) ScaleCentral2D = (TH2D *)h2[0]->Clone(histName + "_ScaleCentral");

 if(iflag == 1) ScaleError1D = (TH1D *)h1[1]->Clone(histName + "_ScaleError");
 if(iflag == 2) ScaleError2D = (TH2D *)h2[1]->Clone(histName + "_ScaleError");

 if(iflag == 1) ScaleStatError1D = (TH1D *)h1[1]->Clone(histName + "_ScaleStatError");
 if(iflag == 2) ScaleStatError2D = (TH2D *)h2[1]->Clone(histName + "_ScaleStatError");

 if(iflag == 1) ScaleStatError1D->Reset();
 if(iflag == 2) ScaleStatError2D->Reset();

 if(iflag == 1){
    for(int i = 1; i < NScales + 1;i++){
        ScalePlot1D[i] = (TH1D *)h1[i]->Clone(histName + "_Scale" + (long)(900 + i));
    }
 }
 if(iflag == 2){
    for(int i = 1; i < NScales + 1;i++){
        ScalePlot2D[i] = (TH2D *)h2[i]->Clone(histName + "_Scale" + (long)(900 + i));
    }
 }


 if(iflag == 1){
  for(int ibin = 1; ibin <= h1[1]->GetNbinsX(); ibin++){
   ScaleStatError1D->SetBinContent(ibin, ScaleCentral1D->GetBinError(ibin));
  }
 }

 if(iflag == 2){
  for(int ibinx = 1; ibinx <= h2[1]->GetNbinsX(); ibinx++){
   for(int ibiny = 1; ibiny <= h2[1]->GetNbinsY(); ibiny++){
   ScaleStatError2D->SetBinContent(ibinx, ibiny, ScaleCentral2D->GetBinError(ibinx, ibiny));
   }
  }
 }

 if(iflag == 1){
  for(int ibin = 1; ibin <= h1[1]->GetNbinsX(); ibin++){
   Unc2 = 0;
   Unc = 0;
   for(int i = 1; i < NScales + 1; i++){
    Unc2 = fabs(h1[i]->GetBinContent(ibin) - h1[0]->GetBinContent(ibin));
    if(Unc2 >= Unc) Unc = Unc2;
   }
   ScaleError1D->SetBinContent(ibin, Unc);
  }
 }
 if(iflag == 2){
  for(int ibinx = 1; ibinx <= h2[1]->GetNbinsX(); ibinx++){
   for(int ibiny = 1; ibiny <= h2[1]->GetNbinsY(); ibiny++){
    Unc2 = 0;
    Unc = 0;
    for(int i = 1; i < NScales + 1; i++){
     Unc2 = fabs(h2[i]->GetBinContent(ibinx, ibiny) - h2[ScaleCentral]->GetBinContent(ibinx, ibiny));
     if(Unc2 >= Unc) Unc = Unc2;
    }
    ScaleError2D->SetBinContent(ibinx, ibiny, Unc);
   }
  }
 }  

 writefile->Write();
 writefile->Close();
}
