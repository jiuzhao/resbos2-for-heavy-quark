#!/bin/bash
WORKDIR=$PWD
multicore=0
localrun=0
WConfig=0   ###1: w321  2: w432  0: FixedOrder
YConfig=0   ###1: y1    2: y2    0: onlyW

PDFNumber=2
ECM=8TeV
# PDFNumber = 1: CT14HEAR2NNLO
# PDFNumber = 2: CT14nnlo
# PDFNumber = 3: MMHT2014nnlo68cl
######################Initialize the PDF Name###############################
source /home/yfu/pku_resbos/FrameWork/script/PDFInformation.sh $PDFNumber
source /home/yfu/pku_resbos/FrameWork/script/ResBosConfig.sh $WConfig $YConfig 1
############################################################################

Process=$1
if [ $Process -eq 1 ]; then
echo Running scale variation.
NProcess=15
InitialProcess=1
elif [ $Process -eq 2 ]; then
echo Running PDF uncertainty.
NProcess=${NumberOfPDFSet}
InitialProcess=0
fi

NJobs=1

if [ $WConfig -eq 0 ]; then
    WHICHPDF=${MYPDF}_${ECM}_FixedOrder
fi

Statistic=$[50*${NJobs}]
Statistic=${Statistic}M

##########Define Scale and PDF name####################
for((k=${InitialProcess};k<=$NProcess;k++)); do
if [ $k -lt 10 ]; then
PDFDIR=${PDFFile}0${k}
else
PDFDIR=${PDFFile}${k}
fi
ScaleVari=$[900+${k}]
ScaleDIR=Scale${ScaleVari}

##########build directory##############################
if [ $Process -eq 1 ]; then
cd $WORKDIR
PDFDIR=${PDFFile}00
elif [ $Process -eq 2 ]; then
cd $WORKDIR
ScaleDIR=Scale908
fi

###############Run Scale####################
if [ $Process -eq 1 ]; then

cd $WORKDIR/${WHICHPDF}/$ScaleDIR

##############Run PDF#######################
elif [ $Process -eq 2 ]; then

cd $WORKDIR/${WHICHPDF}/$PDFDIR

fi

if [ ! -d "$Statistic/" ]; then
    mkdir $Statistic
fi
cd $Statistic

#########Running#######################################
for((n=1;n<=$NJobs;n++)); do
echo ../JOB${n}/MC_results.root >> tem.list
done
hadd MC_results.root @tem.list

echo $PDFDIR $ScaleDIR has finished

done
cd $WORKDIR
